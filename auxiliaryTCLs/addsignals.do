# MODULE simGenerateNodes.do TO CREATE A FILE OF THE EXISTING NODES UNDER A SPECIFIED ENVIRONMENT
# 
# CALL IN MODELSIM USING: do simGenerateNodes.do pathToInst
# EXAMPLES: do simGenerateNodes.do /testbench/d3/u0/leon3x0/cmem0
# UPDATED: 02-feb-2015 created

quietly set inst $1
quietly set itemList {}
quietly set portsList {}
quietly set elemList {}
quietly set signalsList {}
quietly set generateList {}
quietly set architectureList {}

# --------------------
proc buildLists {ipathToInst iportsList isignalsList ielemList} {
	set itemList [Show $ipathToInst]
	
	for {set it 0} {$it < [llength [lindex $itemList 2]]} {incr it} {
		#assume element 1 is architecture, and 2 is the remaining elements
		set currItem [lindex [lindex $itemList 2] $it]
		echo $it, $currItem;
		if {[lindex $currItem 1] != ".."} { 					# ignore parent element

			if {[lindex $currItem 0] == "Port"} {
				lappend iportsList "$ipathToInst/[lindex $currItem 1]"
				lappend ielemList "$ipathToInst/[lindex $currItem 1]"
				# eval add wave {"$currItem $it" { "sim:$inst[lindex $currItem 1]" }}
			}
			if {[lindex $currItem 0] == "Signal"} {
				lappend isignalsList "$ipathToInst/[lindex $currItem 1]"
				lappend ielemList "$ipathToInst/[lindex $currItem 1]"
				# eval add wave {"$currItem $it" { "sim:$inst[lindex $currItem 1]" }}
			}
			if {[regexp {Generate} [lindex $currItem 0]] == 1} {
				set i2pathToInst $ipathToInst/[lindex $currItem 1]
				set ret [buildLists $i2pathToInst $iportsList $isignalsList $ielemList]
				set iportsList [lindex $ret 0]
				set isignalsList [lindex $ret 1]
				set ielemList [lindex $ret 2]
				# lappend generateList "$inst[lindex $currItem 1]/"
				# Generate is equivalent to architecture
			}
			if {[lindex $currItem 0] == "Architecture"} {
				set i2pathToInst $ipathToInst/[lindex $currItem 1]
				set ret [buildLists $i2pathToInst $iportsList $isignalsList $ielemList]
				set iportsList [lindex $ret 0]
				set isignalsList [lindex $ret 1]
				set ielemList [lindex $ret 2]
				# lappend iarchitectureList "[lindex $currItem 1]"
			}
		}
	}
	
	return [list $iportsList $isignalsList $ielemList]
}



proc addElements {ielemList} {
	for {set it 0} {$it < [llength $ielemList]} {incr it} {
		if {[find signals [lindex $ielemList $it].*] != ""}   {
				set sigList [lsort -dictionary [find signals [lindex $ielemList $it].*]]
				for {set s 0} {$s < [llength $sigList]} {incr s} {
					addElements [lindex $sigList $s]
				}
		} elseif { [find signals [lindex $ielemList $it](*)] != ""}  {
				set sigList [lsort -dictionary [find signals [lindex $ielemList $it](*)]]
				for {set s 0} {$s < [llength $sigList]} {incr s} {
					addElements [lindex $sigList $s]
				}
		} else {
				eval add wave [lindex $ielemList $it]
				
		}
	}
}


# ----------------------------------------------------------------------------------------

echo "------------- Processing Hierarchy: ------------------"
quietly set topList [buildLists $inst $portsList $signalsList $elemList]
quietly set portsList [lindex $topList 0]
quietly set signalsList [lindex $topList 1]
quietly set elemList [lindex $topList 2]
echo "------------- END ------------------"

# echo 
# echo "Addding portsList: $portsList"
# foreach p $portsList {eval add wave $p}
# echo
# echo "Adding signalsList: $signalsList"
# foreach s $signalsList {eval add wave $s}

# -----------------

echo
echo "Adding Ports and Signals: $elemList"
addElements $elemList
	



# quietly set str [join [lrange $selected 0 end]]
# echo \n this is str\n $str
# echo "sim:$inst[lindex $selected 0]"
# eval add wave {combi2 { "$str" }}	
# add wave -r $inst/*
#wave select all

# add wave {combi {sig1 sig2 sig3}}

# write format wave