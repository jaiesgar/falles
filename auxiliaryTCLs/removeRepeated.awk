# FILE TO DELETE REPEATED EXPERIMENTS OF INJECTION DATA
#
# CALL: gawk -f removeRepeated.awk Injections.log
# MODIFIED: 3-feb-2015 created

BEGIN {p=-1} {
if ($0 ~ /PACKET/) {
	p=$4;
} else if (/NUMBER/){
	if (p>= 0) {
		if (!($13 in x)) {
			x[$13]++
		} else {
			f=$2;
			system("echo REMOVING Packet"p"/Fault"f".lst")
			system("rm -f Packet*"p"/Fault0*"f".lst")
			system("rm -f Packet*"p+1"/Fault0*"f".lst")
			k++
		}
	}
}

} END {print "Total Removed Files: ",k }