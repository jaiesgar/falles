# CALL: tclsh renameResultsl.tcl [targetDir] [previousSuffix] newSuffix
# If only newSuffix is present adds that suffix to all Results
# If only two arguments are present renames previous to new suffix (also possible to use blank to remove suffixes)
# if three arguments are used renames results in target dir from previous to new suffix
if {$argc < 1} {

puts "CALL: tclsh renameResultsl.tcl \[targetDir\] \[previousSuffix\] newSuffix"
puts "To use targetDir option you must input all 3 parameters"
exit

} elseif {$argc == 1} {

	set directories [glob -nocomplain ./Packet*]
		foreach d $directories {
				catch {file rename -force [glob -nocomplain -directory $d Results*.log] $d/Results$argv.log} errId
		}
	catch {file rename -force [glob -nocomplain GatheredResults*.log] GatheredResults$argv.log} errId

} elseif {$argc == 2} {
	
	set directories [glob -nocomplain ./Packet*]
		foreach d $directories {
				catch {file rename -force $d/Results[lindex $argv 0].log $d/Results[lindex $argv 1].log} errId
		}
	catch {file rename -force GatheredResults[lindex $argv 0].log GatheredResults[lindex $argv 1].log} errId
	
} else {	

	set directories [glob -nocomplain [lindex $argv 0]/Packet*]
		foreach d $directories {
				catch {file rename -force $d/Results[lindex $argv 1].log $d/Results[lindex $argv 2].log} errId
		}
	catch {file rename -force [lindex $argv 0]/GatheredResults[lindex $argv 1].log [lindex $argv 0]/GatheredResults[lindex $argv 2].log} errId

}

if {$errId != 0}  {puts "WARNING: A Results or GatheredResults File could not be renamed correctly" }