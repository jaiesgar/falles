# TCL script to clear out startup information from injections. Change number 1202205 for the startInjTime value
# 
	set directories [glob -nocomplain ./Packet*]
	foreach d $directories {
		puts "indir $d"
		set files [glob -nocomplain -directory $d Fault*.lst]
		foreach f $files {
			exec gawk {{if ($0 !~ /[0-9]+/) print; if ($1 ~ /[0-9]+/) if ($1 > 1202205) print $0}} $f > $f&aux;
			file rename -force $f&aux $f
		}
	}
