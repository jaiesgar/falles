# INJECTED experiments SIMULATIONS OF MODULE top UNTIL time NANOSECONDS
# AUTHOR: J. Espinosa
# ROUND 5 **LINEAR FILTERED** injection from set of Nodes and injection time between startInjTime and endSimulation-lastInj2endTime, with a duration between minDuration and maxDuration ns, and with a separtion between minSeparation and maxSeparation
#
# CALL: do {./simInjections5.do} topModule endTime [[startFrom] experiments injections startInjTime lastInj2endTime maxDuration minDuration maxSeparation minSeparation isInjValueConstant isInjDurationConstant faultModel] 
# UPDATED: 16-mar-2015 debugged new simfiles creation
#					13-mar-2015 made linux compatible avoiding wlf files overwrite: Separated transcript,savestate and waveform files for each simProcess
#					2-mar-2015 Added random or sequential injections
#					27-feb-2015 Filtering now calls proc in simConfig.do, allows >1 loop to injNodes
#					25-feb-2015 Created


# SUPRESS WARNINGS IN PACKAGE NUMERIC_STD (truncations, etc...)
quietly set NumericStdNoWarnings 1
# DISABLE COMPRESSION OF STATE TO SPEED UP LOADING
quietly set CheckpointCompressMode 0

# SET BEGINNING OF EXPERIMENT
quietly set time4 [expr [clock seconds]]

#LOAD FILE WITH IMPLEMENTED FAULT MODELS
source simFaultModel.do

# LOAD FILE WITH INJECTION NODES
source simNodes.do

# LOAD FILE WITH CONFIGURATION -FOR FILTERING-
source simConfig.do

if {$argc >1}  {
		set topModule $1; set endTime $2;
	if {$argc == 2} {
		# ALL UNITS IN NANOSECONDS
		set experiments 50
		set injections 1
		set endResetTime 120
		set lastInj2endTime 40
		set maxDuration 60
		set minDuration 10
		set maxSeparation 5
		set minSeparation 0
		set isInjValueConstant 0
		set isInjDurationConstant 0
		set faultModel 1F
		
	} else {
		# to check if there's a startFrom parameter
		set startFrom 0;
		if { $argc == 16} {
			# case of resuming in MP
			set startFrom $3; shift
			set experiments $3; set injections $4; set endResetTime $5; set lastInj2endTime $6; set maxDuration $7; set minDuration $8; set maxSeparation $9
			shift; shift; shift; shift;  	# to move parameters 4 positions to the left, substracts 4 to argc
			set minSeparation $6; set isInjValueConstant $7; set isInjDurationConstant $8; set faultModel $9
		} elseif { $argc == 15 } {
			set three $3; set four $4; shift; shift;shift;shift;
			if { [regexp {^[0-9]+$} $9]} {
				# case of resuming in Single Process
				set startFrom $three;
				set experiments $four; set injections $1; set endResetTime $2; set lastInj2endTime $3; set maxDuration $4; set minDuration $5; set maxSeparation $6
				set minSeparation $7; set isInjValueConstant $8; set isInjDurationConstant $9; 
				shift; set faultModel $9
			} else {
				# case of starting from 0 in MP
				set experiments $three; set injections $four; set endResetTime $1; set lastInj2endTime $2; set maxDuration $3; set minDuration $4; set maxSeparation $5
				set minSeparation $6; set isInjValueConstant $7; set isInjDurationConstant $8; set faultModel $9
			}
		} else {
			# case of starting from 0 in MP
			set experiments $3; set injections $4; set endResetTime $5; set lastInj2endTime $6; set maxDuration $7; set minDuration $8; set maxSeparation $9
			shift; shift; shift; shift
			set minSeparation $6; set isInjValueConstant $7; set isInjDurationConstant $8; set faultModel $9
		}
		if {$argc > 10} {
			shift; shift; set procCount $8; set maxPacketSize $9
		} else { set maxPacketSize $experiments; set procCount 0}
	}


	
	set randSeed 0;  # change for new distribution
	quietly expr srand($randSeed)
	
	 transcript file transcript#
	restart
	restore ./Results/savedstate.sim
	dataset save sim vsim$procCount.wlf
	checkpoint ./Results/savedstate$procCount.sim
		# restore ./Results/savedstate$procCount.sim
	
	# OPEN OR APPEND TO PREVIOUS FILE
	set fd [open ./Results/Injections$procCount.log a+]
	puts "procCount here: $procCount in pid: [pid]"
	puts $fd "procCount here: $procCount in pid: [pid]"
	
	if {[expr $startFrom % $maxPacketSize] == 0 } {set iPacket [expr [expr $startFrom / $maxPacketSize] - 1];} else { set iPacket [expr $startFrom / $maxPacketSize]; }
	
	# LOOP TO INIT SIMULATION, INJECT AND WRITE RESULTS
onbreak { write transcript errlog.log;	resume	}  # to continue in case of failure type assertion
onerror {  write transcript errlog.log;	resume	}  # to continue in case of failure type assertion
for {set i 0} {$i < $experiments} {incr i} {
	set injDuration [expr [expr [expr rand()] * [expr $maxDuration - $minDuration]] + $minDuration]
	set injSeparation [expr [expr [expr rand()] * [expr $maxSeparation - $minSeparation]] + $minSeparation]
	set injTime [expr [expr [expr rand()]*[expr $endTime - $lastInj2endTime - $endResetTime]] + $endResetTime]
	# UNCOMMENT FOR ALL NODES INJECTED --IN ORDER ONE AFTER ANOTHER---
	set injNode [lindex $injNodes [expr $i - [expr [expr $i / [llength $injNodes]] * [llength $injNodes]]]]
	# UNCOMMENT FOR ALL NODES INJECTED ---RANDOMLY----
	# set injNode [lindex $injNodes [expr round([expr [expr rand()]*[expr [llength $injNodes] -1]])]]
	if {$startFrom == 0} { 												# to skip until we want to write the packet
		restore ./Results/savedstate$procCount.sim
		# restore ./Results/savedstate.sim
		run @[expr $injTime] ns
		set injValue [FindInjValue $faultModel $injNode]
		if {[regexp {F$} $faultModel]} {
			puts $fd [format "NUMBER $i INJECTION, FORCES $injValue FOR %.2f IN MOMENT %.2f AND NODE $injNode" $injDuration $injTime]
			echo [format "NUMBER $i INJECTION, FORCES $injValue FOR %.2f IN MOMENT %.2f AND NODE $injNode" $injDuration $injTime] 
			force -freeze sim:$injNode $injValue 0 -cancel [expr $injDuration] ns
		} elseif {[regexp {D$} $faultModel]} {
			puts $fd [format "NUMBER $i INJECTION, DEPOSITS $injValue FOR %.2f IN MOMENT %.2f AND NODE $injNode" $injDuration $injTime] 
			echo [format "NUMBER $i INJECTION, DEPOSITS $injValue FOR %.2f IN MOMENT %.2f AND NODE $injNode" $injDuration $injTime] 
			force -deposit sim:$injNode $injValue 0 -cancel [expr $injDuration] ns
		}
	}
	# #  FOR FURTHER INJECTIONS
	if {$injections > 1} {
		for {set remaining [expr $injections - 1] } { $remaining > 0} {incr remaining -1} {
				if {$isInjDurationConstant == 0} { set injDuration2 [expr [expr rand()] * $maxDuration]  } else { set injDuration2 $injDuration; }
				if {$startFrom == 0} { 												# to skip until we want to write the packet
					run [expr $injDuration + $injSeparation] ns
					if {$isInjValueConstant == 0} { set injValue2 [FindInjValue $faultModel $injNode]  } else { set injValue2 $injValue; }
					if {[regexp {F$} $faultModel]} {
						puts $fd [format "NUMBER $i INJECTION [expr $injections - $remaining + 1], FORCES $injValue2 FOR %.2f AFTER %.2f" $injDuration2 $injSeparation]
						echo [format "NUMBER $i INJECTION [expr $injections - $remaining + 1], FORCES $injValue2 FOR %.2f AFTER %.2f" $injDuration2 $injSeparation]
						force -freeze sim:$injNode $injValue2 0 -cancel [expr $injDuration2] ns
					} elseif {[regexp {D$} $faultModel]} {
						puts $fd [format "NUMBER $i INJECTION [expr $injections - $remaining + 1], DEPOSITS $injValue2 FOR %.2f AFTER %.2f" $injDuration2 $injSeparation]
						echo [format "NUMBER $i INJECTION [expr $injections - $remaining + 1], DEPOSITS $injValue2 FOR %.2f AFTER %.2f" $injDuration2 $injSeparation]
						force -deposit sim:$injNode $injValue2 0 -cancel [expr $injDuration2] ns
					}
				}
		}
	}



	if {$startFrom == 0} { 												# to skip until we want to write the packet
		run @$endTime ns
			if {[expr $i % $maxPacketSize] == 0} {
				incr iPacket;
				echo "== PACKET NUMBER $iPacket WAS STARTED =="
				puts $fd "== PACKET NUMBER $iPacket WAS STARTED =="
				file mkdir [format ./Results/Packet%04u/ $iPacket]
				# file copy ./Results/FaultFree.lst [format ./Results/Packet%04u/ $iPacket] # We no longer want to copy but to link
				# file link -symbolic [format ./Results/Packet%04u/FaultFree.lst $iPacket] "../FaultFree.lst"  # tcl command Not working with symbolic links to files on win
				set p1 [format ./Results/Packet%04u/FaultFree.lst $iPacket]
				exec ln -s ../FaultFree.lst $p1
			}
			write list [format ./Results/Packet%04u/Fault%07u.lst $iPacket $i];
			filterFunc 1 $iPacket $i;			
	} else {
		incr startFrom -1;
	}
	
}

# WRITE TIME OF EXECUTION
set expTime [expr [clock seconds]-$time4]
puts $fd "Experiments time. Total seconds: $expTime"
echo "Experiments time. Total seconds: $expTime"
puts $fd "[expr $expTime / 3600] H [expr [expr $expTime % 3600] /60] Min [expr $expTime % 60] Sec"
echo "[expr $expTime / 3600] H [expr [expr $expTime % 3600] /60] Min [expr $expTime % 60] Sec"

# WE CLOSE FILE DESCRIPTOR
close $fd

quit
} else {
	echo "ERROR: NO PROPER ARGUMENTS"
}