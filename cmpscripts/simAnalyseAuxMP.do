# MODULE simAnalyseAuxMP.do TO ANALYSE and TIME RESULTS
# AUTHOR: J. Espinosa
# REQUIRES USING simFaultsMP.do IN MODELSIM TO GENERATE DATA TO ANALYSE
# 
# CALL USING: do simAnalyseAuxMP.do startInjTime nameiPacket
# EXAMPLES: do simAnalyseAuxMP.do 11200 {./ResultsRTL 33 1 12000 11200 10000 10000 1 1 1 1 1F/Packet   0/}
# UPDATED:	2-mar-2015 Source simConfig for passing nrOutputs to simReadResults.awk
#						8-oct-2014 Made tcl distribution independent


if {$argc < 2} {
	echo "MYERROR: NO PROPER ARGUMENTS"
	echo "CALL USING: do simAnalyseAuxMP.do startInjTime nameiPacket"
} else {
	if {![info exists 1]} {
		set i 1
		foreach var $argv {
			set $i $var;
			incr i 
		}
	}
	
	# Load config for number of outputs parameter, add 1 if deltas are absent and 2 if they are present
	source simConfig.do
	
	set time1 [expr [clock seconds]];
	# The following line is for names of faults with spaces
	# exec gawk -v startInjTime=$1 -f simReadResults.awk  "$2/FaultFree.lst" "[file join $2 Fault] *.lst" > $2/Results.log
	# exec gawk -v startInjTime=$1 -f simReadResults.awk  "$2/FaultFree.lst" "[file join $2 Fault]0*.lst" > $2/Results.log # for modelsim
	eval exec gawk -v startInjTime=$1 -v lastOutputListOrder=[expr $nrOutputs + 1] -f simReadResults.awk $2/FaultFree.lst [glob [file join $2 Fault]0*.lst] > $2/Results.log
	set fd1 [open $2/Results.log a]
	set expTime1 [expr [clock seconds]-$time1]
	puts $fd1 "Analysis time. Total seconds: $expTime1"		
	puts $fd1 "[expr $expTime1 / 3600] H [expr [expr $expTime1 % 3600] /60] Min [expr $expTime1 % 60] Sec"
	close $fd1
	
}