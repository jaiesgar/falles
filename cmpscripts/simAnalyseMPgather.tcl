# MODULE simAnalyseMPgather.tcl TO GATHER RESULTS FROM MULTIPLE FOLDERS OF RESULTS
# AUTHOR: J. Espinosa
# REQUIRES USING simAnalyseAuxMP.do GENERATE FOLDER REPORTS
# THIS FILE IS INTENDED FOR USE IN TCLSH STANDALONE DISTRIBUTION
# 
# CALL USING: tclsh simAnalyseMPgather.tcl expSetup [sdfFileName] [(sdfmax|sdfmin)]
# EXAMPLES: tclsh simAnalyseMPgather.tcl " {33 1 12000 11200 10000 10000 1 1 1 1 1F} "
# UPDATED:	  5-feb-2015 only analyse Results of a Packet if the file exists
#					 18-nov-2014 modified


if {$argc < 1} {
		puts "MYERROR: NO PROPER ARGUMENTS"
		puts "CALL USING: tclsh simAnalyseMPgather.tcl expSetup \[sdfFileName\] \[sdfFileName\] \[(sdfmax|sdfmin)\]"
		puts "REMEMBER expSetup is a quoted set of braced \{experiment configurations\} "
} else {
		set i 1
		foreach va $argv {	# caution: sometimes foreach does not respect order of parameters
			set $i $va;
			incr i 
		}
	# puts [info nameofexecutable]
	
	set startFromDir 0
	set pCount 0
	
	set expSetup $1
	set sdfFileName ""
	set sdfmaxmin ""
	if {$argc > 1} { set sdfFileName $2}
	if {$argc > 2} { set sdfmaxmin $3}
	

	# READ RESULTS AND SAVE ANALYSIS AND TIME ELAPSED
	for {set currentExp 0} { $currentExp < [llength $expSetup]} {incr currentExp}  {
			set time0anal [expr [clock seconds]]
			set pars [lindex $expSetup $currentExp]
			set sdfminNow 0
			set allDirs {}
			
			while  {$sdfminNow < 2} {
				if {$sdfFileName == ""}  {
					set newStr "ResultsRTL [string range [lindex $expSetup $currentExp]  0 end]"
				} else {
					if {$sdfminNow == 0 && $sdfmaxmin !="sdfmin"} {
						set newStr "Results [string range [lindex $expSetup $currentExp]  0 end]"
					} else {
						set newStr "Results sdfmin [string range [lindex $expSetup $currentExp]  0 end]"
					}
				}
				regsub -all {\s} $newStr {_} newStr
				
				set iPacket $startFromDir
				while {[file isdirectory [format ./$newStr/Packet%04u $iPacket]]} {
					set nameiPacket [format ./$newStr/Packet%04u $iPacket];
					if {[file exists $nameiPacket/Results.log]}	{lappend allDirs $nameiPacket/Results.log}
					incr iPacket
				}
				
				exec gawk  -f simGatherResultsMP.awk {*}$allDirs > $newStr/GatheredResults.log;
				
				set fd [open $newStr/GatheredResults.log a]
				set analTime [expr [clock seconds]-$time0anal]
				puts $fd "Join Analysis time. Total seconds: $analTime"
				puts $fd "[expr $analTime / 3600] H [expr [expr $analTime % 3600] /60] Min [expr $analTime % 60] Sec"	
				close $fd
				
				incr sdfminNow
				if {$sdfFileName == "" || $sdfmaxmin != ""} { incr sdfminNow}
			}
	}
}
