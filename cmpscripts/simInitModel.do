# MODULE simInitModel.do TO RESET SIMULATION AND ADD REQUIRED SIGNALS TO WATCH LIST
# ADD FIRST OUTPUTS AND THEN STATE VARIABLES AS REQUIRED BY simReadResults.awk
# IF USING PARAMETERS, THEN: (COMPLETE FILE)
# CALL USING: do simInitModel.do topModuleName [signalsToWatchList]
# UPDATED: 12 jan 2014 only first mem of register file shown

if {$argc > 0} {
	# restart -force -nolist -nolog -nowave
	view list
	radix hex
	
	# ADD OUTPUTS
	env /$1/
	# add list *
	# add list   clk
	# add list clk -notrigger address data  # This writes line only when clk changes
	add list   address
	add list   data
	
	# ADD REGISTERS
			# we add sparcv8 special regs
	env /$1/d3/u0/leon3x0/p0/iu/
	add list r.f.pc
	add list r.d.cwp
	add list r.a.ctrl.tt
	add list r.a.et
	add list r.e.icc
	add list r.m.y
	add list r.x.npc
	add list r.w.s.tba
	add list r.w.s.wim
	add list r.w.s.pil
	add list r.w.s.ec
	add list r.w.s.ef
	add list r.w.s.ps
	add list r.w.s.s
	add list r.w.s.asr18
	
	# we add sparcv8 user regs
	virtual signal -env sim:/testbench/d3/u0/leon3x0/rf0/s1/rhu/s1/dp/x0/xc2v/x0/a6/x0/a9/x(0)/r0/x/ramproc {(rfd(8:31) & rfd(128:135)) } rf
	add list /testbench/d3/u0/leon3x0/rf0/s1/rhu/s1/dp/x0/xc2v/x0/a6/x0/a9/x(0)/r0/x/rf
	# env /$1/d3/u0/leon3x0/rf0/s1/rhu/s1/dp/
	# add list x0/xc2v/x0/a6/x0/a9/x(0)/r0/x/ramproc/rfd(0:135)
	# add list x1/xc2v/x0/a6/x0/a9/x(0)/r0/x/ramproc/rfd(0:135)  # NOT INCLUDED TO AVOID DUPLICATION OF INFO
	
	# quietly virtual signal -install /UUT { ( (context /UUT ) (cout_r & sum_r ) )} OUTjoined
	# quietly virtual signal -install /UUT { ( (context /UUT ) (parity_error_0_PWR_1_o_MUX_8_o & parity_error_1_GND_1_o_MUX_7_o ) )} parity_errors
	# add list /UUT/OUTjoined
	# add list /UUT/parity_errors
	# add list /UUT/Mmux_parity_error_0_PWR_1_o_MUX_8_o11
		
	# configure list -delta collapse
	configure list -delta none

# env /$1/
# add list [lindex $2 1]   # to add first element of list argument #2 --COMPLETE---

} else {
	echo "ERROR: NO PROPER ARGUMENTS"
}
