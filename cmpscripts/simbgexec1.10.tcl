# ADAPTED VERSION OF bgexec 1.10 package for Injections
# AUTHOR of modification: J. Espinosa
# EDIT FOR DIFFERENT CLUSTER CONFIGURATIONS
# UPDATED:	23-jul-2015 outputs info now returned from Gaudi nodes
#						1-jul-2015 debugged copy to $DATA of unnecessary folders
#						23-jun-2015 cluster wait with ceil time
#						17-apr-2015 Debugged infinite loop when providing jobname
# 						14-apr-2015 uniqueId includes jobname
#						12-apr-2015 Now call to pCount uses upvar
# 						10-4-15 debugged Rigel clExec, updated clWaitForRemaining with jobName opt. parameter to wait only for a specific job
#					 	9-4-15 recover transcript(s) from remotedir, debugged Gaudi analysis processes, set uniqueId for launch script
#						16-3-15 recover everything from remotedir
#					 	4-03-15 modified clWaitForRemaining
#						26-02-15 Added procedures for cluster execution using Oracle Queue Manager
#						25.02.15 v cross platform modifications for cygwin

# ORIGINAL PACKAGE INFO:
# ################################################################################
 # # Modul    : bgexec1.10.tcl                                                    #
 # # Changed  : 13.09.2014                                                        #
 # # Purpose  : running processes in the background, catching their output via    #
 # #            event handlers                                                    #
 # # Author   : M.Hoffmann                                                        #
 # # To do    : - rewrite using NAMESPACEs                                        #
 # # Hinweise : >&@ and 2>@stdout don't work on Windows. A work around probably   #
 # #            could be using a temporay file. Beginning with Tcl 8.4.7 / 8.5    #
 # #            there is another (yet undocumented) way of redirection: 2>@1.     #
 # # History  :                                                                   #
 # # 19.11.03 v1.0 1st version                                                    #
 # # 20.07.04 v1.1 callback via UPLEVEL                                           #
 # # 08.09.04 v1.2 using 2>@1 instead of 2>@stdout if Tcl >= 8.4.7;               #
 # #               timeout-feature                                                #
 # # 13.10.04 v1.3 bugfix in bgExecTimeout, readHandler is interruptable          #
 # # 18.10.04 v1.4 bugfix: bgExecTimeout needs to be canceled when work is done;  #
 # #               some optimizations                                             #
 # # 14.03.05 v1.4 comments translated to english                                 #
 # # 17.11.05 v1.5 If specidied, a user defined timeout handler `toExit` runs in  #
 # #               case of a timeout to give chance to kill the PIDs given as     #
 # #               arg. Call should be compatible (optional parameter).           #
 # # 23.11.05 v1.6 User can give additional argument to his readhandler.          #
 # # 03.07.07 v1.7 Some Simplifications (almost compatible, unless returned       #
 # #               string where parsed):                                          #
 # #               - don't catch error first then returning error to main...      #
 # # 08.10.07 v1.8 fixed buggy version check!                                     #
 # # 20.02.12 v1.9 Optionally signal EOF to eofHandler.                           #
 # # 13.09.14 v1.10 bugfix: incr myCount later (in case of an (open)error it was  #
 # #                 erranously incremented yet)                                  #
 # # ATTENTION: closing a pipe leads to error broken pipe if the opened process   #
 # #             itself is a tclsh interpreter. Currently I don't know how to     #
 # #             avoid this without killing the process via toExit before closing #
 # #             the pipeline.                                                    #
 # ################################################################################

 package provide simbgexec 1.10

 #-------------------------------------------------------------------------------
 # If the <prog>ram successfully starts, its STDOUT and STDERR is dispatched
 # line by line to the <readHandler> (via bgExecGenericHandler) as last arg.
 # <pCount> holds the number of processes called this way. If a <timeout> is
 # specified (as msecs), the process pipeline will be automatically closed after
 # that duration. If specified, and a timeout occurs, <toExit> is called with
 # the PIDs of the processes right before closing the process pipeline.
 # Returns the handle of the process-pipeline.
 #
 proc bgExec {prog readHandler pCount {timeout 0} {toExit ""} {eofHandler ""}} {
      upvar #0 $pCount myCount
      ## MODIFICATION DUE TO PROBLEMS WITH REDIRECTIONS
	  # if {[info sharedlibextension] == ".dll"} {
			set p "| $prog"
	  # } else {
			# set p [expr {[lindex [lsort -dict [list 8.4.7 [info patchlevel]]] 0] == "8.4.7"?"| $prog 2>@1":"| $prog 2>@stdout"}]
	  # }
      set pH [open $p r]
      fconfigure $pH -blocking 0; # -buffering line (does it really matter?!)
      set tID [expr {$timeout?[after $timeout [list bgExecTimeout $pH $pCount $toExit]]:{}}]
      fileevent $pH readable [list bgExecGenericHandler $pH $pCount $readHandler $tID $eofHandler]
      set myCount [expr {[info exists myCount]?[incr myCount]:1}]
      return $pH
 }
 #-------------------------------------------------------------------------------
 proc bgExecGenericHandler {chan pCount readHandler tID eofHandler} {
      upvar #0 $pCount myCount
      if {[eof $chan]} {
         after cancel $tID;   # empty tID is ignored
         catch {close $chan}; # automatically deregisters the fileevent handler
                              # (see Practical Programming in Tcl an Tk, page 229)
         incr myCount -1
         if {[string length $eofHandler]} {
            catch {uplevel $eofHandler $chan}
         }
      } elseif {[gets $chan line] != -1} {
         # we are not blocked (manpage gets, Practical... page.233)
         lappend readHandler $line
         # if {[catch {uplevel $readHandler}]} {
            # # user-readHandler ended with error -> terminate the processing
            # after cancel $tID
            # catch {close $chan}
            # incr myCount -1
         # }
      }
 }
 #-------------------------------------------------------------------------------
 proc bgExecTimeout {chan pCount toExit} {
      upvar #0 $pCount myCount
      if {[string length $toExit]} {
         catch {uplevel [list $toExit [pid $chan]]}
      }
      catch {close $chan}
      incr myCount -1
 }
 # === PROCEDURES FOR CLUSTERS ================================

proc clExec {prog jobName cluster pCount {nameiPacket ""}} {
	upvar $pCount myCount
	if {$cluster == "Gaudi"} {
		clExecGaudi $prog $jobName $nameiPacket
		incr myCount
	} elseif {$cluster == "Rigel"} {
		clExecRigel $prog $jobName $nameiPacket
		incr myCount
	} else {
		puts "ERROR: Cluster NOT CONFIGURED"
		exit
	}
 }
  
proc clExecGaudi {prog jobName nameiPacket} {
	set uniqueId $jobName[expr rand()]
	set ep [open ./exeScript$uniqueId.sh w]
	puts $ep "#!/bin/sh"
	puts $ep "### Directives for queue manager (modify NAMEOFJOB and USERNAME)"
	puts $ep "# Change name of job:"
	puts $ep "#\$ -N $jobName"
	puts $ep "# Specify a shell:"
	puts $ep "#\$ -S /bin/sh"
	puts $ep "# Pass environment variables to jobs"
	puts $ep "CSCRATCH=[exec pwd]"
	# puts $ep "DATA=/scratch/1/`whoami`/$JOB_ID"  # unsure if sharing this in nodes works well
	puts $ep "DATA=/scratch/1/[exec whoami]/\$JOB_ID"
	if {$nameiPacket != ""} {
		puts $ep "mkdir -p \$DATA/Results/Packet"
		puts $ep "rsync -a \$CSCRATCH/sim* \$DATA"
		puts $ep "rsync -a \$CSCRATCH/$nameiPacket/../FaultFree.lst \$DATA/Results"
		puts $ep "rsync -a \$CSCRATCH/$nameiPacket/../Injections.log \$DATA/Results"
		puts $ep "rsync -a \$CSCRATCH/$nameiPacket/* \$DATA/Results/Packet"
	} else {
		# CAUTION: DO NOT HAVE FINISHED RESULT DIRECTORIES IN THE STRUCTURE
		# puts $ep "rsync -a \$CSCRATCH \$DATA"
		puts $ep "rsync -a --exclude '*ResultsRTL*/' --exclude 'Results/Packet*' --exclude '*.log' \$CSCRATCH/* \$DATA"
	}
	puts $ep "cd \$DATA"
	puts $ep "$prog"
	puts $ep "ls -l * > \$DATA/remotedir$uniqueId"
	# puts $ep "rsync -a \$DATA/remotedir$uniqueId \$CSCRATCH" # debug
	if {$nameiPacket != ""} {
		puts $ep "rsync -a \$DATA/Results/Packet/Results.log \$CSCRATCH/$nameiPacket"
	} else {
		puts $ep "rsync -a --exclude 'savedstate*' \$DATA/Results \$CSCRATCH"
		puts $ep "rsync -a \$DATA/transcript* \$CSCRATCH"
	}
	puts $ep "rsync -a \$DATA/$jobName.e* \$CSCRATCH"
	puts $ep "rsync -a \$DATA/$jobName.o* \$CSCRATCH"
	puts $ep "rm -rf \$DATA"
	close $ep
	exec chmod u+x ./exeScript$uniqueId.sh
	exec qsub -V -l huge ./exeScript$uniqueId.sh
	exec rm ./exeScript$uniqueId.sh
 }
 
 proc clExecRigel {prog jobName nameiPacket} {
	set uniqueId $jobName[expr rand()]
	set ep [open ./exeScript$uniqueId.sh w]
	puts $ep "#!/bin/sh"
	puts $ep "### Directives for queue manager (modify NAMEOFJOB and USERNAME)"
	puts $ep "# Change name of job:"
	puts $ep "#\$ -N $jobName"
	puts $ep "# Parameterize execution time limit and max memory:"
	puts $ep "#\$ -l h_rt=144:00:00,h_vmem=4g" 
	puts $ep "# Specify a shell:"
	puts $ep "#\$ -S /bin/sh"
	puts $ep "CSCRATCH=[exec pwd]"
	# puts $ep "DATA=/scratch/1/`whoami`/$JOB_ID"  # unsure if sharing this in nodes works well
	puts $ep "DATA=\$TMP"
	if {$nameiPacket != ""} {
		puts $ep "mkdir -p \$DATA/Results/Packet"
		puts $ep "rsync \$CSCRATCH/sim* \$DATA"
		puts $ep "rsync -a \$CSCRATCH/$nameiPacket/../FaultFree.lst \$DATA/Results"
		puts $ep "rsync -a \$CSCRATCH/$nameiPacket/../Injections.log \$DATA/Results"
		puts $ep "rsync -a \$CSCRATCH/$nameiPacket/* \$DATA/Results/Packet"
	} else {
		# CAUTION: DO NOT HAVE FINISHED RESULT DIRECTORIES IN THE STRUCTURE
		# puts $ep "rsync -a \$CSCRATCH \$DATA"
		puts $ep "rsync -a --exclude '*ResultsRTL*/' --exclude 'Results/Packet*' --exclude '*.log' \$CSCRATCH/* \$DATA"
	}
	puts $ep "cd \$DATA"
	puts $ep "$prog"
	puts $ep "ls -l * > \$DATA/remotedir$uniqueId"
	puts $ep "rsync -a \$DATA/remotedir$uniqueId \$CSCRATCH"
	if {$nameiPacket != ""} {
		puts $ep "rsync -a \$DATA/Results/Packet/Results.log \$CSCRATCH/$nameiPacket"
	} else {
		puts $ep "rsync -a --exclude 'savedstate*' \$DATA/Results \$CSCRATCH"
	}
	puts $ep "rm -rf \$DATA"
	close $ep
	exec chmod u+x ./exeScript$uniqueId.sh
	exec qsub -V ./exeScript$uniqueId.sh
	exec rm ./exeScript$uniqueId.sh
 }
	
 proc clWaitForRemaining {pCount {jobName ""}} {
	set state "processing"
	while {$state != ""} {
		set state [exec qstat]
		if {$jobName == ""} {
			set pending [expr [llength [split $state "\n"]] -2]
			if {$pending <= $pCount} {return}
			if {[regexp {qw} $state]} {set perJob "9000"} else {set perJob "3000"}				
			after [expr $pending * $perJob]
		} else {
			if {[regexp {[$jobName]} $state]} {
				set plist [split $state "\n"]
				set pidx [lsearch -all -regexp $plist $jobName]
				set pending [llength $pidx]
				if {$pending <= $pCount} {return}
				if {[regexp {qw} [lindex $plist [lindex $pidx end]]]} {set perJob "6000"} else {set perJob "2000"}
				if {[expr $pending * $perJob] > 60000} {after 60000} else {after [expr $pending * $perJob]}
			} else {return}
		}
	}
 }
