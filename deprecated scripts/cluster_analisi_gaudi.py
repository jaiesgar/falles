#!/usr/bin/env python

import subprocess, time, os
#EEMBC_name = ["cacheb"]
#EEMBC_name = ["canrdr","iirflt","puwmod","rspeed","tblook","ttsprk","aifirf","basefp", "a2time","pntrch"]
EEMBC_name = ["iirflt","rspeed","tblook","ttsprk","aifirf","basefp", "a2time","pntrch"]
mode_name = ["soft","permanent"]
#mode_name = ["mode1","mode2","mode3","mode4"]
#mode_name = ["per"]
#frq_name = ["frq1","frq250","frq1000","frq2000"]
#frq_name = ["frq250","frq500","frq1000","frq2000"]
#base_command = "./execute_sim /scratch/nas/1/chernand/VeTeSS/tricore_lockstep/inputs/EEMBC_MODE_ITERATION.in > /scratch/nas/1/chernand/VeTeSS/tricore_lockstep/results/"
# benchmarks = ["./iu/leon3-minimal_canrdr/ResultsRTL_6269_1_1197425_1804440_1804460_1804460_0_0_1_1_1F/Packet00","./iu/leon3-minimal_puwmod/ResultsRTL_6269_1_1206605_2528460_2528490_2528490_0_0_1_1_1F/Packet00", "./data_coverage/leon3-minimal_rspeed10ite/ResultsRTL_6269_1_1243505_904760_904790_904790_0_0_1_1_1F/Packet00" ] 
#benchmarks = ["cmem0/leon3-minimal_membench/ResultsRTL_1773_1_1191045_942440_942490_942490_0_0_1_1_ZF/Packet00" ]
benchmarks = ["cmem0/leon3-minimal_ttsprk/ResultsRTL_1773_1_1222425_2131360_2131390_2131390_0_0_1_1_ZF/Packet00" ]
#benchmarks = ["cmem0/leon3-minimal_puwmod/ResultsRTL_1773_1_1206605_2528460_2528490_2528490_0_0_1_1_0F/Packet00" ]
# injectiontime = ["1197425 ", "1206605 ", "1243505 "]
injectiontime = ["1222425 "]
base_command = "tclsh simAnalyseAuxMP.do "
#base_command = "tclsh simAnalyseAuxMP.do 1222425 \"./iu/leon3-minimal_ttsprk/ResultsRTL_6269_1_1222425_2131360_2131390_2131390_0_0_1_1_1F/Packet00#HIGH##LOW#\""
base_address = "/scratch/nas/1/chernand/DAC2015"
temp_address = "/scratch/nas/1/chernand/DAC2015"

for i in range (0, len(benchmarks)):
  command = base_command + injectiontime[i] + benchmarks[i]
  for dec in range (0,10):
      for uni in range (0,10):
	  command = command + str(dec)+  str(uni)
	  # s = eembc_var+str(iteration)
	  # print s
	  exe_command = command
	  print exe_command
	  queue = subprocess.check_output("qstat")
	  time_sleeping = 1
	  while queue.count("\n") > 1000:
	    time.sleep(time_sleeping)
	    try:
		    queue = subprocess.check_output("qstat")
	    except:
		    print "Qstat has failed"
		    pass
	    time_sleeping += time_sleeping
	    if time_sleeping > 300: time_sleeping = 600
	
	  file_dir = temp_address+"script"+".sh"
	  f = open(file_dir,"w")
	  f.write("#!/bin/sh\n"+
	    "### Directivas para el gestor de colas (modificar los valores NAMEOFJOB y USERNAME, o eliminar las lineas si no se usan)\n"+
	    "# Cambiar el nombre del trabajo\n"+
	    "#$ -N CM_TTSPRKZF\n# Especificar un shell\n"+
	    "#$ -S /bin/sh\n"+
	    "CSCRATCH="+base_address+"\n"+
	    "DATA=data.$JOB_ID\ncd $CSCRATCH\n"+
	    "mkdir $DATA\n"+
	    #"rsync $CSCRATCH/iu/leon3-minimal_ttsprk/ResultsRTL_6269_1_1222425_2131360_2131390_2131390_0_0_1_1_1F/Packet00"+str(dec)+str(uni)+" $DATA\n"+
	    "rsync $CSCRATCH/"+str(benchmarks)+str(dec)+str(uni)+" $DATA\n"+
	    exe_command+"\n"+
	    "rm -rf $DATA\n")
	  f.close()
	  subprocess.call(["chmod","u+x",file_dir])
	  subprocess.call(["qsub","-l","big",file_dir])
	  # subprocess.call(["qsub",file_dir])
	  os.remove(file_dir)
	  #command = command.replace("Packet00"+str(dec)+str(uni),"Packet00"+str(dec)+"#LOW#" 	
	  command = base_command + injectiontime[i] + benchmarks[i]
	  #command = command.replace(frq_name_var, "ACCFRQ")
      #command = command.replace("Packet00"+str(dec),"Packet00"+"#HIGH#")

