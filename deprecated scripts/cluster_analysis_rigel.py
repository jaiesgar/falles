#!/usr/bin/env python

import subprocess, time, os
benchmarks = ["iu/leon3-minimal2w_rspeed/ResultsRTL_6269_1_1107985_854400_854440_854440_0_0_1_1_1F/ResultsRTL_6269_1_1107985_854400_854440_854440_0_0_1_1_1F/", "iu/leon3-minimal2w_rspeed/ResultsRTL_6269_1_1107985_854400_854440_854440_0_0_1_1_0F/ResultsRTL_6269_1_1107985_854400_854440_854440_0_0_1_1_0F/"]
#benchmarks = ["iu/leon3-minimal2w_firFn/ResultsRTL_5246_1_1202645_74140_74200_74200_0_0_1_1_1F/", "iu/leon3-minimal2w_firFn/ResultsRTL_5246_1_1202645_74140_74200_74200_0_0_1_1_0F/", "iu/leon3-minimal2w_firFn/ResultsRTL_5246_1_1202645_74140_74200_74200_0_0_1_1_ZF/"]
#benchmarks = ["iu/leon3-minimal2w_fir2Fn/ResultsRTL_5246_1_1280805_34240_34300_34300_0_0_1_1_0F/","iu/leon3-minimal2w_fir2Fn/ResultsRTL_5246_1_1280805_34240_34300_34300_0_0_1_1_ZF/"]
#benchmarks = ["iu/leon3-minimal2w_matmult/ResultsRTL_5246_1_1211065_295720_295720_295720_0_0_1_1_ZF/"]
#benchmarks = ["iu/leon3-minimal2w_matmult2/ResultsRTL_5246_1_1507685_241820_241880_241880_0_0_1_1_1F/", "iu/leon3-minimal2w_matmult2/ResultsRTL_5246_1_1507685_241820_241880_241880_0_0_1_1_0F/", "iu/leon3-minimal2w_matmult2/ResultsRTL_5246_1_1507685_241820_241880_241880_0_0_1_1_ZF/"]
# injectiontime = ["1197425 ", "1206605 ", "1243505 "]
#injectiontime = ["1211065 "]
injectiontime = ["1107985 ", "1107985 "]
# injectiontime = ["1202645 ","1202645 ","1202645 "]
#injectiontime = ["1279045 ","1279045 ","1279045 "]
base_command = "tclsh simAnalyseAuxMP.do "
base_address = "/scratch/jaiesgar/DAC2015"
temp_address = "/scratch/jaiesgar/DAC2015"

for i in range (0, len(benchmarks)):
  command = base_command + injectiontime[i] + benchmarks[i] + "Packet0"
  localcommand = base_command + injectiontime[i] + "$TMP/Packet"
  for cen in range (0,9):
	for dec in range (0,10):
		  for uni in range (0,10):
			  command = command + str(cen) + str(dec)+  str(uni)
			  exe_command = command
			  print exe_command
			  print localcommand
			  # queue = subprocess.check_output("qstat")
			  # time_sleeping = 1
			  # while queue.count("\n") > 1000:
			  #   time.sleep(time_sleeping)
			  #   try:
			  #     queue = subprocess.check_output("qstat")
			  #   except:
			  # 	    print "Qstat has failed"
			  # 	    pass
			  #   time_sleeping += time_sleeping
			  #   if time_sleeping > 300: time_sleeping = 600
			
			  file_dir = temp_address+"script"+".sh"
			  f = open(file_dir,"w")
			  f.write("#!/bin/sh\n"+
				"### Directivas para el gestor de colas (modificar los valores NAMEOFJOB y USERNAME, o eliminar las lineas si no se usan)\n"+
				"# Cambiar el nombre del trabajo\n"+
				"#$ -N RSPEED_10F\n" +
				"# Parametrizar limite tiempo ejecucion\n" +
				"#$ -l h_rt=120:00:00,h_vmem=4g\n" +
				"# Especificar un shell\n"+
				"#$ -S /bin/sh\n"+
				"CSCRATCH="+base_address+"\n"+
				# "DATA=data.$JOB_ID\n"+
				#"cd $CSCRATCH\n"+
				"cd $TMP\n"+
				#"mkdir $DATA\n"+
				#"rsync $CSCRATCH/iu/leon3-minimal_ttsprk/ResultsRTL_6269_1_1222425_2131360_2131390_2131390_0_0_1_1_1F/Packet00"+str(dec)+str(uni)+" $DATA\n"+
				#"rsync $CSCRATCH/"+str(benchmarks[i])+str(dec)+str(uni)+" $DATA\n"+
				"rsync $CSCRATCH/sim* $TMP\n"+
				"rsync $CSCRATCH/"+ benchmarks[i]+ "FaultFree.lst $TMP\n"+
				"rsync $CSCRATCH/"+ benchmarks[i]+ "Injections.log $TMP\n"+
				"rsync -a $CSCRATCH/"+benchmarks[i]+ "Packet0" +str(cen)+str(dec)+str(uni)+"/* $TMP/Packet\n"+
				# exe_command+"\n")
				localcommand+"\n"+
				# "ls -ltrh $TMP > $CSCRATCH/"+str(benchmarks[i])+"Packet0"+str(cen)+str(dec)+str(uni)+"/direc\n"+
				# "ls -ltrh $TMP/Packet > $CSCRATCH/"+str(benchmarks[i])+"Packet0"+str(cen)+str(dec)+str(uni)+"/direc2\n"+
				"mv -f $TMP/Packet/Results.log $CSCRATCH/"+benchmarks[i]+"Packet0"+str(cen)+str(dec)+str(uni)+"\n"+
				"rm -rf $TMP\n")
			  f.close()
			  subprocess.call(["chmod","u+x",file_dir])
			  # subprocess.call(["qsub","-l","big",file_dir])
			  subprocess.call(["qsub",file_dir])
			  os.remove(file_dir)
			  command = base_command + injectiontime[i] + benchmarks[i] + "Packet0"  
			  #command = command.replace("Packet00"+str(dec)+str(uni),"Packet00"+str(dec)+"#LOW#" )
			  #command = command.replace("Packet00"+str(dec),"Packet00"+"#HIGH#")

