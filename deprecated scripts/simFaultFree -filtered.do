# FAULT-FREE SIMULATION OF MODULE top UNTIL endTime SECONDS, SAVING SIM STATE AT startInjTime IN FILE savedstate.sim
# WITH FILTERING OF FIRST DIGIT OF POSITIONS OF 9 DIGITS
# CALL USING: do simFaultFree.do topModuleName endtimeSim startInjTime 
# UPDATED: 13 oct 2014 Swapped init command to avoid saving pre-injections data

if {$argc == 3} {

run @$3 ns
do {./simInitModel.do} $1
checkpoint ./Results/savedstate.sim
run @$2 ns
write list {./Results/FaultFree.lst}
# The following 2 lines are for filtering 
exec gawk "{print gensub(/(\[0\])(\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\])/, \"\\\\\\2\" , \"g\") }" [format ./Results/FaultFree.lst] > [format ./Results/FaultFree-mod.lst]
exec mv [format ./Results/FaultFree-mod.lst] [format ./Results/FaultFree.lst]

} else {
echo "ERROR: NO PROPER ARGUMENTS"
}
