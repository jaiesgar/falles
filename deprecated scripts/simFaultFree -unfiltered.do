# FAULT-FREE SIMULATION OF MODULE top UNTIL endTime SECONDS, SAVING SIM STATE AT startInjTime IN FILE savedstate.sim
# 
# CALL USING: do simFaultFree.do topModuleName endtimeSim startInjTime 
# UPDATED: 13 oct 2014 Swapped init command to avoid saving pre-injections data

if {$argc == 3} {

run @$3 ns
do {./simInitModel.do} $1
checkpoint ./Results/savedstate.sim
run @$2 ns
write list {./Results/FaultFree.lst}

# THE FOLLOWING CODE IS NOT REQUIRED FOR CORRECT ANALYSIS OF RESULTS, BUT CAN BE USED FOR FILE COMPARISON PURPOSES
# exec cp [format ./Results/FaultFree.lst] [format ./Results/FaultFreeFull.lst]
# exec gawk "{gsub(/ +/,\" \"); if (/^ \[0-9\]+/) print}" [format ./Results/FaultFree.lst] > [format ./Results/FaultFree-mod.lst]
# exec mv [format ./Results/FaultFree-mod.lst] [format ./Results/FaultFree.lst]

} else {
echo "ERROR: NO PROPER ARGUMENTS"
}
