# MODULE simFaults.do TO SIMULATE FAULT FREE AND INJECTED VHDL, DOES SDFMAX & SDFMIN UNLESS SPECIFIED
# AUTHOR: J. Espinosa
# FOR EACH DESIGN EDIT simInitModel.do WITH WATCH LIST OF SIGNALS 
#						  AND EDIT simReadResults.awk TO ADJUST PRESENTATION OF RESULTS
#    OPTIONALLY EDIT simWriteNodes.awk OR simNodes.do TO TWEAK NODES TO INJECT.
# REQUIRED FILES: simInjections4.do , simFaultModel.do, simFaultFree.do, simWriteNodes.awk, simReadResults.awk, simInitModel.do
# 
# CALL IN MODELSIM USING: do simFaults.do topModuleName [endTimeSimNs] [sdfFileName] [(sdfmax|sdfmin)]
# EXAMPLES: do simFaults.do tbx_full_adder_4bit_regERR 200 full_a-tbeg.sdf
#  do simFaults.do testbench
# UPDATED: 27-oct-2014 Auto delete savedstate.sim

# SUPRESS WARNINGS IN PACKAGE NUMERIC_STD (truncations, etc...)
quietly  set NumericStdNoWarnings 1
# DISABLE COMPRESSION OF STATE TO SPEED UP LOADING
set CheckpointCompressMode 0

if {$argc < 1} {
	echo "MYERROR: NO PROPER ARGUMENTS"
	echo "CALL USING: do simFaults.do topModuleName \[endTimeSimNs\] \[sdfFileName\] (sdfmax|sdfmin)"
} else {
	quietly set time0 [expr [clock seconds]];

	set componentToInject "iu"

	set top $1
	set endTime 22000
	set sdfFileName ""
	set sdfmaxmin ""
	if {$argc > 1} { set endTime $2}
	if {$argc > 2} { set sdfFileName $3}
	if {$argc > 3} { set sdfmaxmin $4}
	
	# AUTO EXPERIMENTS LIST SETUP
	# [[startFrom] experiments injections startInjTime lastInj2endTime maxDuration minDuration maxSeparation minSeparation isInjValueConstant isInjDurationConstant faultModel] 
	# SEE simInjections4.do AND simFaultModel.do FOR FAULT MODELS REFERENCE
	#	
	#  ====== FAULT MODELS (read values from simFaultModel.do) ==========
	# RF: 1 or 0 (random) stuck-at. -Apply anywhere, duration until end of experiment-
	# RCF: 1 or 0 (random) pulse. -Apply in combinational logic only, duration limited to injection time-
	# 1F: 1 stuck-at. -Apply anywhere, duration until end of experiment-
	# 1CF: 1 pulse. -Apply in combinational logic only, duration limited to injection time-
	# 0F: 1 stuck-at. -Apply anywhere, duration until end of experiment-
	# 0CF: 1 pulse. -Apply in combinational logic only, duration limited to injection time-
	# XF: Permanent indetermination. -Apply anywhere, duration until end of experiment-
	# XF: Transient indetermination. -Apply anywhere, duration limited to injection time-
	# ZF: Open line. -Apply anywhere, duration until end of experiment-
	# NSD: Bit-flip. -Apply in sequential logic only, duration until next write of element value- 
	
	set expSetup {
				{33 1 12000 11200 10000 10000 0 0 1 1 1F}
			}		
			# {5 1 12000 11200 30 20 0 0 1 1 1F}
				# {200 1 120 40 8 5 0 0 1 1 DS}
				# {200 1 120 40 5 2 0 0 1 1 DS}
				# {200 1 120 40 2 0.5 0 0 1 1 DS}
				# {200 1 120 40 0.5 0.1 0 0 1 1 DS}
				# {200 1 120 40 0.1 0.05 0 0 1 1 DS}
				# {200 2 120 40 2 2 2 0.5 1 1 DS}
				# {200 2 120 40 0.5 0.5 2 0.5 1 1 DS}
				# {200 2 120 40 0.5 0.5 0.5 0.5 1 1 DS}
				# {200 2 120 40 0.1 0.1 0.1 0.1 1 1 DS}
				# {200 1 120 60 60 15 0 0 1 1 DS}
				# {0 138 sum_out(0)}
				# {1 137 sum_out(0)}
	
	# AUTO CREATE LIST OF NODES TO INJECT
	if {$sdfFileName == ""} {
		exec gawk -v firstFile=1 -v nameOfUnit=$componentToInject -f simWriteNodesRTL.awk ./d3u0leon3p0iuwave.do >./simNodes.do
		exec gawk -v lastFile=1 -v nameOfUnit=$componentToInject -f simWriteNodesRTL.awk ./d3u0leon3p0iuunexpanded.do >>./simNodes.do
	} else {
		regexp -indices {\.} $sdfFileName sdfDotPos
		set vhdFileName [string range $sdfFileName 0 [expr [lindex $sdfDotPos 0] -1]]
		regexp -indices {_regERR} $top topUnderPos
		# select main component after par for injection
		set componentToInject [string range $top 4 [expr [lindex $topUnderPos 0] -1]]
		exec gawk -v nameOfUnit=$componentToInject -f simWriteNodes.awk ./$vhdFileName.vhd >./simNodes.do
	}
	
	# AUTOMATED PROCESSING
	for {set currentExp 0} { $currentExp < [llength $expSetup]} {incr currentExp}  {
		
		set pars [lindex $expSetup $currentExp]
		set sdfminNow 0
		while {$sdfminNow < 2} {
		
			# LOAD DESIGN
			if {$sdfFileName == ""} {
				vsim -quiet -t 1ps -voptargs=+acc work.$top
			} else {
				if {$sdfmaxmin != ""} {
					#vsim -t 1ps -voptargs=+acc -$4 /TBX_full_adder_4bit_regERR/UUT/full_adder_4bit_regERR=full_a-tbeg.sdf work.$top	
					vsim -quiet -t 1ps -voptargs=+acc -$sdfmaxmin /$top/UUT/[string range $top 4 end]=$sdfFileName work.$top
				} else {
					if {$sdfminNow == 0} {
							vsim -quiet -t 1ps -voptargs=+acc -sdfmax /$top/UUT/[string range $top 4 end]=$sdfFileName work.$top
						} else {
							vsim -quiet -t 1ps -voptargs=+acc -sdfmin /$top/UUT/[string range $top 4 end]=$sdfFileName work.$top
						}
				}
			}
			
			# RENAME IF PREVIOUS EXECUTION IS PRESENT AND WE ARE NOT CONTINUING EXPERIMENT
			if {[file exists Results]} {
				if {[llength $pars] == 11} {
					file rename ./Results Results[file atime ./Results]
					file mkdir Results
				}
			} else {
				file mkdir Results
			}
			
			# FAULT-FREE SIMULATION OF MODULE top UNTIL endTime SECONDS, SAVING SIM STATE AT startInjTime
			if {[llength $pars] == 12} {
				do {./simFaultFree.do} $top $endTime [lindex $pars 3]
			} else {
				do {./simFaultFree.do} $top $endTime [lindex $pars 2]
			}
			
			## INJECTIONS SIMULATION
			## do {./simInjections4.do} $top $endTime [[startFrom] experiments injections startInjTime lastInj2endTime maxDuration minDuration maxSeparation minSeparation isInjValueConstant isInjDurationConstant faultModel] 
			# obs: separation takes from deactivation of previous injection to injection instant of next injection.
			# do {./simInjections.do} $top $endTime [lindex $pars 1] [lindex $pars 2]
			# do {./simInjections1.do} $top $endTime [lindex $pars 1] [lindex $pars 2]
			# do {./simInjections2.do} $top $endTime
			# do {./simInjections3.do} $top $endTime
			# do {./simInjections3b.do} $top $endTime
			# do {./simInjections3c.do} $top $endTime
			# do {./simInjections4.do} $top $endTime
			# do {./simInjections4.do} $top $endTime 100 4 120 40 0.5 0.5 0.5 0.5 1 1 DS
			eval do {./simInjections4.do} $top $endTime "[lrange $pars 0 end]"
			quit -sim 
			
			# READ RESULTS AND SAVE ANALYSIS AND TIME ELAPSED
			set time1 [expr [clock seconds]];
			exec gawk -v startInjTime=[lindex $pars 1] -f simReadResults.awk ./Results/* >./Results/Results.log
			set fd1 [open ./Results/Results.log a]
			set expTime1 [expr [clock seconds]-$time1]
			puts $fd1 "Analysis time. Total seconds: $expTime1"		
			puts $fd1 "[expr $expTime1 / 3600] H [expr [expr $expTime1 % 3600] /60] Min [expr $expTime1 % 60] Sec"
			close $fd1
			
			# AUTOPROC RENAME DIRS
			if {$sdfFileName == ""}  {
				set newStr "ResultsRTL [string range [lindex $expSetup $currentExp]  0 end]"
			} else {
				if {$sdfminNow == 0 && $sdfmaxmin !="sdfmin"} {
					set newStr "Results [string range [lindex $expSetup $currentExp]  0 end]"
				} else {
					set newStr "Results sdfmin [string range [lindex $expSetup $currentExp]  0 end]"
				}
			}
			regsub -all {\s} $newStr {_} newStr
			file delete ./Results/savedstate.sim
			file rename ./Results ./$newStr
		
		# REPEAT ONLY IF SDF AND NOT SPECIFIED
		incr sdfminNow
		if {$sdfFileName == "" || $sdfmaxmin != ""} { incr sdfminNow}
		}
	}

echo "Elapsed time. Total seconds:" [expr [clock seconds]-$time0]
echo "Hours:" [expr [expr [clock seconds]-$time0] / 3600] "Minutes:" [expr [expr [expr [clock seconds]-$time0] % 3600] /60] "Seconds:" [expr [expr [clock seconds]-$time0] % 60]
}

# RESTORE WARNINGS IN PACKAGE NUMERIC_STD (truncations, etc...)
# set NumericStdNoWarnings 0
# RE-ENABLE COMPRESSION OF STATE TO SPEED UP LOADING
# set CheckpointCompressMode 1


