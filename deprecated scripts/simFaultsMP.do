# MODULE simFaultsMP.do TO SIMULATE, USING MULTIPLE PACKETS, FAULT FREE AND INJECTED VHDL, DOES SDFMAX & SDFMIN UNLESS SPECIFIED
# FOR EACH DESIGN EDIT simInitModel.do WITH WATCH LIST OF SIGNALS 
#						  AND EDIT simReadResults.awk TO ADJUST PRESENTATION OF RESULTS
#    OPTIONALLY EDIT simWriteNodes.awk OR simNodes.do TO TWEAK NODES TO INJECT.
# REQUIRED FILES: simInjections4.do , simFaultModel.do, simFaultFree.do, simWriteNodes.awk, simReadResults.awk, simInitModel.do, simAnalyseAuxMP.do
# 
# CALL IN MODELSIM USING: do simFaultsMP.do topModuleName [endTimeSimNs] [sdfFileName] [(sdfmax|sdfmin)]
# CALL IN CONSOLE USING: vsim -c -modelsimini modelsim.ini -do "do simFaultsMP.do topModuleName [endTimeSimNs] [sdfFileName] [(sdfmax|sdfmin)]"
# EXAMPLES: do simFaults.do tbx_full_adder_4bit_regERR 200 full_a-tbeg.sdf
#  do simFaults.do testbench
# UPDATED: 3-feb-2015 Changed to simGenerateNodes.do instead of simWriteNodesRTL.do, and componentToInject needs now full route
#					10-dec-2014 Check simNodes file for correct generation

# SUPRESS WARNINGS IN PACKAGE NUMERIC_STD (truncations, etc...)
quietly set NumericStdNoWarnings 1
# DISABLE COMPRESSION OF STATE TO SPEED UP LOADING
quietly set CheckpointCompressMode 0

if {$argc < 1} {
	echo "MYERROR: NO PROPER ARGUMENTS"
	echo "CALL USING: do simFaults.do topModuleName \[endTimeSimNs\] \[sdfFileName\] \[(sdfmax|sdfmin)\]"
} else {

	set time0 [expr [clock seconds]];

	# EDIT ACCORDINGLY TO DESIGN AND COMPUTING PLATFORM ###
	# IF MULTIPROCESS ANALYSIS IS DESIRED, DO IT SEPARATELY FROM TCLSH
	set componentToInject "/testbench/d3/u0/leon3x0/p0/iu"
	set nrPackets 4
	set doAnalysis 0
	#######################################################
	
	set top $1
	set endTime 17000
	set sdfFileName ""
	set sdfmaxmin ""
	if {$argc > 1} { set endTime $2}
	if {$argc > 2} { set sdfFileName $3}
	if {$argc > 3} { set sdfmaxmin $4}
	
	# AUTO EXPERIMENTS LIST SETUP 
	# [[startFrom] experiments injections startInjTime lastInj2endTime maxDuration minDuration maxSeparation minSeparation isInjValueConstant isInjDurationConstant faultModel] 
	# SEE simInjections4.do AND simFaultModel.do FOR FAULT MODELS REFERENCE
	#	
	#  ====== FAULT MODELS (read values from simFaultModel.do) ==========
	# RF: 1 or 0 (random) stuck-at. -Apply anywhere, duration until end of experiment-
	# RCF: 1 or 0 (random) pulse. -Apply in combinational logic only, duration limited to injection time-
	# 1F: 1 stuck-at. -Apply anywhere, duration until end of experiment-
	# 1CF: 1 pulse. -Apply in combinational logic only, duration limited to injection time-
	# 0F: 1 stuck-at. -Apply anywhere, duration until end of experiment-
	# 0CF: 1 pulse. -Apply in combinational logic only, duration limited to injection time-
	# XF: Permanent indetermination. -Apply anywhere, duration until end of experiment-
	# XF: Transient indetermination. -Apply anywhere, duration limited to injection time-
	# ZF: Open line. -Apply anywhere, duration until end of experiment-
	# NSD: Bit-flip. -Apply in sequential logic only, duration until next write of element value- 
	
	set expSetup {
				{10 1 12000 5000 10 10 0 0 1 1 XF}	
			}		
				# {500 1 12000 11200 10000 10000 0 0 1 1 0F}
				# {200 1 120 40 8 5 0 0 1 1 DS}
				# {200 1 120 40 5 2 0 0 1 1 DS}
				# {200 1 120 40 2 0.5 0 0 1 1 DS}
				# {200 1 120 40 0.5 0.1 0 0 1 1 DS}
				# {200 1 120 40 0.1 0.05 0 0 1 1 DS}
				# {200 2 120 40 2 2 2 0.5 1 1 DS}
				# {200 2 120 40 0.5 0.5 2 0.5 1 1 DS}
				# {200 2 120 40 0.5 0.5 0.5 0.5 1 1 DS}
				# {200 2 120 40 0.1 0.1 0.1 0.1 1 1 DS}
				# {200 1 120 60 60 15 0 0 1 1 DS}
				# {0 138 sum_out(0)}
				# {1 137 sum_out(0)}
	
	
	
	# AUTOMATED PROCESSING OF EXPERIMENTS LIST SETUP
	for {set currentExp 0} { $currentExp < [llength $expSetup]} {incr currentExp}  {
		
		set pars [lindex $expSetup $currentExp]
		set sdfminNow 0
		while {$sdfminNow < 2} {
		
			# LOAD DESIGN
			if {$sdfFileName == ""} {
				vsim -quiet -t 1ps -vopt -voptargs=+acc work.$top
			} else {
				if {$sdfmaxmin != ""} {
					#vsim -t 1ps -voptargs=+acc -$4 /TBX_full_adder_4bit_regERR/UUT/full_adder_4bit_regERR=full_a-tbeg.sdf work.$top	
					vsim -c -quiet -t 1ps -voptargs=+acc -$sdfmaxmin /$top/UUT/[string range $top 4 end]=$sdfFileName work.$top
				} else {
					if {$sdfminNow == 0} {
							vsim -quiet -t 1ps -voptargs=+acc -sdfmax /$top/UUT/[string range $top 4 end]=$sdfFileName work.$top
						} else {
							vsim -quiet -t 1ps -voptargs=+acc -sdfmin /$top/UUT/[string range $top 4 end]=$sdfFileName work.$top
						}
				}
			}
			
			# AUTO CREATE AND CHECK LIST OF NODES TO INJECT IF NOT ALREADY PRESENT
			if {[file exists simNodes.do] == 0} {
				if {$sdfFileName == ""} {
					do {./simGenerateNodes.do} $componentToInject
					# exec gawk -v firstFile=1 -v nameOfUnit=$componentToInject -f simWriteNodesRTL.awk ./d3u0leon3p0iuwave.do >./simNodes.do
					# exec gawk -v lastFile=1 -v nameOfUnit=$componentToInject -f simWriteNodesRTL.awk ./d3u0leon3p0iuunexpanded.do >>./simNodes.do
				} else {
					regexp -indices {\.} $sdfFileName sdfDotPos
					set vhdFileName [string range $sdfFileName 0 [expr [lindex $sdfDotPos 0] -1]]
					regexp -indices {_regERR} $top topUnderPos
					# select main component after par for injection
					set componentToInject [string range $top 4 [expr [lindex $topUnderPos 0] -1]]
					exec gawk -v nameOfUnit=$componentToInject -f simWriteNodes.awk ./$vhdFileName.vhd >./simNodes.do
				}
			}
			if {[exec head simNodes.do | gawk "{if (NR==8) print \"Valid\";}"] != "Valid"} {echo \"MYERROR: GENERATED NODES FILE IS EMPTY\"; abort; break}

			
			# RENAME IF PREVIOUS EXECUTION IS PRESENT AND WE ARE NOT CONTINUING EXPERIMENT
			if {[file exists Results]} {
				if {[llength $pars] == 11} {
					file rename ./Results Results[file atime ./Results]
					file mkdir Results
				}
			} else {
				file mkdir Results
			}
			
			# FAULT-FREE SIMULATION OF MODULE top UNTIL endTime SECONDS, SAVING SIM STATE AT startInjTime
			if {[llength $pars] == 12} {
				do {./simFaultFree.do} $top $endTime [lindex $pars 3]
			} else {
				do {./simFaultFree.do} $top $endTime [lindex $pars 2]
			}
			
			## INJECTIONS SIMULATION
			## do {./simInjections4.do} $top $endTime [[startFrom] experiments injections startInjTime lastInj2endTime maxDuration minDuration maxSeparation minSeparation isInjValueConstant isInjDurationConstant faultModel] 
			# obs: separation takes from deactivation of previous injection to injection instant of next injection.
			# do {./simInjections.do} $top $endTime
			# do {./simInjections1.do} $top $endTime
			# do {./simInjections2.do} $top $endTime
			# do {./simInjections3.do} $top $endTime
			# do {./simInjections3b.do} $top $endTime
			# do {./simInjections3c.do} $top $endTime
			# do {./simInjections4.do} $top $endTime
			# do {./simInjections4.do} $top $endTime 100 4 120 40 0.5 0.5 0.5 0.5 1 1 DS
			eval do {./simInjections4.do} $top $endTime "[lrange $pars 0 end]"  $nrPackets
			quit -sim 
			
			
			# AUTOPROC RENAME DIRS
			if {$sdfFileName == ""}  {
				set newStr "ResultsRTL [string range [lindex $expSetup $currentExp]  0 end]"
			} else {
				if {$sdfminNow == 0 && $sdfmaxmin !="sdfmin"} {
					set newStr "Results [string range [lindex $expSetup $currentExp]  0 end]"
				} else {
					set newStr "Results sdfmin [string range [lindex $expSetup $currentExp]  0 end]"
				}
			}
			regsub -all {\s} $newStr {_} newStr
			file delete ./Results/savedstate.sim
			file rename ./Results ./$newStr
		
		# REPEAT ONLY IF SDF AND NOT SPECIFIED
		incr sdfminNow
		if {$sdfFileName == "" || $sdfmaxmin != ""} { incr sdfminNow}
		}
	}
	
	if  {$doAnalysis == 1} {
		do simAnalyseMP.do $expSetup $sdfFileName $sdfmaxmin
		
	}


echo "Elapsed time. Total seconds:" [expr [clock seconds]-$time0]
echo "Hours:" [expr [expr [clock seconds]-$time0] / 3600] "Minutes:" [expr [expr [expr [clock seconds]-$time0] % 3600] /60] "Seconds:" [expr [expr [clock seconds]-$time0] % 60]
}

# RESTORE WARNINGS IN PACKAGE NUMERIC_STD (truncations, etc...)
# set NumericStdNoWarnings 0
# RE-ENABLE COMPRESSION OF STATE TO SPEED UP LOADING
# set CheckpointCompressMode 1


