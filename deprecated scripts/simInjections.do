# INJECTED SIMULATION OF MODULE top UNTIL time NANOSECONDS
# ROUND 0
# FAULT MODELS (from simFaultModel.do):
# AS: 1 or 0 stuck-at,   AP: 1 or 0 pulse
# BS: 1 stuck-at,   BP: 1 pulse
# CS: 0 stuck-at   CP: 0 pulse
# D: bit-flip (transient)  DS: bit-flip (permanent, only for debug)
# ES: perm indetermination,   EP: trans indetermination
#

if {$argc ==4}  {

#LOAD FILE WITH IMPLEMENTED FAULT MODELS
source simFaultModel.do

set fd [open ./Results/Injections.log w]

set faultModel DS
set injTime $3
set injNodes $4
set maxDuration 8
set minDuration 0.01

	puts $fd "== INJECTIONS OF $maxDuration NS MAXDURATION, $minDuration NS MINDURATION and INJECTED IN THIS NODE: $injNodes \n====="
	
	# LOOP TO INIT SIMULATION, INJECT AND WRITE RESULTS
	set limit 100
	set injDuration $maxDuration
	for {set i 0} {$i < $limit} {incr i} {
		do {./simInitModel.do} $1
		run @$injTime ns
		set injValue [FindInjValue $faultModel /tbx_full_adder_4bit_regerr/UUT/$injNodes]
		force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/$injNodes $injValue 0 -cancel [expr $injDuration] ns
		puts $fd [format "NUMBER $i INJECTION, FORCES $injValue FOR %.5f IN MOMENT %.5f AND NODE $injNodes" $injDuration $injTime]
		echo [format "NUMBER $i INJECTION, FORCES $injValue FOR %.5f IN MOMENT %.5f AND NODE $injNodes" $injDuration $injTime] 
		run @$2 ns
		write list [format ./Results/Fault%4u.lst $i]

		# STEP BETWEEN EACH SIMULATION
		set injDuration [expr $injDuration*0.5]
		if {$injDuration < $minDuration} {set i $limit}
	}
# Injection no. 1
# set injDuration 0.1
# do {./simInitModel.do} $1
# run @138 ns
# # run @[$2 -$injTime] ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(0) 0 0 -cancel {10ns}
# run @$2 ns
# write list {./Results/Fault00000.lst}

# # Injection no. 2
# set injDuration 1
# do {./simInitModel.do} $1
# run @138 ns
# set injValue [FindInjValue $faultModel $injNode]
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(0) 0 0 -cancel {5ns}
# run @$2 ns
# write list {./Results/Fault00001.lst}



# WE CLOSE FILE DESCRIPTOR
close $fd
} else {
	echo "MYERROR: NO PROPER ARGUMENTS"
}

