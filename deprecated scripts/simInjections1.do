# INJECTED SIMULATION OF MODULE top UNTIL time NANOSECONDS
# ROUND 1,  INTERMITTENT TEST
# FAULT MODELS (from simFaultModel.do):
# AS: 1 or 0 stuck-at,   AP: 1 or 0 pulse
# BS: 1 stuck-at,   BP: 1 pulse
# CS: 0 stuck-at   CP: 0 pulse
# D: bit-flip (transient)  DS: bit-flip (permanent, only for debug)
# ES: perm indetermination,   EP: trans indetermination
#

if {$argc ==4}  {

#LOAD FILE WITH IMPLEMENTED FAULT MODELS
source simFaultModel.do
set fd [open ./Results/Injections.log w]

set faultModel DS
set injTime $3
set injNodes $4
set maxSeparation 5
set minSeparation 0.01
set injDuration 0.5

	puts $fd "== INJECTIONS OF $injDuration NS DURATION, $maxSeparation NS MAXSEPARATION and $minSeparation NS MINSEPARATION and INJECTED IN THIS NODE: $injNodes \n====="
	
	# LOOP TO INIT SIMULATION, INJECT AND WRITE RESULTS
	set limit 100
	set injSeparation $maxSeparation
	for {set i 0} {$i < $limit} {incr i} {
		do {./simInitModel.do} $1
		run @$injTime ns
		set injValue [FindInjValue $faultModel /tbx_full_adder_4bit_regerr/UUT/$injNodes]
		force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/$injNodes $injValue 0 -cancel [expr $injDuration] ns
		puts $fd [format "NUMBER $i INJECTION, FORCES $injValue FOR %.5f IN MOMENT %.5f AND NODE $injNodes" $injDuration $injTime]
		echo [format "NUMBER $i INJECTION, FORCES $injValue FOR %.5f IN MOMENT %.5f AND NODE $injNodes" $injDuration $injTime] 
		run [expr $injDuration + $injSeparation] ns
		force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/$injNodes $injValue 0 -cancel [expr $injDuration] ns
		puts $fd [format "NUMBER $i INJECTION 2, FORCES $injValue FOR %.5f AFTER %.5f" $injDuration $injSeparation]
		echo [format "NUMBER $i INJECTION 2, FORCES $injValue FOR %.5f AFTER %.5f" $injDuration $injSeparation]
		run @$2 ns
		write list [format ./Results/Fault%4u.lst $i]

		# STEP BETWEEN EACH SIMULATION
		set injSeparation [expr $injSeparation*0.5]
		if {$injSeparation < $minSeparation} {set i $limit}
	}



# WE CLOSE FILE DESCRIPTOR
close $fd
} else {
	echo "MYERROR: NO PROPER ARGUMENTS"
}

