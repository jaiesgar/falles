# INJECTED SIMULATION OF MODULE top UNTIL time NANOSECONDS
# ROUND 2: Table
if {$argc == 2}  {

# source {./Inject_Commands.MTS}


# Injection no. 1
do {./simInitModel.do} $1
run @136 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {2ns}
run @$2 ns
write list {./Results/Fault00000.lst}

# # Injection no. 2
set injDuration 1
do {./simInitModel.do} $1
run @135 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(0) 0 0 -cancel {13ns}
run @$2 ns
write list {./Results/Fault00001.lst}

# Injection no. 3
do {./simInitModel.do} $1
run @138 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00002.lst}

# # Injection no. 4
do {./simInitModel.do} $1
run @138 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.49ns}
run 1 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.49ns}
run @$2 ns
write list {./Results/Fault00003.lst}

# # Injection no. 5
do {./simInitModel.do} $1
run @141 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {2.4ns}
run @$2 ns
write list {./Results/Fault00004.lst}

# Injection no. 6
do {./simInitModel.do} $1
run @138 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/comb_errors_2/O 1 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00005.lst}

# # Injection no. 7
# do {./simInitModel.do} $1
# run @[expr $injTime] ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.5ns}
# run 0.505 ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.5ns}
# run @$2 ns
# write list {./Results/Fault00006.lst}

# # Injection no. 8
# do {./simInitModel.do} $1
# run @[expr $injTime] ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.5ns}
# run 0.501 ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.5ns}
# run @$2 ns
# write list {./Results/Fault00007.lst}

# # # Injection no. 9
# do {./simInitModel.do} $1
# run @[expr $injTime] ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.5ns}
# run 5.5 ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.5ns}
# run @$2 ns
# write list {./Results/Fault00008.lst}

# # Injection no. 10
# do {./simInitModel.do} $1
# run @[expr $injTime] ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.5ns}
# run 5.5 ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {0.5ns}
# run @$2 ns
# write list {./Results/Fault00009.lst}





} else {
	echo "ERROR: NO PROPER ARGUMENTS"
}



# # CREATE FAULTS IN COMBINATIONAL OUTPUTS sdfmin
# # TRAN 0 
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 1 0 -cancel {1ns}
# # bookmark add wave "injT0"  "[expr $now - 10] [expr $now + 20]" 0
# # wave cursor add -time $now -name {sdfmin injT0 1ns} -lock 1
# run 20 ns
# # INTERM 1
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {1ns}
# # bookmark add wave "injI1"  "[expr $now - 10] [expr $now + 24]" 0
# # wave cursor add -time $now -name {sdfmin injI1 1ns +pause 0.5 +1ns} -lock 1
# run 2 ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {1ns}
# run 20 ns
# # PERM 2
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {$2ns}
# # bookmark add wave "injP2"  "[expr $now - 10] [expr $now + 220]" 0
# # wave cursor add -time $now -name {sdfmin injP2 $2ns} -lock 1
# run 220 ns
