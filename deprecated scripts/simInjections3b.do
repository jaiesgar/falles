# INJECTED SIMULATION OF MODULE top UNTIL time NANOSECONDS
# ROUND 3 All locations, injecting intermittents
if {$argc == 2}  {

expr srand(0)


set injTime [expr rand()*100 + 120]
set injDuration [expr rand()*100 + 120]

  
	    # instfull_adder_4bit_Madd_n0004_Madd_cy_2_1 : STD_LOGIC; -- AKA:instfull_adder_4bit/Madd_n0004_Madd_cy<2>1
	    # instfull_adder_4bit_Madd_n0004_Madd_cy_2_1_F7BMUX_OUT : STD_LOGIC; -- AKA:instfull_adder_4bit/Madd_n0004_Madd_cy<2>1.F7BMUX.OUT
	   # instfull_adder_4bit_Madd_n0004_Madd_cy_2_1_D6LUT_O6 : STD_LOGIC; -- AKA:instfull_adder_4bit/Madd_n0004_Madd_cy<2>1.D6LUT.O6
	    # instfull_adder_4bit_Madd_n0004_Madd_cy_2_1_C6LUT_O6 : STD_LOGIC; -- AKA:instfull_adder_4bit/Madd_n0004_Madd_cy<2>1.C6LUT.O6
	   
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {0.49ns}
# run 1 ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {0.49ns}

# Injection no. 1
do {./simInitModel.do} $1
run @137 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/instfull_adder_4bit_Madd_n0004_Madd_cy_2_1 0 0 -cancel {0.5ns}
run 2 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/instfull_adder_4bit_Madd_n0004_Madd_cy_2_1 0 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00000.lst}

# # Injection no. 2
do {./simInitModel.do} $1
run @137 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/instfull_adder_4bit_Madd_n0004_Madd_cy_2_1_F7BMUX_OUT 0 0 -cancel {0.5ns}
run 2 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/instfull_adder_4bit_Madd_n0004_Madd_cy_2_1_F7BMUX_OUT 0 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00001.lst}

# Injection no. 3
do {./simInitModel.do} $1
run @137 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/instfull_adder_4bit_Madd_n0004_Madd_cy_2_1_D6LUT_O6 0 0 -cancel {0.5ns}
run 2 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/instfull_adder_4bit_Madd_n0004_Madd_cy_2_1_D6LUT_O6 0 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00002.lst}

# Injection no. 4
do {./simInitModel.do} $1
run @137 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/instfull_adder_4bit_Madd_n0004_Madd_cy_2_1_C6LUT_O6 0 0 -cancel {0.5ns}
run 2 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/instfull_adder_4bit_Madd_n0004_Madd_cy_2_1_C6LUT_O6 0 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00003.lst}

# # Injection no. 5
do {./simInitModel.do} $1
run @137 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(0) 0 0 -cancel {0.5ns}
run 2 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(0) 0 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00004.lst}

# Injection no. 6
do {./simInitModel.do} $1
run @137 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(1) 0 0 -cancel {0.5ns}
run 2 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(1) 0 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00005.lst}

# Injection no. 7
do {./simInitModel.do} $1
run @137 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(2) 0 0 -cancel {0.5ns}
run 2 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(2) 0 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00006.lst}

# Injection no. 8
do {./simInitModel.do} $1
run @137 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {0.5ns}
run 2 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00007.lst}

# Injection no. 9
do {./simInitModel.do} $1
run @137 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/cout_out 0 0 -cancel {0.5ns}
run 2 ns
force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/cout_out 0 0 -cancel {0.5ns}
run @$2 ns
write list {./Results/Fault00008.lst}



} else {
	echo "ERROR: NO PROPER ARGUMENTS"
}



# # CREATE FAULTS IN COMBINATIONAL OUTPUTS sdfmin
# # TRAN 0 
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {1ns}
# # bookmark add wave "injT0"  "[expr $now - 10] [expr $now + 20]" 0
# # wave cursor add -time $now -name {sdfmin injT0 1ns} -lock 1
# run 20 ns
# # INTERM 1
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {1ns}
# # bookmark add wave "injI1"  "[expr $now - 10] [expr $now + 24]" 0
# # wave cursor add -time $now -name {sdfmin injI1 1ns +pause 0.5 +1ns} -lock 1
# run 2 ns
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {1ns}
# run 20 ns
# # PERM 2
# force -freeze sim:/tbx_full_adder_4bit_regerr/UUT/sum_out(3) 0 0 -cancel {$2ns}
# # bookmark add wave "injP2"  "[expr $now - 10] [expr $now + 220]" 0
# # wave cursor add -time $now -name {sdfmin injP2 $2ns} -lock 1
# run 220 ns
