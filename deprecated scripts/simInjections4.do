# INJECTED experiments SIMULATIONS OF MODULE top UNTIL time NANOSECONDS
# ROUND 4 **LINEAR** injection from set of Nodes and injection time between startInjTime and endSimulation-lastInj2endTime, with a duration between minDuration and maxDuration ns, and with a separtion between minSeparation and maxSeparation
#
#  ====== FAULT MODELS (read values from simFaultModel.do) ==========
# RF: 1 or 0 (random) stuck-at. -Apply anywhere, duration until end of experiment-
# RCF: 1 or 0 (random) pulse. -Apply in combinational logic only, duration limited to injection time-
# 1F: 1 stuck-at. -Apply anywhere, duration until end of experiment-
# 1CF: 1 pulse. -Apply in combinational logic only, duration limited to injection time-
# 0F: 1 stuck-at. -Apply anywhere, duration until end of experiment-**
# 0CF: 1 pulse. -Apply in combinational logic only, duration limited to injection time-
# XF: Permanent indetermination. -Apply anywhere, duration until end of experiment-
# XF: Transient indetermination. -Apply anywhere, duration limited to injection time-
# ZF: Open line. -Apply anywhere, duration until end of experiment-
# NSD: Bit-flip. -Apply in sequential logic only, duration until next write of element value- 
#
# CALL: do {./simInjections4.do} topModule endTime [[startFrom] experiments injections startInjTime lastInj2endTime maxDuration minDuration maxSeparation minSeparation isInjValueConstant isInjDurationConstant faultModel] 
# UPDATED: 	24-feb-2015 Added transcript off and on in source Nodes to increase compatibility
#							23-dec-2014 Removed extra spaces in injection files
#

# SET BEGINNING OF EXPERIMENT
quietly set time4 [expr [clock seconds]]

#LOAD FILE WITH IMPLEMENTED FAULT MODELS
source simFaultModel.do

# LOAD FILE WITH INJECTION NODES
source simNodes.do

if {$argc >1}  {
		set topModule $1; set endTime $2;
	if {$argc == 2} {
		# ALL UNITS IN NANOSECONDS
		set experiments 50
		set injections 1
		set endResetTime 120
		set lastInj2endTime 40
		set maxDuration 60
		set minDuration 10
		set maxSeparation 5
		set minSeparation 0
		set isInjValueConstant 0
		set isInjDurationConstant 0
		set faultModel 1F
		
	} else {
		# to check if there's a startFrom parameter
		set startFrom 0;
		if { $argc == 15} {
			# case of resuming in MP
			set startFrom $3; shift
			set experiments $3; set injections $4; set endResetTime $5; set lastInj2endTime $6; set maxDuration $7; set minDuration $8; set maxSeparation $9
			shift; shift; shift; shift;  	# to move parameters 4 positions to the left, substracts 4 to argc
			set minSeparation $6; set isInjValueConstant $7; set isInjDurationConstant $8; set faultModel $9
		} elseif { $argc == 14 } {
			set three $3; set four $4; shift; shift;shift;shift;
			if { [regexp {^[0-9]+$} $9]} {
				# case of resuming in Single Process
				set startFrom $three;
				set experiments $four; set injections $1; set endResetTime $2; set lastInj2endTime $3; set maxDuration $4; set minDuration $5; set maxSeparation $6
				set minSeparation $7; set isInjValueConstant $8; set isInjDurationConstant $9; 
				shift; set faultModel $9
			} else {
				# case of starting from 0 in MP
				set experiments $three; set injections $four; set endResetTime $1; set lastInj2endTime $2; set maxDuration $3; set minDuration $4; set maxSeparation $5
				set minSeparation $6; set isInjValueConstant $7; set isInjDurationConstant $8; set faultModel $9
			}
		} else {
			# case of starting from 0 in MP
			set experiments $3; set injections $4; set endResetTime $5; set lastInj2endTime $6; set maxDuration $7; set minDuration $8; set maxSeparation $9
			shift; shift; shift; shift
			set minSeparation $6; set isInjValueConstant $7; set isInjDurationConstant $8; set faultModel $9
		}
		if {$argc > 9} {
			shift; set nrProcesses $9
		} else { set nrProcesses 1;}
	}

	set randSeed 0;  # change for new distribution
	quietly expr srand($randSeed)
	
	set maxPacketSize [expr int ([expr ceil ([expr $experiments / $nrProcesses.0])])];
	if {[expr $startFrom % $maxPacketSize] == 0 } {set iPacket [expr [expr $startFrom / $maxPacketSize] - 1];} else { set iPacket [expr $startFrom / $maxPacketSize]; }
	
	# OPEN OR APPEND TO PREVIOUS FILE
	if {$startFrom == 0} {
		set fd [open ./Results/Injections.log w]
		puts $fd "== $experiments INJECTIONS OF $maxDuration NS MAXDURATION,  $minDuration MINDURATION, $maxSeparation NS MAXSEPARATION, $minSeparation NS MINSEPARATION, $faultModel FAULTMODEL and INJECTED IN ANY OF THESE NODES: === $injNodes \n====="
	} else {
		set fd [open ./Results/Injections.log a]
	}
	
	# LOOP TO INIT SIMULATION, INJECT AND WRITE RESULTS
onbreak { write transcript errlog.log;	resume	}  # to continue in case of failure type assertion
onerror {  write transcript errlog.log;	resume	}  # to continue in case of failure type assertion
for {set i 0} {$i < $experiments} {incr i} {
	set injDuration [expr [expr [expr rand()] * [expr $maxDuration - $minDuration]] + $minDuration]
	set injSeparation [expr [expr [expr rand()] * [expr $maxSeparation - $minSeparation]] + $minSeparation]
	set injTime [expr [expr [expr rand()]*[expr $endTime - $lastInj2endTime - $endResetTime]] + $endResetTime]
	set injNode [lindex $injNodes $i]
	# CHECK IF SELECTED NODE IS NOT A STRUCT OR BUS, OTHERWISE SELECT ANOTHER
	set ok 1
	if {[find signals ${injNode}.*] != "" || [find signals ${injNode}(*)] != ""} {
		set ok 0
	}
	if {$ok == 1} {
	
		if {$startFrom == 0} { 												# to skip until we want to write the packet
			# do {./simInitModel.do} $topModule    # only if we are not restoring state of simulation as in next line
			restore ./Results/savedstate.sim
			run @[expr $injTime] ns
			set injValue [FindInjValue $faultModel $injNode]
			if {[regexp {F$} $faultModel]} {
				puts $fd [format "NUMBER $i INJECTION, FORCES $injValue FOR %.2f IN MOMENT %.2f AND NODE $injNode" $injDuration $injTime]
				echo [format "NUMBER $i INJECTION, FORCES $injValue FOR %.2f IN MOMENT %.2f AND NODE $injNode" $injDuration $injTime] 
				force -freeze sim:$injNode $injValue 0 -cancel [expr $injDuration] ns
			} elseif {[regexp {D$} $faultModel]} {
				puts $fd [format "NUMBER $i INJECTION, DEPOSITS $injValue FOR %.2f IN MOMENT %.2f AND NODE $injNode" $injDuration $injTime] 
				echo [format "NUMBER $i INJECTION, DEPOSITS $injValue FOR %.2f IN MOMENT %.2f AND NODE $injNode" $injDuration $injTime] 
				force -deposit sim:$injNode $injValue 0 -cancel [expr $injDuration] ns
			}
		}
		# #  FOR FURTHER INJECTIONS
		if {$injections > 1} {
			for {set remaining [expr $injections - 1] } { $remaining > 0} {incr remaining -1} {
					if {$isInjDurationConstant == 0} { set injDuration2 [expr [expr rand()] * $maxDuration]  } else { set injDuration2 $injDuration; }
					if {$startFrom == 0} { 												# to skip until we want to write the packet
						run [expr $injDuration + $injSeparation] ns
						if {$isInjValueConstant == 0} { set injValue2 [FindInjValue $faultModel $injNode]  } else { set injValue2 $injValue; }
						if {[regexp {F$} $faultModel]} {
							puts $fd [format "NUMBER $i INJECTION [expr $injections - $remaining + 1], FORCES $injValue2 FOR %.2f AFTER %.2f" $injDuration2 $injSeparation]
							echo [format "NUMBER $i INJECTION [expr $injections - $remaining + 1], FORCES $injValue2 FOR %.2f AFTER %.2f" $injDuration2 $injSeparation]
							force -freeze sim:$injNode $injValue2 0 -cancel [expr $injDuration2] ns
						} elseif {[regexp {D$} $faultModel]} {
							puts $fd [format "NUMBER $i INJECTION [expr $injections - $remaining + 1], DEPOSITS $injValue2 FOR %.2f AFTER %.2f" $injDuration2 $injSeparation]
							echo [format "NUMBER $i INJECTION [expr $injections - $remaining + 1], DEPOSITS $injValue2 FOR %.2f AFTER %.2f" $injDuration2 $injSeparation]
							force -deposit sim:$injNode $injValue2 0 -cancel [expr $injDuration2] ns
						}
					}
			}
		}

	}

	if {$startFrom == 0} { 												# to skip until we want to write the packet
		run @$endTime ns
		if {$maxPacketSize != $experiments} {
			if {[expr $i % $maxPacketSize] == 0} {
				incr iPacket;
				echo "== PACKET NUMBER $iPacket WAS STARTED =="
				puts $fd "== PACKET NUMBER $iPacket WAS STARTED =="
				file mkdir [format ./Results/Packet%04u/ $iPacket]
				# file copy ./Results/FaultFree.lst [format ./Results/Packet%04u/ $iPacket] # We no longer want to copy but to link
				# file link -symbolic [format ./Results/Packet%04u/FaultFree.lst $iPacket] "../FaultFree.lst"  # tcl command Not working with symbolic links to files on win
				set p1 [format ./Results/Packet%04u/FaultFree.lst $iPacket]
				exec ln -s ../FaultFree.lst $p1
			}
			if {$ok == 1} { write list [format ./Results/Packet%04u/Fault%07u.lst $iPacket $i];
								exec gawk "{gsub(/ +/,\" \"); if (/^ \[0-9\]+/) print}" [format ./Results/Packet%04u/Fault%07u.lst $iPacket $i] > [format ./Results/Packet%04u/Fault-mod%07u.lst $iPacket $i]
								exec mv [format ./Results/Packet%04u/Fault-mod%07u.lst $iPacket $i] [format ./Results/Packet%04u/Fault%07u.lst $iPacket $i]
							   }
		} else {
			if {$ok == 1} { write list [format ./Results/Fault%07u.lst $i];
								exec gawk "{gsub(/ +/,\" \"); if (/^ \[0-9\]+/) print}" [format ./Results/Fault%07u.lst $i] > [format ./Results/Fault-mod%07u.lst $i]
								exec mv [format ./Results/Fault-mod%07u.lst $i] [format ./Results/Fault%07u.lst $i]
								}
		}
		
	} else {
		incr startFrom -1;
	}
	
}

# WRITE TIME OF EXECUTION
set expTime [expr [clock seconds]-$time4]
puts $fd "Experiments time. Total seconds: $expTime"
echo "Experiments time. Total seconds: $expTime"
puts $fd "[expr $expTime / 3600] H [expr [expr $expTime % 3600] /60] Min [expr $expTime % 60] Sec"
echo "[expr $expTime / 3600] H [expr [expr $expTime % 3600] /60] Min [expr $expTime % 60] Sec"

# WE CLOSE FILE DESCRIPTOR
close $fd
} else {
	echo "ERROR: NO PROPER ARGUMENTS"
}

