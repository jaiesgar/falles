# FILE TO RETRIEVE RESULTS FROM SIMULATIONS IN MULTIPLE LIST FILES ==FOR LEON3-MINIMAL==
# CALL: gawk  -v startInjTime=timeInNs -f simReadResults.awk ./* >Results.log
# WILL LOOK FROM startInjTime VALUE ONWARDS
# PARAMETER: lastOutputLIstOrder is the number of field of the last system output, where the first possible signal position in the list is field nr. 3. Used to differentiate failures from errors
# UPDATED: 23-OCT-2014 Reads independently errors and failures in each experiment, i. e. counts errors also in failured experiments

function restart()
{
	# RESTARTS INPUT ARGUMENTS TO READ, TO PROCESS TWICE THE INPUT FILES
    # shift remaining arguments up
    for (i = ARGC; i > 0; i--)
        ARGV[i-1+ARGIND] = ARGV[i-1]

    # make sure gawk knows to keep going
    ARGC+=ARGIND

    # continue
    nextfile
}


BEGIN{tickCapture=0;transCount=0;interCount=0;permCount=0;othersCount=0;noDetCount=0;faultDetectedections=0;totalFiles=0;faultDetected=-1;
goldRun=1;w=0;v=0;undetectedErr=0;lastFail=""; lastErr= "";detectedNoErr=0; detectedErr=0; failureCount=0;errCount=0; lastLine[0]=0;

	# WE GET LIST NR. EXPERIMENT | TIME OF INJECTION FROM FILE Injections.log
	split (ARGV[1] ,dir, "/")
	injFile = dir[2] "/Injections.log"
	i=1;
	while(( getline l < injFile) > 0 ) {
		# print l
		if (l ~ /MOMENT/) {
			# print l
			split (l, linelist, " ");
			# injTable[i,1] = linelist[2];
			injTable[i] = linelist[10];     # check for multiple injections (intermittent)
			# print injTable[i,1],	injTable[i,2] # debug
			i++;
		} 
	 }
	maxFailLatency = 0;
	maxErrLatency = 0;
}   # end of begin --------------------------------------------------------------------------------------------------------------------------------------------------
		
{
	# Order of last Output in simInitModel file, from 3 to NF   ### EDIT FOR EACH CONFIGURATION OF simInitModel.do
	lastOutputListOrder = 3
	
	###### TO SAVE OUTPUTS OF GOLDEN RUN FOR LATER COMPARISON
	if (FILENAME ~ /FaultFree/ && goldRun == 1) {
		# print FILENAME,goldRun, startInjTime # debug 
				if ($1 ~ /^[0-9]+/ && $1 > startInjTime) {
					goldRes[w,0] = $1;
					for (v = 3; v<=NF; v++) {
						goldRes[w,v] = $v;
					}
					w++;
				# print FILENAME,goldRun, "after" # debug 			
				}
	} else if (w!=0 && goldRun == 1) {
		goldRun=0;
		restart();
	} else {
	# print FILENAME,goldRun # debug 
	# ############################################
		if (goldRun == 0) {
	
				if (FILENAME ~ /.lst/) {
					if (FNR==1) {
							# FOR FAULTS DETECTION SYSTEMS
								# if (faultDetected == 0) {
										# noDetCount+=1;print ARGV[ARGIND-1], "A FAULT WAS NOT DETECTED"; 
										# if (ARGV[ARGIND-1] == lastFail) {print lastFail,"glups! This undetected fault caused output failure";undetectedFail++}
								# } else {
										# if (ARGV[ARGIND-1] != lastFail) {detectedNoFail++} else {detectedFail++}
								# }
								
								# Next block is to check if previous file finished transitions before expected 
								if (lastLine[1] ~ /[0-9]+/)
									if (lastLine[1] < goldRes[w-1,0]) {
									
												if (lastFail != lastFilename) {
															split(lastFilename,auxl,"Fault0")
															split(auxl[2],auxl2,".lst")
															if (maxFailLatency < (lastline[1] - injTable[(auxl2[1]+1)]) ) {
																	maxFailLatency = (lastline[1] - injTable[(auxl2[1]+1)]) }
													failureCount++;
													print "\t\t\t\t\tFAILURE in ", lastFilename
													lastFail = lastFilename; 
													ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
													for (c=0; c<= v; c++) {
														lastFailVal[c] = lastLine[c]
														 if ((w-1,c) in goldRes) {
																lastFailGoldVal[c] = goldRes[0,c];
																if (c==0) {print goldRes[w-1,c], "\t"; } else {print goldRes[w-1,c],""; }
														}
													}								
													print "\n\t\t\t\t\tThis experiment outputs:\t";
													for (c=1;c<=v;c++) { if (c!=2) print lastLine[c],""; else print "\t" }
													ORS="\n";	 print "\n\t\t\t\t\tTransition(s) missed at the end"
												}
															
												if (lastErr != lastFilename) {
															split(lastFilename,auxl,"Fault0")
															split(auxl[2],auxl2,".lst")
															if (maxErrLatency < (lastline[1] - injTable[(auxl2[1]+1)]) ) {
																	maxErrLatency = (lastline[1] - injTable[(auxl2[1]+1)]) }
													errCount++;
													print "\t\t\t\t\tERROR in ", lastFilename
													lastErr = lastFilename;
													ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
													for (c=0; c<=v; c++) { 
														lastErrVal[c] = lastLine[c]
														if ((w-1,c) in goldRes) {
																lastErrGoldVal[c] = goldRes[w-1,c];
																if (c==0) {print goldRes[w-1,c], "\t"; } else {print goldRes[w-1,c],""; }
														}
													}
													print "\n\t\t\t\t\tThis experiment outputs:\t";
													for (c=1;c<=v;c++) { if (c!=2) print lastLine[c],""; else print "\t" }
													ORS="\n";	 print "\n\t\t\t\t\tTransition(s) missed at the end"
												}
											
									}

								faultDetected=0; totalFiles+=1;last="";nextkVal=0; delete lastFailGoldVal; delete lastFailVal; delete lastErrGoldVal; delete lastErrVal;
					}
			


					if ($1 ~ /[0-9]+/) { 				# auxiliarys To check if transition has disappeared at the end which was present in GoldenRun
						split($0,lastLine," ");
						lastFilename= FILENAME;
					}
					
					if ($1 > startInjTime) {
						# FOR FAULTS DETECTION SYSTEMS
						# tickCapture used to compare only in falling edge of signal $3 (clk)
						# if ($3 == 1) tickCapture=1;
						# if ($3 == 0 && tickCapture ==1 && $4 !~ /110/) {
									# # print FILENAME;
									# # print;
									# if (faultDetected==0) {
										# if ($4 ~ /000/) {transCount+=1;last="trans"}
										# if ($4 ~ /011/) {interCount+=1;last="inter"}
										# if ($4 ~ /101/) {permCount+=1;last="perm"}
										# if ($4 !~ /000/ && $4 !~ /011/ && $4 !~ /101/) {othersCount+=1;last="other"}
									# }
									
									# if (faultDetected==1) {
										# if ($4 !~/101/) {faultDetectedections+=1;}
										# else { if (last!="perm") {  permCount+=1;}
												  # if (last=="trans") {transCount-=1;last="perm";} else if (last=="inter") {interCount-=1;last="perm"} else if (last=="other") {othersCount-=1;last="perm"}
										# }
									# }
									# faultDetected=1;
									 # # # # print transCount; print interCount; print permCount;print othersCount; print last;
						# }
						# if ($3 == 0) tickCapture=0;
						
						
						####### TO COMPARE WITH GOLDEN RUN OUTPUTS
						for (k = nextkVal; k<w-1; k++) {
							if ($1 == goldRes[k,0]) {		# The transition times match ---------------------------------------------
								delete mismatch; m=0; nextkVal = k+1;
								for (i=3; i<=NF; i++) {
									if ($i != goldRes[k,i]) {
										if (i <= lastOutputListOrder) {										
											if (i in lastFailVal && i in lastFailGoldVal) {
												if (lastFailVal[i] != $i || lastFailGoldVal[i] != goldRes[k,i])
													{mismatch[m] = i; m++}
											} else { mismatch[m] = i; m++}
										} else {
											if (i in lastErrVal && i in lastErrGoldVal) {
												if (lastErrVal[i] != $i || lastErrGoldVal[i] != goldRes[k,i])
													{mismatch[m] = i; m++ }
											} else { mismatch[m] = i ; m++}
										}
									}
								}
								
								for (m in mismatch) {
									if (mismatch[m] <=lastOutputListOrder) {
										if (lastFail != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													if (maxFailLatency < ($1 - injTable[(auxl2[1]+1)]) ) {
															maxFailLatency = ($1 - injTable[(auxl2[1]+1)]) }
											failureCount++;
											print "\t\t\t\t\tFAILURE in ", FILENAME
											lastFail = FILENAME; split($0,lastFailVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
												lastFailGoldVal[c] = goldRes[k,c];
												if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++) { if (c!=2) print lastFailVal[c],""; else print "\t" };
											print "\n";  ORS="\n";	
										}
									} else {
										if (lastErr != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													if (maxErrLatency < ($1 - injTable[(auxl2[1]+1)]) ) {
															maxErrLatency = ($1 - injTable[(auxl2[1]+1)]) }
											errCount++;
											print "\t\t\t\t\tERROR in ", FILENAME
											lastErr = FILENAME; split($0,lastErrVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
												lastErrGoldVal[c] = goldRes[k,c];
												if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++) { if (c!=2) print lastErrVal[c],""; else print "\t" };
											print "\n";  ORS="\n";
										}
									}
									# break
								}
							# a new transition time has appeared which was not in the goldenRun --------------------
							} else if ($1 > goldRes[k-1,0] && $1 < goldRes[k,0]) { 
								delete mismatch; m=0; nextkVal=k;
								for (i=3; i<=NF; i++) {
									if ($i != goldRes[k-1,i]) {
										if (i <= lastOutputListOrder) {										
											if (i in lastFailVal) {
												if (lastFailVal[i] != $i)	{mismatch[m] = i; m++}
											} else {mismatch[m] = i; m++}
										} else {
											if (i in lastErrVal) {
												if (lastErrVal[i] != $i)   {mismatch[m] = i; m++}
											} else {mismatch[m] = i; m++}
										}
									}
								}
								
								for (m in mismatch) {
									if (mismatch[m] <=lastOutputListOrder) {
										if (lastFail != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													if (maxFailLatency < ($1 - injTable[(auxl2[1]+1)]) ) {
															maxFailLatency = ($1 - injTable[(auxl2[1]+1)]) }
											failureCount++;
											print "\t\t\t\t\tFAILURE in ", FILENAME
											lastFail = FILENAME; split($0,lastFailVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
												lastFailGoldVal[c] = goldRes[k,c];
												if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++) { if (c!=2) print lastFailVal[c],""; else print "\t" };
											print "\n";  ORS="\n";
											print "\t\t\t\t\tNew transition appeared"
										}
									} else {
										if (lastErr != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													if (maxErrLatency < ($1 - injTable[(auxl2[1]+1)]) ) {
															maxErrLatency = ($1 - injTable[(auxl2[1]+1)]) }
											errCount++;
											print "\t\t\t\t\tERROR in ", FILENAME
											lastErr = FILENAME; split($0,lastErrVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
												lastErrGoldVal[c] = goldRes[k,c];
												if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++) { if (c!=2) print lastErrVal[c],""; else print "\t" };
											print "\n";  ORS="\n";
											print "\t\t\t\t\tNew transition appeared"
										}
									}
								}										
							# disappeared cases
							# a transition time has disappeared which was present in the goldenRun
							} else if ($1 ~/[0-9]+/ && $1 > goldRes[k,0]) {
								delete mismatch; m=0; nextkVal = k+1;
								for (i=3; i<=NF; i++) {
									# USE THE NEXT LINE FOR MARKING AS WRONG A DELAYED OUTPUT WITH CORRECT VALUES
									mismatch[m] = i; m++
									# OR USE THE NEXT BLOCK TO ACCEPT AS GOOD A CORRECT DELAYED OUTPUT
									# if ($i != goldRes[k,i]) {
										# if (i <= lastOutputListOrder) {										
											# if (i in lastFailVal) {
												# if (lastFailVal[i] != $i)	{mismatch[m] = i; m++}
											# } else {mismatch[m] = i; m++}
										# } else {
											# if (i in lastErrVal) {
												# if (lastErrVal[i] != $i)   {mismatch[m] = i; m++}
											# } else {mismatch[m] = i; m++}
										# }
									# }
								}
								
								for (m in mismatch) {
									if (mismatch[m] <=lastOutputListOrder) {
										if (lastFail != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													if (maxFailLatency < ($1 - injTable[(auxl2[1]+1)]) ) {
															maxFailLatency = ($1 - injTable[(auxl2[1]+1)]) }
											failureCount++;
											print "\t\t\t\t\tFAILURE in ", FILENAME
											lastFail = FILENAME; split($0,lastFailVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
												lastFailGoldVal[c] = goldRes[k,c];
												if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++) { if (c!=2) print lastFailVal[c],""; else print "\t" };
											print "\n";  ORS="\n";
											print "\t\t\t\t\tExpected transition has disappeared"
										}
									} else {
										if (lastErr != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													if (maxErrLatency < ($1 - injTable[(auxl2[1]+1)]) ) {
															maxErrLatency = ($1 - injTable[(auxl2[1]+1)]) }
											errCount++;
											print "\t\t\t\t\tERROR in ", FILENAME
											lastErr = FILENAME; split($0,lastErrVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
												lastErrGoldVal[c] = goldRes[k,c];
												if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++) { if (c!=2) print lastErrVal[c],""; else print "\t" };
											print "\n";  ORS="\n";
											print "\t\t\t\t\tExpected transition has disappeared"
										}	
									}
								}
							}
						}
						
						# endTime cases
						# a new transition time has appeared at the end which was not in the goldenRun
						if ($1 ~/[0-9]+/ && $1 > goldRes[w-1,0]) {
								delete mismatch; m=0;
								for (i=3; i<=NF; i++) {
									if ($i != goldRes[w-1,i]) {
										if (i <= lastOutputListOrder) {										
											if (i in lastFailVal) {
												if (lastFailVal[i] != $i)	{mismatch[m] = i; m++}
											} else {mismatch[m] = i; m++}
										} else {
											if (i in lastErrVal) {
												if (lastErrVal[i] != $i)   {mismatch[m] = i; m++}
											} else {mismatch[m] = i; m++}
										}
									}
								}
								
								for (m in mismatch) {							
									if (mismatch[m] <= lastOutputListOrder) {
										if (lastFail != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													if (maxFailLatency < ($1 - injTable[(auxl2[1]+1)]) ) {
															maxFailLatency = ($1 - injTable[(auxl2[1]+1)]) }
											failureCount++;
											print "\t\t\t\t\tFAILURE in ", FILENAME
											lastFail = FILENAME; split($0,lastFailVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((w-1,c) in goldRes) {
												lastFailGoldVal[c] = goldRes[w-1,c];
												if (c==0) {print goldRes[w-1,c], "\t"; } else {print goldRes[w-1,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++) { if (c!=2) print lastFailVal[c],""; else print "\t" };
											print "\n";  ORS="\n";
											print "\t\t\t\t\tNew transition appeared at the end"
										}
									} else {
										if (lastErr != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													if (maxErrLatency < ($1 - injTable[(auxl2[1]+1)]) ) {
															maxErrLatency = ($1 - injTable[(auxl2[1]+1)]) }
											errCount++;
											print "\t\t\t\t\tERROR in ", FILENAME
											lastErr = FILENAME; split($0,lastErrVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((w-1,c) in goldRes) {
												lastErrGoldVal[c] = goldRes[w-1,c];
												if (c==0) {print goldRes[w-1,c], "\t"; } else {print goldRes[w-1,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++) { if (c!=2) print lastErrVal[c],""; else print "\t" };
											print "\n";  ORS="\n";
											print "\t\t\t\t\tNew transition appeared at the end"
										}
									}
								}
						}
						
						#########
					}
					
				}
		}  else { nextfile}
	}
}

END{
detectedNoErr--; 
 # USE ONLY IF LAST ONE IS AN INJECTED EXPERIMENT if (faultDetected == 0) noDetCount+=1;detectedNoErr++;
 i=0; do {
			if (ARGV[ARGIND-i] ~ /.lst/) { print ARGV[ARGIND-i] " is last processed file\n\n"; i=ARGIND }
			i++
		} while (i<ARGIND)
 # print "Transients (000): \n",transCount;
 # print "Intermittents (011):\n",interCount;
 # print "Permanents (101): \n",permCount;
 # print "Other detected faults/errors (others):\n",othersCount;
 # print "Non detected experiments (110):\n",noDetCount;
 # print "Redetections (not included in previous categories):\n",faultDetectedections;
 # print "Detected faults without ERRORS in registered outputs: \n",detectedNoErr;
 # print "Undetected faults with ERRORS in registered outputs: \n",undetectedErr;
 print "Number of failures found: \n",failureCount;
 print "Maximum propagation latency for failures: \n",maxFailLatency;
 print "THE FOLLOWING NUMBER OF ERRORS DOES NOT EXCLUDE FAILURED EXPERIMENTS"
 print "Number of errors found: \n",errCount;
 print "Maximum propagation latency for errors: \n",maxErrLatency;
 print "Total Experiments excluding FaultFree: \n",totalFiles-1;
 }