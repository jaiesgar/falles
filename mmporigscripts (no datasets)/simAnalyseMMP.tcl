# MODULE simAnalyseMMP.tcl TO ANALYSE USING -MULTIPLE PROCESSES IN SHARED MEM MACHINE- FAULT FREE AND INJECTED VHDL, DOES SDFMAX & SDFMIN UNLESS SPECIFIED
# AUTHOR: J. Espinosa
# REQUIRES USING simAnalyseAuxMP.do GENERATE FOLDER REPORTS
# THIS FILE IS INTENDED FOR USE IN TCLSH STANDALONE DISTRIBUTION
# 
# CALL USING: tclsh simAnalyseMMP.tcl maxConcurrentProcs expSetup [sdfFileName] [(sdfmax|sdfmin)]
# EXAMPLES: tclsh simAnalyseMMP.tcl 4 "{33 1 12000 11200 10000 10000 1 1 1 1 1F}"
# UPDATED: 	3-mar-2015 created

 source simbgexec1.10.tcl
 # Required simbgexec1.10.tcl in directory for background execution control
 package require simbgexec 1.10


if {$argc < 2} {
		puts "MYERROR: NO PROPER ARGUMENTS"
		puts "CALL USING: tclsh simAnalyseMP.tcl maxConcurrentProcs expSetup sdfFileName \[sdfFileName\] \[(sdfmax|sdfmin)\]"
		puts "REMEMBER expSetup is a quoted set of braced \{experiment configurations\} "
} else {
		set i 1
		foreach va $argv {	# caution: sometimes foreach does not respect order of parameters
			set $i $va;
			incr i 
		}
	# puts [info nameofexecutable]
	
	set startFromDir 0
	set maxConcurrentProcs $1
	set pCount 0
	
	set expSetup $2
	set sdfFileName ""
	set sdfmaxmin ""
	if {$argc > 2} { set sdfFileName $3}
	if {$argc > 3} { set sdfmaxmin $4}
	
	# READ RESULTS AND SAVE ANALYSIS AND TIME ELAPSED
	for {set currentExp 0} { $currentExp < [llength $expSetup]} {incr currentExp}  {
			set time0anal [expr [clock seconds]]
			set pars [lindex $expSetup $currentExp]
			set sdfminNow 0
			set allDirs {}
			
			while  {$sdfminNow < 2} {
				if {$sdfFileName == ""}  {
					set newStr "ResultsRTL [string range [lindex $expSetup $currentExp]  0 end]"
				} else {
					if {$sdfminNow == 0 && $sdfmaxmin !="sdfmin"} {
						set newStr "Results [string range [lindex $expSetup $currentExp]  0 end]"
					} else {
						set newStr "Results sdfmin [string range [lindex $expSetup $currentExp]  0 end]"
					}
				}
				regsub -all {\s} $newStr {_} newStr
				
				set iPacket $startFromDir
				while {[file isdirectory [format ./$newStr/Packet%04u $iPacket]]} {
					set nameiPacket [format ./$newStr/Packet%04u $iPacket];
					while {$pCount > [expr $maxConcurrentProcs -1]} {
						vwait pCount
					}
					if {[llength $pars] == 11} {
							# exec tclsh simAnalyseAuxMP.do "[lindex $pars 1]" "$nameiPacket" &
							lappend h1 [bgExec "[info nameofexe] simAnalyseAuxMP.do [lindex $pars 2] $nameiPacket" [list dummy *$iPacket*] pCount]
					} else {
							# exec tclsh simAnalyseAuxMP.do "[lindex $pars 2]" "$nameiPacket" &
							lappend h2 [bgExec "[info nameofexe] simAnalyseAuxMP.do [lindex $pars 3] $nameiPacket" [list dummy *$iPacket*] pCount]
							
					}
					incr iPacket
					lappend allDirs $nameiPacket/Results.log
				}
				
				# puts [lrange $pids 0 end]
				# after 12000 set go 1
				# vwait go
				# puts "waited 12 sec"
						
					 # example with timeout for task: 
					 # set h3 [bgExec "[info nameofexe] delayout3.tcl" [list dummy *3*]
					# pCount 5000 toExit]
				
				while {$pCount > 0} {
					   vwait pCount
				}
				exec gawk  -f simGatherResultsMP.awk {*}$allDirs > $newStr/GatheredResults.log;
				
				set fd [open $newStr/GatheredResults.log a]
				set analTime [expr [clock seconds]-$time0anal]
				puts $fd "Real Analysis time. Total seconds: $analTime"
				puts $fd "[expr $analTime / 3600] H [expr [expr $analTime % 3600] /60] Min [expr $analTime % 60] Sec"	
				close $fd
				
				incr sdfminNow
				if {$sdfFileName == "" || $sdfmaxmin != ""} { incr sdfminNow}
			}
	}
}
