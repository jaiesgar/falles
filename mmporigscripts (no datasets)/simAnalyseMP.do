# MODULE simAnalyseMP.do TO ANALYSE USING MULTIPLE PROCESSES FAULT FREE AND INJECTED VHDL, DOES SDFMAX & SDFMIN UNLESS SPECIFIED
# AUTHOR: J. Espinosa
# BETTER USE ALTERNATE FILE simAnalyseMMP.tcl FROM TCLSH BECAUSE MODELSIM IMPAIRS PARALLEL EXECUTION FROM A MACRO
# REQUIRES USING simFaultsMP.do IN MODELSIM TO GENERATE DATA TO ANALYSE
# 
# CALL USING: do simAnalyseMP.do expSetup [sdfFileName] [(sdfmax|sdfmin)]
# EXAMPLES: do simAnalyseMP.do { {33 1 12000 11200 10000 10000 1 1 1 1 1F} } 
# UPDATED: 16-oct-2014 Added timer for real time of analysis


if {$argc < 1} {
		echo "MYERROR: NO PROPER ARGUMENTS"
		echo "CALL USING: do simAnalyseMP.do expSetup sdfFileName \[sdfFileName\] \[(sdfmax|sdfmin)\]"
} else {

	set expSetup $1
	set sdfFileName ""
	set sdfmaxmin ""
	if {$argc > 1} { set sdfFileName $2}
	if {$argc > 2} { set sdfmaxmin $3}
	

	# READ RESULTS AND SAVE ANALYSIS AND TIME ELAPSED
	for {set currentExp 0} { $currentExp < [llength $expSetup]} {incr currentExp}  {
			set time0anal [expr [clock seconds]]
			set pars [lindex $expSetup $currentExp]
			set sdfminNow 0
			set allDirs {}
			
			while  {$sdfminNow < 2} {
				if {$sdfFileName == ""}  {
					set newStr "ResultsRTL [string range [lindex $expSetup $currentExp]  0 end]"
				} else {
					if {$sdfminNow == 0 && $sdfmaxmin !="sdfmin"} {
						set newStr "Results [string range [lindex $expSetup $currentExp]  0 end]"
					} else {
						set newStr "Results sdfmin [string range [lindex $expSetup $currentExp]  0 end]"
					}
				}
				regsub -all {\s} $newStr {_} newStr
				
				set iPacket 0
				while {[file isdirectory [format ./$newStr/Packet%04u $iPacket]]} {
					set nameiPacket [format ./$newStr/Packet%04u $iPacket];
					if {[llength $pars] == 11} {
							do simAnalyseAuxMP.do [lindex $pars 1] $nameiPacket &
					} else {
							do simAnalyseAuxMP.do [lindex $pars 2] $nameiPacket &
					}
					incr iPacket
					lappend allDirs $nameiPacket/Results.log
				}
				
				eval gawk  -f simGatherResultsMP.awk "[lrange $allDirs 0 end]" > $newStr/GatheredResults.log;
				
				set fd [open $newStr/GatheredResults.log a]
				set analTime [expr [clock seconds]-$time0anal]
				puts $fd "Real Analysis time. Total seconds: $analTime"
				puts $fd "[expr $analTime / 3600] H [expr [expr $analTime % 3600] /60] Min [expr $analTime % 60] Sec"	
				close $fd
				
				incr sdfminNow
				if {$sdfFileName == "" || $sdfmaxmin != ""} { incr sdfminNow}
			}
	}
}


