# MODULE simConfig.do TO CONFIGURE PARAMETERS OF SIMULATION-BASED INJECTION CAMPAIGN
# AUTHOR: J. Espinosa
# FOR EACH DESIGN EDIT simInitModel.do WITH WATCH LIST OF SIGNALS 
#	  OPTIONALLY EDIT simReadResults.awk TO ADJUST PRESENTATION OF RESULTS
#    OPTIONALLY EDIT simWriteNodes.awk OR simNodes.do TO TWEAK NODES TO INJECT.
# 
# USAGE: source simConfig.do
# UPDATED: 	3-mar-2015 added number of outputs parameter
#						26-feb-2015 added jobName parameter for cluster sims
#						19-feb-2015 Created 

# EDIT ACCORDINGLY TO DESIGN AND COMPUTING PLATFORM ###
set nrSimProcesses 4
set nrMaxAnalysisProcesses 4
set nrPackets 4
set doAnalysis 1
 #######################################################

# top entity to simulate
set top "testbench"
# end of simulation time in nsecs
set endTime 30000
# blank for RTL sim, or name of sdf file for gate level simulation
set sdfFileName ""
# blank for both or value sdfmax or sdfmin for a single type, only applicable for gate level simulation
set sdfmaxmin ""
# every bit of the hierarchy under next component will be injected
set componentToInject "/testbench/d3/u0/leon3x0/p0/iu"
# number of output positions in simInitModel.do
set nrOutputs 2
# name of job for queue system in cluster, and name of cluster, only edit if using CMP runs
set jobName "TSTJOB"
set cluster "Rigel"

# AUTO EXPERIMENTS LIST SETUP 
# [[startFrom] experiments injections startInjTime lastInj2endTime maxDuration minDuration maxSeparation minSeparation isInjValueConstant isInjDurationConstant faultModel] 
# SEE simInjections4.do AND simFaultModel.do FOR FAULT MODELS REFERENCE
#	
#  ====== FAULT MODELS (read values from simFaultModel.do) ==========
# RF: 1 or 0 (random) stuck-at. -Apply anywhere, duration until end of experiment-
# RCF: 1 or 0 (random) pulse. -Apply in combinational logic only, duration limited to injection time-
# 1F: 1 stuck-at. -Apply anywhere, duration until end of experiment-
# 1CF: 1 pulse. -Apply in combinational logic only, duration limited to injection time-
# 0F: 1 stuck-at. -Apply anywhere, duration until end of experiment-
# 0CF: 1 pulse. -Apply in combinational logic only, duration limited to injection time-
# XF: Permanent indetermination. -Apply anywhere, duration until end of experiment-
# XF: Transient indetermination. -Apply anywhere, duration limited to injection time-
# ZF: Open line. -Apply anywhere, duration until end of experiment-
# NSD: Bit-flip. -Apply in sequential logic only, duration until next write of element value- 

set expSetup {
			{40 1 5000 25000 30000 30000 0 0 1 1 1F}	
		}		
			# {500 1 12000 11200 10000 10000 0 0 1 1 0F}
			# {200 1 120 40 8 5 0 0 1 1 DS}
			# {200 1 120 40 5 2 0 0 1 1 DS}
			# {200 1 120 40 2 0.5 0 0 1 1 DS}
			# {200 1 120 40 0.5 0.1 0 0 1 1 DS}
			# {200 1 120 40 0.1 0.05 0 0 1 1 DS}
			# {200 2 120 40 2 2 2 0.5 1 1 DS}
			# {200 2 120 40 0.5 0.5 2 0.5 1 1 DS}
			# {200 2 120 40 0.5 0.5 0.5 0.5 1 1 DS}
			# {200 2 120 40 0.1 0.1 0.1 0.1 1 1 DS}
			# {200 1 120 60 60 15 0 0 1 1 DS}
			# {0 138 sum_out(0)}
			# {1 137 sum_out(0)}
			

# FILTERING FUNCTION FOR RESULTS OF FAULTFREE (injected =0) AND FAULTS (injected=1)
proc filterFunc {injected {iPacket -1} {i -1}} {
	if {$injected == 0} {
		# GENERAL CASE
		# THE FOLLOWING CODE IS NOT REQUIRED FOR CORRECT ANALYSIS OF RESULTS, BUT CAN BE USED FOR FILE COMPARISON PURPOSES
		# exec cp [format ./Results/FaultFree.lst] [format ./Results/FaultFreeFull.lst]
		# exec gawk "{gsub(/ +/,\" \"); if (/^ \[0-9\]+/) print}" [format ./Results/FaultFree.lst] > [format ./Results/FaultFree-mod.lst]
		# exec mv [format ./Results/FaultFree-mod.lst] [format ./Results/FaultFree.lst]
		# FILTERED CASE, FILTER FIRST ZERO OF LENGTH 9 WORDS, number of \ before '2' is 6 for cygwin platform and 4 for linux platform
		exec gawk "{print gensub(/(\[0\])(\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\])/, \"\\\\\\2\" , \"g\") }" [format ./Results/FaultFree.lst] > [format ./Results/FaultFree-mod.lst]
		exec mv [format ./Results/FaultFree-mod.lst] [format ./Results/FaultFree.lst]
	} else {
		# NEXT LINE IS GENERAL CASE, UNCOMMENT FOR USUAL EXPERIMENTS
		# exec gawk "{gsub(/ +/,\" \"); if (/^ \[0-9\]+/) print}" [format ./Results/Packet%04u/Fault%07u.lst $iPacket $i] > [format ./Results/Packet%04u/Fault-mod%07u.lst $iPacket $i]
		# NEXT LINE IS PARTICULAR CASE TO FILTER FIRST ZERO OF LENGTH 9 WORDS, number of \ before '2' is 6 for cygwin platform and 4 for linux platform
		exec gawk "{gsub(/ +/,\" \"); if (/^ \[0-9\]+/) print gensub(/(\[0\])(\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\]\[0-9a-fA-F\])/, \"\\\\\\2\" , \"g\") }" [format ./Results/Packet%04u/Fault%07u.lst $iPacket $i] > [format ./Results/Packet%04u/Fault-mod%07u.lst $iPacket $i]
		
		exec mv [format ./Results/Packet%04u/Fault-mod%07u.lst $iPacket $i] [format ./Results/Packet%04u/Fault%07u.lst $iPacket $i]
	}
}