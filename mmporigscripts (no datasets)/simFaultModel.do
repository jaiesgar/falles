# GENERIC AUXILIARY FILE OF simFaults.do IMPLEMENTING FAULT MODELS INJECTION VALUES
# AUTHOR: J. Espinosa
# UPDATED: 26-9-2014 changed names and added open line

proc FindInjValue {faultModel node} {
			switch $faultModel {
				RCF -
				RF {
					### pulse or stuck-at with random value
					set procInjValue [expr round([expr rand()])]
				}
				1CF -
				1F {
					### pulse or stuck at 1
					set procInjValue 1
				}
				0CF -
				0F {
					### pulse or stuck at 0
					set procInjValue 0
				}
				XF {
					### indetermination
					set procInjValue X
				}
				ZF {
					### Open line
					set procInjValue Z
				}
				NF -
				NSD {
					### bit flip, NF only for test
					set currentValue [examine $node]
					if {$currentValue == 1}  {
						set procInjValue 0
					} else {
						set procInjValue 1
					}
				}
			}
			return $procInjValue
}