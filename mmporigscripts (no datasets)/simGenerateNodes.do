# MODULE simGenerateNodes.do TO CREATE A FILE simNodes.do OF THE EXISTING NODES UNDER A SPECIFIED ENVIRONMENT
# REQUIRES SIMULATOR TO BE LOADED
# AUTHOR: J. Espinosa
#
# CALL IN MODELSIM USING: do simGenerateNodes.do pathToInst
# EXAMPLES: do simGenerateNodes.do /testbench/d3/u0/leon3x0/cmem0 
# UPDATED: 	24-feb-2015	removed 'quietly' to add compatibility with calls from tclsh
#							03-feb-2015 created

quietly set itemList {}
quietly set portsList {}
quietly set elemList {}
quietly set signalsList {}
quietly set generateList {}
quietly set architectureList {}

# --------------------
proc buildLists {ipathToInst iportsList isignalsList ielemList} {
	set itemList [Show $ipathToInst]
	
	for {set it 0} {$it < [llength [lindex $itemList 2]]} {incr it} {
		#assume element 1 is architecture, and 2 is the remaining elements
		set currItem [lindex [lindex $itemList 2] $it]
		# echo $it, $currItem;
		if {[lindex $currItem 1] != ".."} { 					# ignore parent element

			if {[lindex $currItem 0] == "Port"} {
				lappend iportsList "$ipathToInst/[lindex $currItem 1]"
				lappend ielemList "$ipathToInst/[lindex $currItem 1]"
				# eval add wave {"$currItem $it" { "sim:$inst[lindex $currItem 1]" }}
			}
			if {[lindex $currItem 0] == "Signal"} {
				lappend isignalsList "$ipathToInst/[lindex $currItem 1]"
				lappend ielemList "$ipathToInst/[lindex $currItem 1]"
				# eval add wave {"$currItem $it" { "sim:$inst[lindex $currItem 1]" }}
			}
			if {[regexp {Generate} [lindex $currItem 0]] == 1} {
				set i2pathToInst $ipathToInst/[lindex $currItem 1]
				set ret [buildLists $i2pathToInst $iportsList $isignalsList $ielemList]
				set iportsList [lindex $ret 0]
				set isignalsList [lindex $ret 1]
				set ielemList [lindex $ret 2]
				# lappend generateList "$inst[lindex $currItem 1]/"
				# Generate is equivalent to architecture
			}
			if {[lindex $currItem 0] == "Architecture"} {
				set i2pathToInst $ipathToInst/[lindex $currItem 1]
				set ret [buildLists $i2pathToInst $iportsList $isignalsList $ielemList]
				set iportsList [lindex $ret 0]
				set isignalsList [lindex $ret 1]
				set ielemList [lindex $ret 2]
				# lappend iarchitectureList "[lindex $currItem 1]"
			}
		}
	}
	
	return [list $iportsList $isignalsList $ielemList]
}



proc addElements {ielemList fp} {
	for {set it 0} {$it < [llength $ielemList]} {incr it} {
		if {[find signals [lindex $ielemList $it].*] != ""}   {
				set sigList [lsort -dictionary [find signals [lindex $ielemList $it].*]]
				for {set s 0} {$s < [llength $sigList]} {incr s} {
					addElements [lindex $sigList $s] $fp
				}
		} elseif { [find signals [lindex $ielemList $it](*)] != ""}  {
				set sigList [lsort -dictionary [find signals [lindex $ielemList $it](*)]]
				for {set s 0} {$s < [llength $sigList]} {incr s} {
					addElements [lindex $sigList $s] $fp
				}
		} else {
				# eval add wave [lindex $ielemList $it]
				puts $fp "{[lindex $ielemList $it]}"
				
		}
	}
}


# ==============================================================
 

# echo "------------- Processing Hierarchy: ------------------"
quietly set inst $1
quietly set topList [buildLists $inst $portsList $signalsList $elemList]
quietly set portsList [lindex $topList 0]
quietly set signalsList [lindex $topList 1]
quietly set elemList [lindex $topList 2]
# echo "------------- END ------------------"

quietly set fp [open ./simNodes.do w]
puts $fp "# SUPPORT FILE TO SELECT NODES TO INJECT. GENERATED BY simGenerateNodes.do";
puts $fp "# PATH TO TOP OF NODES: $1";
puts $fp "# Call using: source simNodes.do\n";
puts $fp "set injNodes {";

# echo
# echo "Adding Ports and Signals: $elemList"
addElements $elemList $fp

puts $fp "};\n"
close $fp
