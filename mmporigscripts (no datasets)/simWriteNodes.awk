# FILE TO RETRIEVE NODES TO INJECT FROM  POST-PAR VHDL FLATTENED FILE FOR SIMULATION
# CURRENTLY ADDS NODES OF OUTPUTS AND INTERNAL NODES OF COMBINATIONAL LOGIC BLOCK nameOfUnit
# AUTHOR: J. Espinosa
# INPUT PARAMETER: nameOfUnit, the unit to inject
#
# CALL: gawk -v nameOfUnit="mynameofUnit"-f simWriteNodes.awk design.vhd >simNodes.do
# UPDATED 07/6/2014 added quietly to avoid printing nodes

BEGIN{ OFS="";
				print "# SUPPORT FILE TO SELECT NODES TO INJECT. CALLED BY simInjections";
				print "# Call using: source simNodes.do\n";
				print "quietly set injNodes {";
			}
{

	# ADD INTERNAL SIGNALS OF COMBINATIONAL (START WITH "inst"+nameOfUnit)
	if (($1 ~ signal) && ($2 ~ "inst" nameOfUnit)) {
			print "{/tbx_", nameOfUnit, "_regerr/UUT/", $2, "}"
	}
	
	# ADD OUTPUTS OF COMBINATIONAL (END WITH "_out")
	if ($2 ~ /_out$/) {
			if ($4 ~ /_VECTOR$/)  {
				for (i = $6; i >= 0; i--) {
					print "{/tbx_", nameOfUnit, "_regerr/UUT/", $2, "(", i , ")}"
				}
			} else {
				print "{/tbx_", nameOfUnit, "_regerr/UUT/", $2, "}"
			}
	}
	
	# # ADD ALL SIGNALS EXCLUDING KEY ELEMENTS (clock, powersupply, iobuffers)
	# if ($1 ~ /signal/ && $2 !~ /INSERTED/ && $2 !~ /IBUF/ && $2 !~ /OBUF/ && $2 !~ /GND/ && $2 !~ /VCC/ && $2 !~ /clk_BUFGP/) {
			# if ($4 ~ /_VECTOR$/) {
						# if ($7 ~ /downto/) {
									# for (i = $6; i >= $8; i--) {
										# print "{/tbx_", nameOfUnit, "_regerr/UUT/", $2, "(", i , ")}"
									# }
						# } else {
									# for (i = $6; i <= $8; i++) {
										# print "{/tbx_", nameOfUnit, "_regerr/UUT/", $2, "(", i , ")}"
									# }
						# }
			# } else {
						# print "{/tbx_", nameOfUnit, "_regerr/UUT/", $2, "}"
			# }
	# }

}
END{
			print "};\n"
		}
 