# MODULE simAnalyseAuxMPfiltercache.do TO ANALYSE RESULTS FILTERING OUT CACHE INPUTS
#  
# CALL USING: do simAnalyseAuxMP.do startInjTime nameiPacket
# EXAMPLES: do simAnalyseAuxMP.do 11200 {./ResultsRTL 33 1 12000 11200 10000 10000 1 1 1 1 1F/Packet0000}
# UPDATED:	22-oct-2015 Source simConfig for passing nrOutputs to simReadResults.awk
# 						8-oct-2014 Made tcl distribution independent


if {$argc < 2} {
	echo "MYERROR: NO PROPER ARGUMENTS"
	echo "CALL USING: do simAnalyseAuxMP.do startInjTime nameiPacket"
} else {
	if {![info exists 1]} {
		set i 1
		foreach var $argv {
			set $i $var;
			incr i 
		}
	}
	
	# READ Injections.log and create a table with Faults to rename to *lstx to avoid including them in the analysis
	 set fp [open $2/../Injections.log r]
     set file_data [read $fp]
     close $fp
	 set data [split $file_data "\n"]
     foreach line $data {
		if {[regexp {(^NUMBER )([0-9]+)([^/]+)(.*)} $line a b c d e]} {
			if {[regexp {dci} $e] || [regexp {ici} $e]} {
				lappend filterList $c
			}
		}
     }
	
	foreach elem $filterList {
		if {[file exists [format $2/Fault%07u.lst $elem]]} {
			exec mv [format $2/Fault%07u.lst $elem] [format $2/Fault%07u.lstx $elem];
		}	
	}
	 
 	# Load config for number of outputs parameter, add 1 if deltas are absent and 2 if they are present
	source simConfig.do
	
	set time1 [expr [clock seconds]];
	# The following line is for names of faults with spaces
	# exec gawk -v startInjTime=$1 -f simReadResults.awk  "$2/FaultFree.lst" "[file join $2 Fault] *.lst" > $2/Results.log
	# exec gawk -v startInjTime=$1 -f simReadResults.awk  "$2/FaultFree.lst" "[file join $2 Fault]0*.lst" > $2/Results.log # for modelsim
	eval exec gawk -v startInjTime=$1 -v lastOutputListOrder=[expr $nrOutputs + 1] -f simReadResults.awk $2/FaultFree.lst [glob [file join $2 Fault]0*.lst] > $2/Results.log
	set fd1 [open $2/Results.log a]
	set expTime1 [expr [clock seconds]-$time1]
	puts $fd1 "Analysis time. Total seconds: $expTime1"		
	puts $fd1 "[expr $expTime1 / 3600] H [expr [expr $expTime1 % 3600] /60] Min [expr $expTime1 % 60] Sec"
	close $fd1
	
}