# MODULE simAnalyseCMP.tcl TO ANALYSE USING -MULTIPLE PROCESSES IN CLUSTER- FAULT FREE AND INJECTED VHDL, DOES SDFMAX & SDFMIN UNLESS SPECIFIED
# AUTHOR: J. Espinosa
# REQUIRES USING simAnalyseAuxMP.do GENERATE FOLDER REPORTS
# THIS FILE IS INTENDED FOR USE IN TCLSH STANDALONE DISTRIBUTION
# 
# CALL USING: tclsh simAnalyseCMP.tcl
# EXAMPLES: tclsh simAnalyseCMP.tcl
# UPDATED: 	4-mar-2015 With relative path to design

 source simbgexec1.10.tcl
 # Required simbgexec1.10.tcl in directory for background execution control
 package require simbgexec 1.10

 source simConfig.do

if {$argc > 1} {
		puts "MYERROR: NO PROPER ARGUMENTS"
		puts "CALL USING: tclsh simAnalyseMP.tcl"
} else {
		set i 1
		foreach va $argv {	# caution: sometimes foreach does not respect order of parameters
			set $i $va;
			incr i 
		}

	# EDIT IF ANALYSIS HAS TO START FROM A SPECIFIC PACKET, OR PATH ##
	# set pathToDesign "."
	set startFromDir 0
	##################################################################
	
	set pCount 0

	# READ RESULTS AND SAVE ANALYSIS AND TIME ELAPSED
	for {set currentExp 0} { $currentExp < [llength $expSetup]} {incr currentExp}  {
			set time0anal [expr [clock seconds]]
			set pars [lindex $expSetup $currentExp]
			set sdfminNow 0
			set allDirs {}
			
			while  {$sdfminNow < 2} {
				if {$sdfFileName == ""}  {
					set newStr "ResultsRTL [string range [lindex $expSetup $currentExp]  0 end]"
				} else {
					if {$sdfminNow == 0 && $sdfmaxmin !="sdfmin"} {
						set newStr "Results [string range [lindex $expSetup $currentExp]  0 end]"
					} else {
						set newStr "Results sdfmin [string range [lindex $expSetup $currentExp]  0 end]"
					}
				}
				regsub -all {\s} $newStr {_} newStr
				
				set iPacket $startFromDir
				while {[file isdirectory [format $newStr/Packet%04u $iPacket]]} {
					set nameiPacket [format $newStr/Packet%04u $iPacket];
					while {$pCount > [expr $nrMaxAnalysisProcesses -1]} {
						clWaitForRemaining $nrMaxAnalysisProcesses
					}
					if {[llength $pars] == 11} {
							puts "started $nameiPacket"
							clExec "[info nameofexe] simAnalyseAuxMP.do [lindex $pars 2] ./Results/Packet" $jobName $cluster $pCount $nameiPacket
					} else {
							puts "started $nameiPacket"
							clExec "[info nameofexe] simAnalyseAuxMP.do [lindex $pars 3] ./Results/Packet" $jobName $cluster $pCount $nameiPacket
					}
					incr iPacket
					lappend allDirs $nameiPacket/Results.log
				}
				puts "waiting to end of processes"
				clWaitForRemaining 0
				
				exec gawk  -f simGatherResultsMP.awk {*}$allDirs > $newStr/GatheredResults.log;
				
				set fd [open $newStr/GatheredResults.log a]
				set analTime [expr [clock seconds]-$time0anal]
				puts $fd "Real Analysis time. Total seconds: $analTime"
				puts $fd "[expr $analTime / 3600] H [expr [expr $analTime % 3600] /60] Min [expr $analTime % 60] Sec"	
				close $fd
				
				incr sdfminNow
				if {$sdfFileName == "" || $sdfmaxmin != ""} { incr sdfminNow}
			}
	}
}
