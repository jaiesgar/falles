# MODULE simAnalyseMMP.tcl TO ANALYSE USING -MULTIPLE PROCESSES IN SHARED MEM MACHINE- FAULT FREE AND INJECTED VHDL, DOES SDFMAX & SDFMIN UNLESS SPECIFIED
# AUTHOR: J. Espinosa
# REQUIRES USING simAnalyseAuxMP.do GENERATE FOLDER REPORTS
# THIS FILE IS INTENDED FOR USE IN TCLSH STANDALONE DISTRIBUTION
# 
# CALL USING: tclsh simAnalyseMMP.tcl
# UPDATED: 	21-apr-2015 read parameters from simConfig.do
# 						3-mar-2015 created

 source simbgexec1.10.tcl
 # Required simbgexec1.10.tcl in directory for background execution control
 package require simbgexec 1.10
proc printProc {param data} {
	# Prints all info (except stderr under cygwin) to stdout
	puts "simProcess $param: $data"
}

source simConfig.do

if {$argc > 0} {
		puts "MYERROR: NO PROPER ARGUMENTS"
		puts "CALL USING: tclsh simAnalyseMMP.tcl"
} else {
		set i 1
		foreach va $argv {	# caution: sometimes foreach does not respect order of parameters
			set $i $va;
			incr i 
		}
		
	# EDIT IF ANALYSIS HAS TO START FROM A SPECIFIC PACKET, OR PATH ##
	# set pathToDesign "."
	set startFromDir 0
	##################################################################
	
	set pCount 0
	
	# READ RESULTS AND SAVE ANALYSIS AND TIME ELAPSED
	for {set currentExp 0} { $currentExp < [llength $expSetup]} {incr currentExp}  {
			set time0anal [expr [clock seconds]]
			set pars [lindex $expSetup $currentExp]
			set sdfminNow 0
			set allDirs {}
			
			while  {$sdfminNow < 2} {
				if {$sdfFileName == ""}  {
					set newStr "ResultsRTL [string range [lindex $expSetup $currentExp]  0 end]"
				} else {
					if {$sdfminNow == 0 && $sdfmaxmin !="sdfmin"} {
						set newStr "Results [string range [lindex $expSetup $currentExp]  0 end]"
					} else {
						set newStr "Results sdfmin [string range [lindex $expSetup $currentExp]  0 end]"
					}
				}
				regsub -all {\s} $newStr {_} newStr
				
				set iPacket $startFromDir
				while {[file isdirectory [format ./$newStr/Packet%04u $iPacket]]} {
					set nameiPacket [format ./$newStr/Packet%04u $iPacket];
					while {$pCount > [expr $nrMaxAnalysisProcesses -1]} {
						vwait pCount
					}
					if {[llength $pars] == 11} {
							puts "started analysis $pCount: $nameiPacket"
							# exec tclsh simAnalyseAuxMP.do "[lindex $pars 1]" "$nameiPacket" &
							lappend h1 [bgExec "[info nameofexe] simAnalyseAuxMP.do [lindex $pars 2] $nameiPacket"  [list printProc $iPacket] pCount]
					} else {
							puts "started analysis $pCount: $nameiPacket"
							# exec tclsh simAnalyseAuxMP.do "[lindex $pars 2]" "$nameiPacket" &
							lappend h2 [bgExec "[info nameofexe] simAnalyseAuxMP.do [lindex $pars 3] $nameiPacket"  [list printProc $iPacket] pCount]
							
					}
					incr iPacket
					lappend allDirs $nameiPacket/Results.log
				}
				
				# puts [lrange $pids 0 end]
				# after 12000 set go 1
				# vwait go
				# puts "waited 12 sec"
						
					 # example with timeout for task: 
					 # set h3 [bgExec "[info nameofexe] delayout3.tcl" [list dummy *3*]
					# pCount 5000 toExit]
				puts "waiting to end of processes"
				while {$pCount > 0} {
					   vwait pCount
				}
				exec gawk  -f simGatherResultsMP.awk {*}$allDirs > $newStr/GatheredResults.log;
				
				set fd [open $newStr/GatheredResults.log a]
				set analTime [expr [clock seconds]-$time0anal]
				puts $fd "Real Analysis time. Total seconds: $analTime"
				puts $fd "[expr $analTime / 3600] H [expr [expr $analTime % 3600] /60] Min [expr $analTime % 60] Sec"	
				close $fd
				
				incr sdfminNow
				if {$sdfFileName == "" || $sdfmaxmin != ""} { incr sdfminNow}
			}
	}
}
