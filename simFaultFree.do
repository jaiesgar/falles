# FAULT-FREE SIMULATION OF MODULE top UNTIL endTime SECONDS, SAVING SIM STATE AT startInjTime IN FILE savedstate.sim
# WITH POSSIBLE FILTERING
# AUTHOR: J. Espinosa
#
# CALL: do simFaultFree.do topModuleName endtimeSim startInjTime 
# UPDATED: 27 feb 2015 Changed to filtering proc configured in simConfig.do
#					13 oct 2014 Swapped init command to avoid saving pre-injections data

if {$argc == 3} {

run @$3 ns
do {./simInitModel.do} $1
checkpoint ./Results/savedstate.sim
run @$2 ns
write list {./Results/FaultFree.lst}
source simConfig.do
filterFunc 0

} else {
echo "ERROR: NO PROPER ARGUMENTS"
}
