# MODULE simFaultsMMP.do TO SIMULATE -USING MULTIPLE PROCESSES- FAULT FREE AND INJECTED VHDL, DOES SDFMAX & SDFMIN UNLESS SPECIFIED
# AUTHOR: J. Espinosa
# FOR EACH DESIGN EDIT simInitModel.do WITH WATCH LIST OF SIGNALS 
#						AND EDIT simConfig.do FOR GENERAL CONFIGURATION
#						  AND EDIT simReadResults.awk TO ADJUST PRESENTATION OF RESULTS
#    OPTIONALLY EDIT simWriteNodes.awk OR simGenerateNodes.do TO TWEAK NODES TO INJECT.
# REQUIRED FILES: simInjections5.do , simFaultModel.do, simFaultFree.do, simWriteNodes.awk, simReadResults.awk, simInitModel.do, simAnalyseAuxMP.do, simbgexec1.10.tcl
# 
# CALL IN CONSOLE USING: tclsh simFaultsMMP.tcl
# UPDATED:    16-mar-2015 cleans savedstates for each simProcess
#						13-mar-2015 made linux compatible avoiding wlf files overwrite
#						11-mar-2015 Catch added to avoid break when no wlf files found to erase
#						4-mar-2015 changed name nrMaxAnalysisProcesses; now loads project file instead of modelsim.ini
#						3-mar-2015 Analysis command updated
#						26-feb-2015 Created


source simbgexec1.10.tcl
# Required simbgexec1.10.tcl in directory for background execution control
package require simbgexec 1.10
proc printProc {param data} {
	# Prints all info (except stderr under cygwin) to stdout
	puts "simProcess $param: $data"
}

set time0 [expr [clock seconds]];

# LOAD CONFIGURATION FILE
source simConfig.do

puts "Started Injections....please wait"

# AUTOMATED PROCESSING OF EXPERIMENTS LIST SETUP
for {set currentExp 0} { $currentExp < [llength $expSetup]} {incr currentExp}  {
	
	set pars [lindex $expSetup $currentExp]
	set sdfminNow 0
	while {$sdfminNow < 2} {
	
		# AUTO CREATE AND CHECK LIST OF NODES TO INJECT IF NOT ALREADY PRESENT
		if {[file exists simNodes.do] == 0} {
			if {$sdfFileName == ""} {
				# exec vsim -c -modelsimini modelsim.ini -do "vsim -quiet -t 1ps -vopt -voptargs=+acc work.$top;do simGenerateNodes.do $componentToInject;quit"
				exec vsim -c -do "project open $top;vsim -quiet -t 1ps -vopt -voptargs=+acc work.$top; do simGenerateNodes.do $componentToInject;quit"
			} else {
				regexp -indices {\.} $sdfFileName sdfDotPos
				set vhdFileName [string range $sdfFileName 0 [expr [lindex $sdfDotPos 0] -1]]
				regexp -indices {_regERR} $top topUnderPos
				# select main component after par for injection
				set componentToInject [string range $top 4 [expr [lindex $topUnderPos 0] -1]]
				exec gawk -v nameOfUnit=$componentToInject -f simWriteNodes.awk ./$vhdFileName.vhd >./simNodes.do
			}
		}
		if {[exec head simNodes.do | gawk "{if (NR==8) print \"Valid\";}"] != "Valid"} {puts \"MYERROR: GENERATED NODES FILE IS EMPTY\"; abort; break}

		
		# RENAME IF PREVIOUS EXECUTION IS PRESENT AND WE ARE NOT CONTINUING EXPERIMENT
		if {[file exists Results]} {
			if {[llength $pars] == 11} {
				file rename ./Results Results[file atime ./Results]
				file mkdir Results
			}
		} else {
			file mkdir Results
		}

		
		# FAULT-FREE SIMULATION OF MODULE top UNTIL endTime SECONDS, SAVING SIM STATE AT startInjTime
		if {[llength $pars] == 12} {
			# exec vsim -c -modelsimini modelsim.ini -do "vsim -quiet -t 1ps -vopt -voptargs=+acc work.$top;do simFaultFree.do $top $endTime [lindex $pars 3];quit"
			exec vsim -c -wlflock -do "project open $top;vsim -quiet -t 1ps -vopt -voptargs=+acc work.$top; do simFaultFree.do $top $endTime [lindex $pars 3];quit"
		} else {
			# exec vsim -c -modelsimini modelsim.ini -do "vsim -quiet -t 1ps -vopt -voptargs=+acc work.$top;do simFaultFree.do $top $endTime [lindex $pars 2];quit"
			exec vsim -c -wlflock -do "project open $top;vsim -quiet -t 1ps -vopt -voptargs=+acc work.$top; do simFaultFree.do $top $endTime [lindex $pars 2];quit"
		}
		
		# MULTIPROCESS INJECTIONS SIMULATION
		if {[llength $pars] == 12} {
			set startFrom [lindex $pars 0]
			set experiments [lindex $pars 1]
			set maxDuration [lindex $pars 5]
			set minDuration [lindex $pars 6]
			set maxSeparation [lindex $pars 7]
			set minSeparation [lindex $pars 8]
			set faultModel [lindex $pars 11]
			set fd [open ./Results/Injections.log a]
		} else {
			set startFrom 0
			set experiments [lindex $pars 0]
			set maxDuration [lindex $pars 4]
			set minDuration [lindex $pars 5]
			set maxSeparation [lindex $pars 6]
			set minSeparation [lindex $pars 7]
			set faultModel [lindex $pars 10]
			set fd [open ./Results/Injections.log w]
			source simNodes.do
			puts $fd "== $experiments INJECTIONS OF $maxDuration NS MAXDURATION,  $minDuration MINDURATION, $maxSeparation NS MAXSEPARATION, $minSeparation NS MINSEPARATION, $faultModel FAULTMODEL and INJECTED IN ANY OF THESE NODES: === $injNodes \n====="
		}

		set maxPacketSize [expr int ([expr ceil ([expr $experiments / $nrPackets.0])])];
		set packetsxSimProcess [expr int ([expr ceil ([expr [expr ceil ([expr $experiments / $maxPacketSize.0])] / $nrSimProcesses]) ]) ]
		set pCount 0;
		for {set pp 0} {$pp<$nrSimProcesses} {incr pp} {
			## CALCULATE startFrom PARAMETER AND experiments FOR EACH SIM PROCESS BEFORE LAUNCH.
			if {$pp == 0} {
				if {[llength $pars] == 12} {
					# we had a startFrom !=0
					set pp [expr int ([expr $startFrom / [expr $packetsxSimProcess*$maxPacketSize]])]
				} else {
					set parsaux [linsert $pars 0 [expr ($pp*$packetsxSimProcess*$maxPacketSize)]]
					set pars $parsaux
				}
			} else {
				lset pars 0 [expr ([expr ($pp*$packetsxSimProcess*$maxPacketSize)])]
			}
			if {$pp != [expr $nrSimProcesses-1]} {
				lset pars 1 [expr ([expr $pp+1]*$packetsxSimProcess*$maxPacketSize)]
			} else {
				lset pars 1 $experiments
			}
			# if {$pp != 0} {
				# while {[file exists ./vsim0.wlf] == 0} {
					# after 100
				# }
			# }
			puts "vsim -c -restore Results/savedstate.sim -do \"do simInjections5.do $top $endTime [lrange $pars 0 end]  $pp $maxPacketSize\""
			# lappend h1 [bgExec "vsim -c -modelsimini modelsim.ini -restore Results/savedstate.sim -do \"do simInjections5.do $top $endTime [lrange $pars 0 end]  $pp $maxPacketSize\"" [list dummy $pp] pCount]
			lappend h1 [bgExec "vsim -c -wlflock -restore Results/savedstate.sim -do \"do simInjections5.do $top $endTime [lrange $pars 0 end]  $pp $maxPacketSize\"" [list printProc $pp] pCount]
		}
		while {$pCount > 0} {
		   vwait pCount
		}
		
		catch {foreach tmpFile [file nativename [glob ./wlft*]] { exec rm $tmpFile } } errId
		catch {foreach tmp2File [file nativename [glob ./*wlf]] { exec rm $tmp2File } } errId
		
		# GATHER ALL INJECTIONS FILES IN MAIN INJECTIONS FILE AND CLOSE HANDLER
		set expTime 0
		for {set pp 0} {$pp<$nrSimProcesses} {incr pp} {
			if {[file exists ./Results/Injections$pp.log]}	{
				set rfd [open ./Results/Injections$pp.log r]
				set rf_data [read $rfd]
				close $rfd
				set data [split $rf_data "\n"]
				foreach line $data {
					if {[regexp {NUMBER} $line]} { puts $fd $line }
					if {[regexp {^(Experiments)} $line]} { regexp {[0-9]+$} $line match; set expTime [expr $expTime+$match]}
				}
			}
		}
		puts $fd "Experiments time. Total seconds: $expTime"
		puts $fd "[expr $expTime / 3600] H [expr [expr $expTime % 3600] /60] Min [expr $expTime % 60] Sec"
		close $fd
		
		# AUTOPROC RENAME DIRS
		if {$sdfFileName == ""}  {
			set newStr "ResultsRTL [string range [lindex $expSetup $currentExp]  0 end]"
		} else {
			if {$sdfminNow == 0 && $sdfmaxmin !="sdfmin"} {
				set newStr "Results [string range [lindex $expSetup $currentExp]  0 end]"
			} else {
				set newStr "Results sdfmin [string range [lindex $expSetup $currentExp]  0 end]"
			}
		}
		regsub -all {\s} $newStr {_} newStr
		catch {foreach savestateFile [file nativename [glob ./Results/savedstate*.sim]] { exec rm $savestateFile }} errId
		file rename ./Results ./$newStr
	
	# REPEAT ONLY IF SDF AND NOT SPECIFIED
	incr sdfminNow
	if {$sdfFileName == "" || $sdfmaxmin != ""} { incr sdfminNow}
	}
}

if  {$doAnalysis == 1} {
	source simAnalyseMMP.tcl
}


puts "Elapsed time. Total seconds: [expr [clock seconds]-$time0]"
puts "Hours: [expr [expr [clock seconds]-$time0] / 3600] Minutes: [expr [expr [expr [clock seconds]-$time0] % 3600] /60] Seconds: [expr [expr [clock seconds]-$time0] % 60]"



