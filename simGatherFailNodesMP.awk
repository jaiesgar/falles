# FILE TO RETRIEVE FAULTY NODES ACROSS VARIOUS RESULTS FILES
# CALL: gawk  -f simGatherFailNodesMP.awk {List of inputs} >GatheredFailNodes.log
#
# UPDATED:	  28-apr-2015 Debugged problem with fault0000
#  				  24-mar-2015 Created


BEGIN{
				PROCINFO["sorted_in"] = "@ind_num_asc" # This forces arrays to be sorted by index in numerical increasing order
			
			
				# WE GET LIST NR. EXPERIMENT | TIME OF INJECTION FROM FILE Injections.log
				nn=split (ARGV[1] ,dir, "/")
				injFile=dir[1]; for (n =2; n <= nn-1; n++) injFile=injFile "/"dir[n]; injFile = injFile"/Injections.log";
				if( system( "[ -f " injFile " ] " )  != 0 ) { injFile=dir[1]; for (n =2; n <= nn-2; n++) injFile=injFile "/"dir[n]; injFile = injFile"/Injections.log"; }
				if( system( "[ -f " injFile " ] " )  != 0 ) { print "EXITING! injFile not found: "; print injFile; }
				if( system( "[ -f " injFile " ] " )  != 0 ) exit;
				i=1;
				while(( getline l < injFile) > 0 ) {
					if (l ~ /MOMENT/) {
						split (l, linelist, " ");
						injTable[linelist[2]] = linelist[13];     # we save number and node
						i++;
					} 
				 }
				 # for (y in injTable) print "index " y ": " injTable[y] # debug
		  }
		
{

	if (FILENAME ~ /Results.log/ ) {
			if (/ERROR in/) {
				ee=split($NF,dir,"/")
				split(dir[ee], numchunk, "Fault0")
				split(numchunk[2],nm,".lst")
				number=nm[1]
				errTable[number]++ 
			}
			if (/FAILURE in/) {
				ee=split($NF,dir,"/")
				split(dir[ee], numchunk, "Fault0")
				split(numchunk[2],nm,".lst")
				number=nm[1]
				failTable[number]++ 
				if (!(number in errTable)) {
					failNoErrTable[number]++
				}
			}
			

	}
}

END{
n=asorti(errTable,eT)
print "Nodes which caused ERROR ========================================";
for (i=1;i<=n;i++) {
	gsub ("^0*", "", eT[i]);
	if (eT[i] == "") { print injTable[0]; } else { print injTable[eT[i]]; }
}
print "\n"

n=asorti(failTable,fT)
print "Nodes which caused FAILURE =========================================";
for (i=1;i<=n;i++) {
	gsub ("^0*", "", fT[i]);
	if (fT[i] == "") { print injTable[0]; } else { print injTable[fT[i]]; }
}
print "\n"

n=asorti(failNoErrTable,fneT)
print "Nodes which caused FAILURE WITHOUT PREVIOUS ERROR =========================";
for (i=1;i<=n;i++) {
	gsub ("^0*", "", fneT[i]);
	if (fneT[i] == "") { print injTable[0]; } else { print injTable[fneT[i]]; }
}

 }