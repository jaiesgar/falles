# FILE TO RETRIEVE RESULTS FROM VARIOUS RESULTS FILES SPREAD IN DIRECTORIES
# AUTHOR: J. Espinosa
# CALL: gawk  -f simGatherResultsMP.awk {List of inputs} >GatheredResults.log
#
# UPDATED:	3-jun-2015 Added error model in registers analysis
# 					 	5-feb-2015 Added histogram of latencies and averages
#						03- feb- 2015 Added support for fail after err latencies and counts, and hang count, debugged
#						21-OCT-2014 Added maximum propagation latencies


BEGIN{totalFiles=0; failureCount=0;maxFailLatency=0; errCount=0;maxErrLatency=0; secondsCount=0;sglsecondsCount=0;
				hangCount=0; failAfterErrCount=0; maxFailAfterErrLatency=0; lastFailAfterErrCount=0; avgFailAfterErrLatency=0;
				addedErrs=0;
				PROCINFO["sorted_in"] = "@ind_num_asc" # This forces arrays to be sorted by index in numerical increasing order
				}
		
{
	if (FILENAME ~ /Results.log/ ) {
			if (/Distribution of Failure Latencies from fault injection instant:/) {
				getline;
				while ($1 == "failLatency:") {
					failLatencyHistogram[$2] += $4;
					getline
				}
			}
			if (/Distribution of Error Latencies from fault injection instant:/) {
				getline;
				while ($1 == "errLatency:") {
					errLatencyHistogram[$2] += $4;
					getline
				}
			}
			if (/Distribution of Failure After Error Latencies:/) {
				getline;
				while ($1 == "failAfterErrLatency:") {
					failAfterErrLatencyHistogram[$2] += $4;
					getline
				}
			}
			if (/Distribution of accumulated total errors per position of outputs:/) {
				getline;
				while ($1 == "Position:") {
					cumulativeErrors[$2] += $5;
					getline 
				}
			}
			if (/Distribution of accumulated total error Repetitions per position of outputs:/) {
				getline;
				while ($1 == "Position:") {
					cumulativeErrRepetitions[$2] += $6;
					getline 
				}
			}
			if (/Distribution of accumulated and average total Bit Upsets per position of outputs:/) {
				getline;
				while ($1 == "Position:") {
					cumulativeErrBitUpsets[$2] += $7;
					getline 
				}
				if ($1 ~ /--------------------/) {
					getline
					while ($1 == "Position:") {
						averageErrBitUpsets[$2] += $7;
						getline 
					}
				}
			}
			if (/Distribution of Error Durations:/) {
				getline;
				while ($1 !~ /^$/) {
					while ($1 ~ /POSITION/) {
						thisPos =  $2
						getline
					}
					while ($1 == "ErrDurations:") {
						errDurationsHistogram[thisPos,$2]+= $4;
						getline 
					}
				}
			}
			if (/Distribution of Error Separations:/) {
				getline;
				while ($1 !~ /^$/) {
					while ($1 ~ /POSITION/) {
						thisPos =  $2
						getline
					}
					while ($1 == "ErrSeparations:") {
						errSeparationsHistogram[thisPos,$2]+= $4;
						getline 
					}
				}
			}
			if (/Number of failures found:/) {
				getline;
				failureCount+=$1; lastFailureCount = $1;
			}
			if (/Of total failures, hangs \(incl. after error\):/) {
				getline;
				hangCount+= $1;
			}
			if (/Of total failures, happened after error \(incl. hangs\):/) {
				getline;
				failAfterErrCount+= $1; lastFailAfterErrCount = $1;
			}
			if (/Number of errors found \(excl. prior to failure\):/) {
				getline;
				errCount+=$1; lastErrCount = $1
			}
			if (/Accumulated number of errors in all experiments including multiple errors:/) {
				getline;
				addedErrs+=$1;
			}
			if (/Average number of Error Bit Upsets:/) {
				getline;
				avgErrBU+=$1;
			}
			if (/Maximum propagation latency for failures:/) {
				getline;
				if (maxFailLatency < $1) maxFailLatency = $1;
			}
			if (/Maximum propagation latency for errors:/) {
				getline;
				if (maxErrLatency < $1) maxErrLatency = $1;
			}
			if (/Maximum propagation latency from error to failure:/) {
				getline;
				if (maxFailAfterErrLatency < $1) maxFailAfterErrLatency = $1;
			}
			if (/Average propagation latency for failures:/) {
				getline;
				avgFailLatency+= (lastFailureCount * $1);
			}
			if (/Average propagation latency for errors:/) {
				getline;
				avgErrLatency+= ((lastErrCount+lastFailAfterErrCount) * $1);
			}
			if (/Average propagation latency from error to failure:/) {
				getline;
				avgFailAfterErrLatency+= (lastFailAfterErrCount * $1);
			}
			if (/Total Experiments excluding FaultFree:/) {
				getline;
				totalFiles+=$1;
			}
			if (/seconds:/) {
				secondsCount+=$NF;
				if (sglsecondsCount == 0) sglsecondsCount=$NF;
			}
	}
}

END{
detectedNoErr--; 
 # USE ONLY IF LAST ONE IS AN INJECTED EXPERIMENT if (faultDetected == 0) noDetCount+=1;detectedNoErr++;
 i=0; do {
			if (ARGV[ARGIND-i] ~ /Results/) { print ARGV[ARGIND-i] " is last processed file\n\n"; i=ARGIND }
			i++
		} while (i<ARGIND)
		
if (failureCount) avgFailLatency = avgFailLatency / failureCount;
if (errCount + failAfterErrCount) avgErrLatency = avgErrLatency / (errCount + failAfterErrCount);
if (failAfterErrCount) avgFailAfterErrLatency = avgFailAfterErrLatency / failAfterErrCount;


print "Distribution of Failure Latencies from fault injection instant:";
for (i in failLatencyHistogram) {
	print "failLatency: ",i, " Repetitions: ",failLatencyHistogram[i]; # >> "failLatencyHistogram.log"
}
print "\n"
print "Distribution of Error Latencies from fault injection instant:";
for (i in errLatencyHistogram) {
	print "errLatency: ",i, " Repetitions: ",errLatencyHistogram[i]; # >> "errLatencyHistogram.log"
}
print "\n"
print "Distribution of Failure After Error Latencies:";
for (i in failAfterErrLatencyHistogram) {
	print "failAfterErrLatency: ",i, " Repetitions: ",failAfterErrLatencyHistogram[i]; # >> "failAfterErrLatencyHistogram.log"
}
print "\n"

print "Distribution of accumulated total errors per position of outputs:";
for (pos in cumulativeErrors) {
	print "Position: ",pos, "\tAccumulated Errors: ",cumulativeErrors[pos];
}
print "\n"
addedRepeatedErrs=0;
print "Distribution of accumulated total error Repetitions per position of outputs:";
for (pos in cumulativeErrRepetitions) {
	if (pos != 0) { 
		addedRepeatedErrs+=cumulativeErrRepetitions[pos];
		print "Position: ",pos, " Accumulated Error Repetitions: ",cumulativeErrRepetitions[pos];
	}
}
print "\n"
addedBUs=0;
print "Distribution of accumulated and average total Bit Upsets per position of outputs:";
for (pos in cumulativeErrBitUpsets) {
	if (pos != 0) { 
		addedBUs+=cumulativeErrBitUpsets[pos];
		print "Position: ",pos, " Accumulated Error Bit Upsets: ",cumulativeErrBitUpsets[pos];
	}
}
print "--------------------"
for (pos in cumulativeErrBitUpsets) {
	if (pos != 0) { 
		print "Position: ",pos, " Average Error Bit Upsets: ",cumulativeErrBitUpsets[pos] / cumulativeErrRepetitions[pos];
	}
}
print "\n"
print "Distribution of Error Durations:";
for (pos in cumulativeErrRepetitions) {
print "POSITION ", pos;
	for (combi in errDurationsHistogram) {
		split(combi, sep, SUBSEP)
		if (sep[1] == pos) {
			print "ErrDurations: ",sep[2], " Repetitions: ",errDurationsHistogram[combi]; # >> "failAfterErrLatencyHistogram.log"
		}
	}
}
print "\n"
print "Distribution of Error Separations:";
for (pos in cumulativeErrRepetitions) {
print "POSITION ", pos;
	for (combi in errSeparationsHistogram) {
		split(combi, sep, SUBSEP)
		if (sep[1] == pos) {
			print "ErrSeparations: ",sep[2], " Repetitions: ",errSeparationsHistogram[combi]; # >> "failAfterErrLatencyHistogram.log"
		}
	}
}
print "\n"
 # print "Transients (000): \n",transCount;
 # print "Intermittents (011):\n",interCount;
 # print "Permanents (101): \n",permCount;
 # print "Other detected faults/errors (others):\n",othersCount;
 # print "Non detected experiments (110):\n",noDetCount;
 # print "Redetections (not included in previous categories):\n",faultDetectedections;
 # print "Detected faults without ERRORS in registered outputs: \n",detectedNoErr;
 # print "Undetected faults with ERRORS in registered outputs: \n",undetectedErr;
 
 if (addedRepeatedErrs) avgBUs = addedBUs / addedRepeatedErrs;
 
 print "Number of failures found: \n",failureCount;
 print "\t Of total failures, hangs (incl. after error): \n\t ",hangCount;
 print "\t Of total failures, happened after error (incl. hangs): \n\t ",failAfterErrCount;
 print "Number of errors found (excl. prior to failure): \n",errCount;
 print "Accumulated number of errors in all experiments including multiple errors: \n",addedErrs;
 print "Average number of Error Bit Upsets:  \n",avgBUs;
 print "Maximum propagation latency for failures: \n",maxFailLatency;
 print "Maximum propagation latency for errors: \n",maxErrLatency;
 print "Maximum propagation latency from error to failure: \n",maxFailAfterErrLatency;
 print "Average propagation latency for failures: \n",avgFailLatency;
 print "Average propagation latency for errors: \n",avgErrLatency;
 print "Average propagation latency from error to failure: \n",avgFailAfterErrLatency;
 print "Total Experiments excluding FaultFree: \n",totalFiles;
  
 print "Combined Analysis time. Total seconds: ", secondsCount;
 print (int(secondsCount / 3600 )),"H",(int((secondsCount % 3600) / 60)), "Min", (secondsCount % 60), "Sec";
 print "Per Packet Aprox. Analysis time. Total seconds: ", sglsecondsCount;
 print (int(sglsecondsCount / 3600 )),"H",(int((sglsecondsCount % 3600) / 60)), "Min", (sglsecondsCount % 60), "Sec";
 }