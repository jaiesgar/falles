# FILE TO RETRIEVE COMMON CLASSIFIED NODES AMONG EXPERIMENTS
# CALL: gawk  -f simGetCoincidences.awk {List of inputs} > Coincidences.log
#
# EXAMPLE: gawk  -f simGetCoincidences.awk dir1F/GatheredFailNodes_4934.log dir0F/GatheredFailNodes_4934.log > Coincidences.log
# EXAMPLE: gawk  -f simGetCoincidences.awk ResultsRTL*/GatheredFailNodes_* > Coincidences.log
# EXAMPLE: gawk  -f simGetCoincidences.awk $(<inputslist.txt) > Coincidences.log
# UPDATED:	  22-apr-2015 Matrix data manipulation to allow multiple inputs
# 					  24-mar-2015 Created


BEGIN{		filep=0;
				 PROCINFO["sorted_in"] = "@ind_num_asc" # This forces arrays to be sorted by index in numerical increasing order

			}
#	************************************************************************************************************	
{
	if (FNR ==1) {
		filep++
	}
	
	if (/Nodes which caused ERROR/) {
		getline
		while (/^\s*$/) getline
		while ($1 ~ /^[\/]/) {
			Err[filep,$1]++;
			getline
		}
	}
	
	if (/Nodes which caused FAILURE [^WITHOUT]/) {
		getline
		while (/^\s*$/) getline
		while ($1 ~ /^[\/]/) {
			Fail[filep,$1]++;
			getline
		}
	}

	if (/Nodes which caused FAILURE WITHOUT PREVIOUS ERROR/) {
		getline
		while (/^\s*$/) getline
		while ($1 ~ /^[\/]/) {
			failNoErr[filep,$1]++;
			getline
		}
	}
	
}

END{
print "Nodes which caused ERROR in EVERY CAMPAIGN ====================";
for (combined in Err) {
	split(combined,sep, SUBSEP)
	accumErr[sep[2]]++;
}
for (node in accumErr) {
	if (accumErr[node] == filep) { print node; }
}

print "\n"
print "Nodes which caused FAILURE in EVERY CAMPAIGN ====================";
for (combined in Fail) {
	split(combined,sep, SUBSEP)
	accumFail[sep[2]]++;
}
for (node in accumFail) {
	if (accumFail[node] == filep) { print node; }
}

print "\n"
print "Nodes which caused FAILURE WITHOUT PREVIOUS ERROR in EVERY CAMPAIGN ====================";
for (combined in failNoErr) {
	split(combined,sep, SUBSEP)
	accumFailNoErr[sep[2]]++;
}
for (node in accumFailNoErr) {
	if (accumFailNoErr[node] == filep) { print node; }
}

 }