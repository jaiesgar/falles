# FILE TO RETRIEVE EXCLUSIVELY CLASSIFIED NODES AMONG EXPERIMENTS
# CALL: gawk  -f simGetExclusiveness.awk {List of inputs} > Exclusiveness.log
#
# EXAMPLE: gawk  -f simGetExclusiveness.awk dir1F/GatheredFailNodes_4934.log dir0F/GatheredFailNodes_4934.log > Exclusiveness.log
# EXAMPLE: gawk  -f simGetExclusiveness.awk ResultsRTL*/GatheredFailNodes_* > Exclusiveness.log
# EXAMPLE: gawk  -f simGetExclusiveness.awk $(<inputslist.txt) > Exclusiveness.log
# UPDATED:	  23-apr-2015 Created



BEGIN{		filep=0;
				 PROCINFO["sorted_in"] = "@ind_num_asc" # This forces arrays to be sorted by index in numerical increasing order
			}
{

	if (FNR ==1) {
		filep++
		filenames[filep] = FILENAME;
	}
	
	if (/Nodes which caused ERROR/) {
		getline
		while (/^\s*$/) getline
		while ($1 ~ /^[\/]/) {
			Err[filep,$1]++;
			getline
		}
	}
	
	if (/Nodes which caused FAILURE [^WITHOUT]/) {
		getline
		while (/^\s*$/) getline
		while ($1 ~ /^[\/]/) {
			Fail[filep,$1]++;
			getline
		}
	}

	if (/Nodes which caused FAILURE WITHOUT PREVIOUS ERROR/) {
		getline
		while (/^\s*$/) getline
		while ($1 ~ /^[\/]/) {
			failNoErr[filep,$1]++;
			getline
		}
	}
	
}

END{
print "############################################################"
print "##                                      ERRORS                                              ##"
print "############################################################"
for (combined in Err) {
	split(combined,sep, SUBSEP)
	accumErr[sep[2]]++;
}
for (i=1; i<=filep; i++) {
	if (i>1) print "\n"
	print "Nodes which caused ERROR in ",filenames[i], "EXCLUSIVELY ====================";
	for (node in accumErr) {
		if (accumErr[node] == 1) {
			for (combined in Err) {
				split(combined,sep, SUBSEP)
				if (sep[2] == node && sep[1] == i) {	print node; break;}
			}				
		}
	}
}

print "\n"
print "############################################################"
print "##                                     FAILURES                                             ##"
print "############################################################"
print "\n"
for (combined in Fail) {
	split(combined,sep, SUBSEP)
	accumFail[sep[2]]++;
}
for (i=1; i<=filep; i++) {
	if (i>1) print "\n"
	print "Nodes which caused FAILURE in ",filenames[i], "EXCLUSIVELY ====================";
	for (node in accumFail) {
		if (accumFail[node] == 1) {
			for (combined in Fail) {
				split(combined,sep, SUBSEP)
				if (sep[2] == node && sep[1] == i) {	print node; break;}
			}				
		}
	}
}

print "\n"
print "############################################################"
print "##                FAILURE WITHOUT PREVIOUS ERROR                       ##"
print "############################################################"
print "\n"
for (combined in failNoErr) {
	split(combined,sep, SUBSEP)
	accumFailNoErr[sep[2]]++;
}
for (i=1; i<=filep; i++) {
	if (i>1) print "\n"
	print "Nodes which caused FAILURE WITHOUT PREVIOUS ERROR in ",filenames[i], "EXCLUSIVELY ====================";
	for (node in accumFailNoErr) {
		if (accumFailNoErr[node] == 1) {
			for (combined in failNoErr) {
				split(combined,sep, SUBSEP)
				if (sep[2] == node && sep[1] == i) {	print node; break;}
			}				
		}
	}
}

 }