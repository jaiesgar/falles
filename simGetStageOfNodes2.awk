# FILE TO RETRIEVE STAGE INVOLVED IN DIFFERENT NODES AMONG EXPERIMENTS, JOINING CACHES WITH STAGES
# CALL: gawk  -f simGetStageOfNodes2.awk {List of inputs} > StageOfNodes.log
#
# EXAMPLE: gawk  -f simGetStageOfNodes.awk dir1F/GatheredFailNodes_4934.log dir0F/GatheredFailNodes_4934.log > StageOfNodes.log
# EXAMPLE: gawk  -f simGetStageOfNodes.awk ResultsRTL*/GatheredFailNodes_* > StageOfNodes.log
# EXAMPLE: gawk  -f simGetStageOfNodes.awk $(<inputslist.txt) > StageOfNodes.log
# UPDATED:	19-may-2015 Better detection of cases
# 					  15-may-2015 Others case introduced
# 					  30-apr-2015 Relative weights of each stage introduced
# 					  29-apr-2015 Created



BEGIN{		filep=0;
				 PROCINFO["sorted_in"] = "@ind_num_asc" # This forces arrays to be sorted by index in numerical increasing order
			}
{

	if (FNR ==1) {
		filep++
		filenames[filep] = FILENAME;
	}
	
	if (/Nodes which caused ERROR/) {
		getline
		while (/^\s*$/) getline
		while ($1 ~ /^[\/]/) {
			Err[filep,$1]++;
			totErr[filep]++;
			getline
		}
	}
	
	if (/Nodes which caused FAILURE [^WITHOUT]/) {
		getline
		while (/^\s*$/) getline
		while ($1 ~ /^[\/]/) {
			Fail[filep,$1]++;
			totFail[filep]++;
			getline
		}
	}

	if (/Nodes which caused FAILURE WITHOUT PREVIOUS ERROR/) {
		getline
		while (/^\s*$/) getline
		while ($1 ~ /^[\/]/) {
			failNoErr[filep,$1]++;
			totFailNoErr[filep]++;
			getline
		}
	}
	
}

END{
# print "############################################################"
# print "##                                      ERRORS                                              ##"
# print "############################################################"

# for (i=1; i<=filep; i++) {
	# if (i>1) print "\n"
	# print "Break down of Stage of Nodes which caused ERROR in ",filenames[i], "====================";
	# print "FETCH stage: r.f or rin.f ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.f\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,1]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,1] / totErr[i]) " %\n"
	
	# print "DECODE stage: r.d or rin.d ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.d\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,2]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,2] / totErr[i]) " %\n"
	
	# print "REGISTER ACCESS stage: r.a or rin.a ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.a\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,3]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,3] / totErr[i]) " %\n"
	
	# print "EXECUTE stage: r.e or rin.e ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.e\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,4]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,4] / totErr[i]) " %\n"

	# print "MEMORY ACCESS stage: r.m or rin.m ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.m\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,5]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,5] / totErr[i]) " %\n"
	
	# print "EXCEPTION stage: r.x or rin.x ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.x\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,6]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,6] / totErr[i]) " %\n"

	# print "WRITE BACK stage: r.w or rin.w ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.w\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,7]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,7] / totErr[i]) " %\n"
	
	# print "DATA CACHE related signals: dci or dco ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/dc(i|o)\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,8]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,8] / totErr[i]) " %\n"
	
	# print "INSTRUCTION CACHE related signals: ici or ico ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/ic(i|o)\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,9]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,9] / totErr[i]) " %\n"
		
	# print "REGISTER FILE related signals: rfi or rfo ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /rf(i|o)\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesErr[i,10]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,10] / totErr[i]) " %\n"

	# print "OTHERS related signals ---------------------------------------"
	# for (combined in Err) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (!(sep[2] in printedNodes)) {
				# print sep[2]
				# stagesErr[i,11]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesErr[i,11] / totErr[i]) " %\n"
# }

# delete printedNodes;
# print "\n"
# print "############################################################"
# print "##                                     FAILURES                                             ##"
# print "############################################################"
# print "\n"

# for (i=1; i<=filep; i++) {
	# if (i>1) print "\n"
	# print "Break down of Stage of Nodes which caused FAILURE in ",filenames[i], "====================";
	# print "FETCH stage: r.f or rin.f ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.f\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,1]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,1] / totFail[i]) " %\n"
	
	# print "DECODE stage: r.d or rin.d ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.d\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,2]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,2] / totFail[i]) " %\n"
	
	# print "REGISTER ACCESS stage: r.a or rin.a ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.a\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,3]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,3] / totFail[i]) " %\n"
	
	# print "EXECUTE stage: r.e or rin.e ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.e\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,4]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,4] / totFail[i]) " %\n"

	# print "MEMORY ACCESS stage: r.m or rin.m ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.m\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,5]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,5] / totFail[i]) " %\n"
	
	# print "EXCEPTION stage: r.x or rin.x ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.x\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,6]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,6] / totFail[i]) " %\n"

	# print "WRITE BACK stage: r.w or rin.w ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/(rin|r)\.w\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,7]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,7] / totFail[i]) " %\n"
	
	# print "DATA CACHE related signals: dci or dco ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/dc(i|o)\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,8]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,8] / totFail[i]) " %\n"
	
	# print "INSTRUCTION CACHE related signals: ici or ico ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/ic(i|o)\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,9]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,9] / totFail[i]) " %\n"
		
	# print "REGISTER FILE related signals: rfi or rfo ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /rf(i|o)\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFail[i,10]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,10] / totFail[i]) " %\n"
	
	# print "OTHERS related signals ---------------------------------------"
	# for (combined in Fail) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (!(sep[2] in printedNodes)) {
				# print sep[2]
				# stagesFail[i,11]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFail[i,11] / totFail[i]) " %\n"
# }

# delete printedNodes;
print "\n"
print "############################################################"
print "##                FAILURE WITHOUT PREVIOUS ERROR                       ##"
print "############################################################"
print "\n"

for (i=1; i<=filep; i++) {
	if (i>1) print "\n"
	print "Break down of Stage of Nodes which caused FAILURE WITHOUT PREVIOUS ERROR in ",filenames[i], "====================";
	print "FETCH + INSTR. CACHE stage: r.f or rin.f  or  ici or ico ---------------------------------------"
	for (combined in failNoErr) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if ((sep[2] ~ /\/(rin|r)\.f\./) || (sep[2] ~ /\/ic(i|o)\./)) {
				print sep[2]
				printedNodes[sep[2]]++;
				stagesFailNoErr[i,1]++;
			}
		}
	}
	print "Weight of this stage: "(100 * stagesFailNoErr[i,1] / totFailNoErr[i]) " %\n"
	
	print "DECODE stage: r.d or rin.d ---------------------------------------"
	for (combined in failNoErr) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /\/(rin|r)\.d\./) {
				print sep[2]
				printedNodes[sep[2]]++;
				stagesFailNoErr[i,2]++;
			}
		}
	}
	print "Weight of this stage: "(100 * stagesFailNoErr[i,2] / totFailNoErr[i]) " %\n"
	
	print "REGISTER ACCESS stage: r.a or rin.a ---------------------------------------"
	for (combined in failNoErr) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /\/(rin|r)\.a\./) {
				print sep[2]
				printedNodes[sep[2]]++;
				stagesFailNoErr[i,3]++;
			}
		}
	}
	print "Weight of this stage: "(100 * stagesFailNoErr[i,3] / totFailNoErr[i]) " %\n"
	
	print "EXECUTE stage: r.e or rin.e ---------------------------------------"
	for (combined in failNoErr) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /\/(rin|r)\.e\./) {
				print sep[2]
				printedNodes[sep[2]]++;
				stagesFailNoErr[i,4]++;
			}
		}
	}
	print "Weight of this stage: "(100 * stagesFailNoErr[i,4] / totFailNoErr[i]) " %\n"

	print "MEMORY ACCESS + DATA CACHE stage: r.m or rin.m  OR  dci or dco---------------------------------------"
	for (combined in failNoErr) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if ((sep[2] ~ /\/(rin|r)\.m\./) || (sep[2] ~ /\/dc(i|o)\./)) {
				print sep[2]
				printedNodes[sep[2]]++;
				stagesFailNoErr[i,5]++;
			}
		}
	}
	print "Weight of this stage: "(100 * stagesFailNoErr[i,5] / totFailNoErr[i]) " %\n"
	
	print "EXCEPTION stage: r.x or rin.x ---------------------------------------"
	for (combined in failNoErr) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /\/(rin|r)\.x\./) {
				print sep[2]
				printedNodes[sep[2]]++;
				stagesFailNoErr[i,6]++;
			}
		}
	}
	print "Weight of this stage: "(100 * stagesFailNoErr[i,6] / totFailNoErr[i]) " %\n"

	print "WRITE BACK stage: r.w or rin.w ---------------------------------------"
	for (combined in failNoErr) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /\/(rin|r)\.w\./) {
				print sep[2]
				printedNodes[sep[2]]++;
				stagesFailNoErr[i,7]++;
			}
		}
	}
	print "Weight of this stage: "(100 * stagesFailNoErr[i,7] / totFailNoErr[i]) " %\n"
	
	# print "DATA CACHE related signals: dci or dco ---------------------------------------"
	# for (combined in failNoErr) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/dc(i|o)\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFailNoErr[i,8]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFailNoErr[i,8] / totFailNoErr[i]) " %\n"
	
	# print "INSTRUCTION CACHE related signals: ici or ico ---------------------------------------"
	# for (combined in failNoErr) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /\/ic(i|o)\./) {
				# print sep[2]
				# printedNodes[sep[2]]++;
				# stagesFailNoErr[i,9]++;
			# }
		# }
	# }
	# print "Weight of this stage: "(100 * stagesFailNoErr[i,9] / totFailNoErr[i]) " %\n"
		
	print "REGISTER FILE related signals: rfi or rfo ---------------------------------------"
	for (combined in failNoErr) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /rf(i|o)\./) {
				print sep[2]
				printedNodes[sep[2]]++;
				stagesFailNoErr[i,10]++;
			}
		}
	}
	print "Weight of this stage: "(100 * stagesFailNoErr[i,10] / totFailNoErr[i]) " %\n"
	
	print "OTHERS related signals ---------------------------------------"
	for (combined in failNoErr) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (!(sep[2] in printedNodes)) {
				print sep[2]
				stagesFailNoErr[i,11]++;
			}
		}
	}
	print "Weight of this stage: "(100 * stagesFailNoErr[i,11] / totFailNoErr[i]) " %\n"
}

 }