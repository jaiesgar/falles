# FILE TO RETRIEVE RESULTS FROM SIMULATIONS IN MULTIPLE LIST FILES, ANALYSING FAULT MODEL PROPAGATED TO REGISTERS ==FOR LEON3-MINIMAL==
# WILL LOOK FROM startInjTime VALUE ONWARDS
# AUTHOR: J. Espinosa
# CALL: gawk  -v startInjTime=timeInNs -v lastOutputListOrder=nrOutputs+1 -f simReadResults.awk ./* >Results.log
# PARAMETER: lastOutputLIstOrder is the number of field of the last system output, where the first possible signal position in the list is field nr. 2.
# 						 Used to differentiate failures from errors. Sequentially first we find outputs and then registers come next in the list of simInitModel.do
# UPDATED:			1-sep-2015 Debugged problem with some ErrDurations=-1 not being accounted when workload finished on time or when Failure after Error
#							17-aug-2015 Modified Upsetbits to account for X and Z values as 1 bit mismatch
#							4-jun-2015 Added break when times match and when new transitions appear to speed up, changed checking in disappeared cases, 
#												changed display or ref values for errorrs shown in results
#					 		6-may-2015 Analysis of each line now continues until last position even after finding previous mismatch in that line
#							4-mar-2015 Commented out "found in injTable"
#							3-mar-2015 lastOutputListOrder now comes as variable
#							4-feb-2015 Added histogram of latencies and averages
#							29-jan-2015 Debugged for allowing detection of hang in last processed file of a Packet
#							28-jan-2015 Changed disappeared transitions from always failure to failure or error depending on expected position of change
#							27-jan-2015 Added counter for failure after previous error and counter for hang failures, and latencies for fail after err 
# 							21-jan-2015 Delta collapse and delta none compatible

function restart()
{
	# RESTARTS INPUT ARGUMENTS TO READ, TO PROCESS TWICE THE INPUT FILES
    # shift remaining arguments up
    for (i = ARGC; i > 0; i--)
        ARGV[i-1+ARGIND] = ARGV[i-1]

    # make sure gawk knows to keep going
    ARGC+=ARGIND

    # continue
    nextfile
}

function dec2binstr(dec,lw,		mask,bin)
{
	if (dec == 0)
        bin = "0"
		
    mask = 1
    for (; dec != 0; dec = rshift(dec, 1))
        bin = (and(dec, mask) ? "1" : "0") bin

    if (lw != 0) {
		while ((length(bin) % lw) != 0)
			bin = "0" bin
	} else {
		while ((length(bin) % 4) != 0)
			bin = "0" bin
	}
	return bin
}

function Upsetbits(word,ref,		wbin,rbin,ubits)
{
# RETURNS NUMBER OF MISMATCHED BITS IN 2 WORDS WHICH ARE IN HEX
# We consider 1 affected bit every time a hex digit is affected, produces an error of
# up to 300% for X values due to hex codification
	ubits = 0;
	starthex = 1;
	for (b =1;b<=length(word);b++) {
		myw = substr(word, b,1);
		if (myw !~ /[A-Fa-f0-9]/) {
			wbin = "0x" substr(word, starthex, (b-1));
			lw = 4 * (length(wbin) -2);
			 # print wbin   #debug
			wbin = strtonum(wbin); # string to decimal
			 # print wbin   #debug
			wbin = dec2binstr(wbin,lw); # decimal to binary string 
			rbin = "0x" substr(ref,starthex,(b-1));
			lr = 4 * (length(rbin)-2);
			 # print rbin	#debug
			rbin = strtonum(rbin);
			 # print rbin	#debug
			rbin = dec2binstr(rbin,lr);
			 # print wbin   #debug
			 # print rbin	#debug		
			for (bt =1;bt<=length(wbin);bt++)
				if (substr(wbin, bt,1) != substr(rbin,bt,1))
					ubits++;
			if (substr(word, b,1) != substr(ref,b,1))
				ubits++; # we count only 1 wrong bit when an hex is wrong
			starthex=b+1;
		}
	}
	
	if (starthex <= length(word)) {
		wbin = "0x" substr(word, starthex);
		lw = 4 * (length(word) - starthex + 1);
		wbin = strtonum(wbin); # string to decimal
		 # print wbin   #debug
		wbin = dec2binstr(wbin,lw); # decimal to binary string 
		rbin = "0x" substr(ref,starthex);
		lr = 4 * (length(ref) - starthex + 1);
		rbin = strtonum(rbin);
		 # print rbin	#debug
		rbin = dec2binstr(rbin,lr);
		 # print wbin   #debug
		 # print rbin	#debug
		for (bt =1;bt<=length(wbin);bt++)
			if (substr(wbin, bt,1) != substr(rbin,bt,1))
				ubits++;
	}
		
	return ubits
}



BEGIN{tickCapture=0;transCount=0;interCount=0;permCount=0;othersCount=0;noDetCount=0;faultDetectedections=0;totalFiles=0;faultDetected=-1;
goldRun=1;w=0;v=0;undetectedErr=0;lastFail=""; lastErr= "";detectedNoErr=0; detectedErr=0; failureCount=0;errCount=0; failAfterErrCount =0; hangCount=0; lastLine[0]=0;
lastErrPositions[0]=0; cumulativeErrors[0]=0;
# lastErrRepetitions[0]=0; cumulativeErrRepetitions[0]=0;lastErrBitUpsets[0]=0;cumulativeErrBitUpsets[0]=0;
# errSeparationsHistogram[0][0]=0;errDurationsHistogram[0][0]=0;errActive[0]=0;lastErrActivationTimes[0]=0;lastErrDeactivationTimes[0]=0;
PROCINFO["sorted_in"] = "@ind_num_asc" # This forces arrays to be sorted by index in numerical increasing order

	maxFailLatency = 0; avgFailLatency=0;
	maxFailAfterErrLatency = 0; avgFailAfterErrLatency=0; lastErrTime = 0; 
	maxErrLatency = 0; prevErrLatency = 0; lastErrLatency = 0; avgErrLatency = 0;
	
	
	# WE GET LIST NR. EXPERIMENT | TIME OF INJECTION FROM FILE Injections.log
	nn=split (ARGV[1] ,dir, "/")
	injFile=dir[1]; for (n =2; n <= nn-1; n++) injFile=injFile "/"dir[n]; injFile = injFile"/Injections.log";
	if( system( "[ -f " injFile " ] " )  != 0 ) { injFile=dir[1]; for (n =2; n <= nn-2; n++) injFile=injFile "/"dir[n]; injFile = injFile"/Injections.log"; }
	if( system( "[ -f " injFile " ] " )  != 0 ) { print "EXITING! injFile not found: "; print injFile; }
	if( system( "[ -f " injFile " ] " )  != 0 ) exit;
	i=1;
	while(( getline l < injFile) > 0 ) {
		if (l ~ /MOMENT/) {
			split (l, linelist, " ");
			injTable[linelist[2]] = linelist[10];     # check for multiple injections (intermittent)
			i++;
		} 
	 }
	 # for (y in injTable) print "index " y ": " injTable[y] # debug
}   # end of BEGIN
		
{

	###### TO SAVE OUTPUTS OF GOLDEN RUN FOR LATER COMPARISON
	if (FILENAME ~ /FaultFree/ && goldRun == 1) {
		# print FILENAME,goldRun, startInjTime # debug 
				if ($1 ~ /^[0-9]+/ && $1 > startInjTime) {
					goldRes[w,0] = $1;
					($2 ~ /^+[0-9]+/)? afterdelta = 3 : afterdelta = 2
					for (v = afterdelta; v<=NF; v++) {
						goldRes[w,v] = $v;
					}
					w++;
				# print FILENAME,goldRun, "after" # debug 			
				}
	} else if (w!=0 && goldRun == 1) {
		goldRun=0;
		restart();
	} else {
	# print FILENAME,goldRun # debug 
	# ############################################
		if (goldRun == 0) {
	
				if (FILENAME ~ /.lst/) {
					if (FNR==1) {       # a transition has disappeared at the end, wich was present in goldenRun -------------------------
							# FOR FAULTS DETECTION SYSTEMS
								# if (faultDetected == 0) {
										# noDetCount+=1;print ARGV[ARGIND-1], "A FAULT WAS NOT DETECTED"; 
										# if (ARGV[ARGIND-1] == lastFail) {print lastFail,"glups! This undetected fault caused output failure";undetectedFail++}
								# } else {
										# if (ARGV[ARGIND-1] != lastFail) {detectedNoFail++} else {detectedFail++}
								# }
								
								# Next block is to check if previous file finished transitions before expected 
								if (lastLine[1] ~ /[0-9]+/) {
									if (lastLine[1] < goldRes[w-1,0]) {
									
												if (lastFail != lastFilename) {
															split(lastFilename,auxl,"Fault0")
															split(auxl[2],auxl2,".lst")
															if (maxFailLatency <= (lastLine[1] - injTable[(auxl2[1]+0)]) ) {
																	maxFailLatency = (lastLine[1] - injTable[(auxl2[1]+0)]) }
													failureCount++; hangCount++;
													failLatencyHistogram[(lastLine[1] - injTable[(auxl2[1]+0)])]++;
													avgFailLatency+= (lastLine[1] - injTable[(auxl2[1]+0)])
													print "\t\t\t\t\tFAILURE in ", lastFilename
													lastFail = lastFilename; 
													ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
													for (c=0; c<= v; c++) {
														lastFailVal[c] = lastLine[c]
														 if ((w-1,c) in goldRes) {
																lastFailGoldVal[c] = goldRes[0,c];
																if (c==0) {print goldRes[w-1,c], "\t"; } else {print goldRes[w-1,c],""; }
														}
													}								
													print "\n\t\t\t\t\tThis experiment outputs:\t";
													for (c=1;c<=v;c++) { if (c >= afterdelta) print lastLine[c],""; if (c==1) print lastLine[c],"\t";}
													ORS="\n";	 print "\n\t\t\t\t\tTransition(s) missed at the end"
													if (lastErr == lastFilename) { 
														errCount--; print "\t\t\t\t\tFound failure with previous Error in current file"
														failAfterErrCount++; 
														failAfterErrLatencyHistogram[(lastLine[1] - lastErrTime)]++;
														avgFailAfterErrLatency+= (lastLine[1] - lastErrTime)
														if (maxFailAfterErrLatency <= (lastLine[1] - lastErrTime) ) {
																	maxFailAfterErrLatency = (lastLine[1] - lastErrTime) }
													}
												}
									}
									
									for (i=afterdelta; i<=NF; i++) {
											if (i in errActive) {
												delete errActive[i];
												value= -1;
												errDurationsHistogram[i,value] +=1;
											}
									}
								
								}
								
									
								for (pos in lastErrPositions) {  			# lastErrPositions is a vector of 1 in the positions of registers with error 
									cumulativeErrors[pos] += lastErrPositions[pos]
								}
								delete lastErrPositions
								
								for (pos in lastErrRepetitions) {  		# vector of nr of repetitions in the positions of registers with error 
									cumulativeErrRepetitions[pos] += lastErrRepetitions[pos]
								}
								delete lastErrRepetitions
								
								for (pos in lastErrBitUpsets) {  		# vector of total nr of upset bits for each reg with error
									cumulativeErrBitUpsets[pos] += lastErrBitUpsets[pos]
								}
								delete lastErrBitUpsets
								
								
								faultDetected=0; totalFiles+=1;last="";nextkVal=0; delete lastFailGoldVal; delete lastFailVal; delete lastErrGoldVal; delete lastErrVal;
								delete errActive; delete lastErrActivationTimes; delete lastErrDeactivationTimes;
					}
			


					if ($1 > startInjTime) {
						# FOR FAULTS DETECTION SYSTEMS
						# tickCapture used to compare only in falling edge of signal $3 (clk)
						# if ($3 == 1) tickCapture=1;
						# if ($3 == 0 && tickCapture ==1 && $4 !~ /110/) {
									# # print FILENAME;
									# # print;
									# if (faultDetected==0) {
										# if ($4 ~ /000/) {transCount+=1;last="trans"}
										# if ($4 ~ /011/) {interCount+=1;last="inter"}
										# if ($4 ~ /101/) {permCount+=1;last="perm"}
										# if ($4 !~ /000/ && $4 !~ /011/ && $4 !~ /101/) {othersCount+=1;last="other"}
									# }
									
									# if (faultDetected==1) {
										# if ($4 !~/101/) {faultDetectedections+=1;}
										# else { if (last!="perm") {  permCount+=1;}
												  # if (last=="trans") {transCount-=1;last="perm";} else if (last=="inter") {interCount-=1;last="perm"} else if (last=="other") {othersCount-=1;last="perm"}
										# }
									# }
									# faultDetected=1;
									 # # # # print transCount; print interCount; print permCount;print othersCount; print last;
						# }
						# if ($3 == 0) tickCapture=0;
						
						
						################# TO COMPARE WITH GOLDEN RUN OUTPUTS ############
						for (k = nextkVal; k<w; k++) {
							if ($1 == goldRes[k,0]) {		# The transition times match ------------------------------------
								mismatch=0; nextkVal = k+1;
								for (i=afterdelta; i<=NF; i++) {
									if ($i != goldRes[k,i]) {
											if ( (i <= lastOutputListOrder) || (!(i in errActive)) )
												mismatch=i; 
									}
									else {
										if (i in errActive) {
											lastErrDeactivationTimes[i] = $1;
											delete errActive[i];
											value= lastErrDeactivationTimes[i] - lastErrActivationTimes[i];
											errDurationsHistogram[i,value] +=1;
											print "This position ", i ," had an error active and is deactivated in: ",$1
											print "Its duration is: ",value
										}
									}
								
									if (mismatch !=0) {
										if (mismatch <=lastOutputListOrder) {
											if (lastFail != FILENAME) {
														split(FILENAME,auxl,"Fault0")
														split(auxl[2],auxl2,".lst")
														if (maxFailLatency <= ($1 - injTable[(auxl2[1]+0)]) ) {
																maxFailLatency = ($1 - injTable[(auxl2[1]+0)]) }
														# if (auxl2[1] in injTable) print "found in injTable"
														# print "$1: " $1 " auxl2[1]: " auxl2[1] " injTable[(auxl2[1]+0)]: " injTable[(auxl2[1]+0)]
												failureCount++;
												failLatencyHistogram[($1 - injTable[(auxl2[1]+0)])]++;
												avgFailLatency+= ($1 - injTable[(auxl2[1]+0)]);
												print "\t\t\t\t\tFAILURE in ", FILENAME
												lastFail = FILENAME; split($0,lastFailVal," ");
												ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
												for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
													lastFailGoldVal[c] = goldRes[k,c];
													if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
												}
												print "\n\t\t\t\t\tThis experiment outputs:\t"
												for (c=1;c<=v;c++) { if (c >= afterdelta) print lastFailVal[c],""; if (c==1) print lastFailVal[c],"\t";}
												print "\n";  ORS="\n";
												if (lastErr == FILENAME) { 
													errCount--; print "\t\t\t\t\tFound failure with previous Error in current file"
													failAfterErrCount++;
													failAfterErrLatencyHistogram[($1 - lastErrTime)]++;
													avgFailAfterErrLatency+= ($1 - lastErrTime)
													if (maxFailAfterErrLatency <= ($1 - lastErrTime) ) {
																maxFailAfterErrLatency = ($1 - lastErrTime) }
																
													for (i=afterdelta; i<=NF; i++) {
															if (i in errActive) {
																delete errActive[i];
																value= -1;
																errDurationsHistogram[i,value] +=1;
															}
													}
													
												}
												nextfile
											}
										} else {
											if (!(mismatch in errActive)) {
												lastErrActivationTimes[mismatch] = $1;
												errActive[mismatch] = 1;
												print "This position ", mismatch ," has found an error in: ",$1
											}
											
											if (lastErr != FILENAME) {
														split(FILENAME,auxl,"Fault0")
														split(auxl[2],auxl2,".lst")
														lastErrLatency = ($1 - injTable[(auxl2[1]+0)]);
														lastErrTime = $1;
														if (maxErrLatency <= lastErrLatency ) {
																prevErrLatency = maxErrLatency;
																maxErrLatency = ($1 - injTable[(auxl2[1]+0)]) }
												errCount++;
												errLatencyHistogram[($1 - injTable[(auxl2[1]+0)])]++;
												avgErrLatency+= ($1 - injTable[(auxl2[1]+0)]);
												print "\t\t\t\t\tERROR in ", FILENAME
												lastErr = FILENAME; split($0,lastErrVal," ");
												ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
												for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
													lastErrGoldVal[c] = goldRes[k,c];
													if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
												}
												print "\n\t\t\t\t\tThis experiment outputs:\t"
												for (c=1;c<=v;c++) { if (c >= afterdelta) print lastErrVal[c],""; if (c==1) print lastErrVal[c],"\t";}
												print "\n";  ORS="\n";
												lastErrPositions[mismatch] = 1;
												lastErrRepetitions[mismatch] = 1;
												lastErrBitUpsets[mismatch] = Upsetbits($mismatch, goldRes[k,mismatch])
											}
											else {
												lastErrRepetitions[mismatch] += 1;
												lastErrBitUpsets[mismatch] += Upsetbits($mismatch, goldRes[k,mismatch])
												
												if (!(mismatch in lastErrPositions)) {
													print "\t\t\t\t\tANOTHER ERROR in ", FILENAME
													ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
													for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
														if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
													}
													print "\n\t\t\t\t\tThis experiment outputs:\t"
													for (c=1;c<=v;c++) { if (c >= afterdelta) print $c,""; if (c==1) print $c,"\t";}
													print "\n";  ORS="\n";
													lastErrPositions[mismatch] = 1;
												}
												else {
													# repeated mismatch in a specific register
													print "\t\t\t\t\tREPEATED ERROR in ", FILENAME
													ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
													for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
														if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
													}
													print "\n\t\t\t\t\tThis experiment outputs:\t"
													for (c=1;c<=v;c++) { if (c >= afterdelta) print $c,""; if (c==1) print $c,"\t";}
													print "\n";  ORS="\n";
													value= lastErrActivationTimes[mismatch] - lastErrDeactivationTimes[mismatch];
													errSeparationsHistogram[mismatch,value] +=1;
												}
											}
										}
										# break
									}
									mismatch=0; # reset for analysis of next position
								}
								break	# to stop checking later times in goldenRun when match found
							
							# a new transition time has appeared which was not in the goldenRun ----------------------------
							} else if ($1 > goldRes[k-1,0] && $1 < goldRes[k,0]) {
								mismatch = 0; nextkVal=k;
								for (i=afterdelta; i<=NF; i++) {
									if ($i != goldRes[k-1,i]) {
										if ( (i <= lastOutputListOrder) || (!(i in errActive)) )
											mismatch=i;
									}
									else {
										if (i in errActive) {
											lastErrDeactivationTimes[i] = $1;
											delete errActive[i];
											value= lastErrDeactivationTimes[i] - lastErrActivationTimes[i];
											errDurationsHistogram[i,value] +=1;
											print "This position ", i ," had an error active and is deactivated in: ",$1
											print "Its duration is: ",value
										}
									}
								
									if (mismatch !=0) {
										if (mismatch <=lastOutputListOrder) {
											if (lastFail != FILENAME) {
														split(FILENAME,auxl,"Fault0")
														split(auxl[2],auxl2,".lst")
														if (maxFailLatency <= ($1 - injTable[(auxl2[1]+0)]) ) {
																maxFailLatency = ($1 - injTable[(auxl2[1]+0)]) }
												failureCount++;
												failLatencyHistogram[($1 - injTable[(auxl2[1]+0)])]++;
												avgFailLatency+= ($1 - injTable[(auxl2[1]+0)]);
												print "\t\t\t\t\tFAILURE in ", FILENAME
												lastFail = FILENAME; split($0,lastFailVal," ");
												ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
												for (c=0;c<=NF;c++) if ((k-1,c) in goldRes) {
													lastFailGoldVal[c] = goldRes[k-1,c];
													if (c==0) {print goldRes[k-1,c], "\t"; } else {print goldRes[k-1,c],""; }
												}
												print "\n\t\t\t\t\tThis experiment outputs:\t"
												for (c=1;c<=v;c++) { if (c >= afterdelta) print lastFailVal[c],""; if (c==1) print lastFailVal[c],"\t";}
												print "\n";  ORS="\n";
												print "\t\t\t\t\tNew transition appeared in position: ",mismatch, " and instant: ",$1
												if (lastErr == FILENAME) { 
													errCount--; print "\t\t\t\t\tFound failure with previous Error in current file"
													failAfterErrCount++;
													failAfterErrLatencyHistogram[($1 - lastErrTime)]++;
													avgFailAfterErrLatency+= ($1 - lastErrTime)
													if (maxFailAfterErrLatency <= ($1 - lastErrTime) ) {
																maxFailAfterErrLatency = ($1 - lastErrTime) }
																
													for (i=afterdelta; i<=NF; i++) {
															if (i in errActive) {
																delete errActive[i];
																value= -1;
																errDurationsHistogram[i,value] +=1;
															}
													}
													
												}
												nextfile
											}
										} else {
											if (!(mismatch in errActive)) {
												lastErrActivationTimes[mismatch] = $1;
												errActive[mismatch] = 1;
												print "This position ", mismatch ," has found an error in: ",$1
											}
											
											if (lastErr != FILENAME) {
														split(FILENAME,auxl,"Fault0")
														split(auxl[2],auxl2,".lst")
														lastErrLatency = ($1 - injTable[(auxl2[1]+0)]);
														lastErrTime = $1;
														if (maxErrLatency <= lastErrLatency ) {
																prevErrLatency = maxErrLatency;
																maxErrLatency = ($1 - injTable[(auxl2[1]+0)]) }
												errCount++;
												errLatencyHistogram[($1 - injTable[(auxl2[1]+0)])]++;
												avgErrLatency+= ($1 - injTable[(auxl2[1]+0)]);
												print "\t\t\t\t\tERROR in ", FILENAME
												lastErr = FILENAME; split($0,lastErrVal," ");
												ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
												for (c=0;c<=NF;c++) if ((k-1,c) in goldRes) {
													lastErrGoldVal[c] = goldRes[k-1,c];
													if (c==0) {print goldRes[k-1,c], "\t"; } else {print goldRes[k-1,c],""; }
												}
												print "\n\t\t\t\t\tThis experiment outputs:\t"
												for (c=1;c<=v;c++)  { if (c >= afterdelta) print lastErrVal[c],""; if (c==1) print lastErrVal[c],"\t";}
												print "\n";  ORS="\n";
												print "\t\t\t\t\tNew transition appeared in position: ",mismatch, " and instant: ",$1
												lastErrPositions[mismatch] = 1;
												lastErrRepetitions[mismatch] = 1;
												lastErrBitUpsets[mismatch] = Upsetbits($mismatch, goldRes[k-1,mismatch])
											}
											else {
												lastErrRepetitions[mismatch] += 1;
												lastErrBitUpsets[mismatch] += Upsetbits($mismatch, goldRes[k-1,mismatch])

												if (!(mismatch in lastErrPositions)) {
													print "\t\t\t\t\tANOTHER ERROR in ", FILENAME
													ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
													for (c=0;c<=NF;c++) if ((k-1,c) in goldRes) {
														if (c==0) {print goldRes[k-1,c], "\t"; } else {print goldRes[k-1,c],""; }
													}
													print "\n\t\t\t\t\tThis experiment outputs:\t"
													for (c=1;c<=v;c++) { if (c >= afterdelta) print $c,""; if (c==1) print $c,"\t";}
													print "\n";  ORS="\n";
													print "\t\t\t\t\tNew transition appeared in position: ",mismatch, " and instant: ",$1
													lastErrPositions[mismatch] = 1;
												}
												else {
													# repeated mismatch in a specific register
													print "\t\t\t\t\tREPEATED ERROR in ", FILENAME
													ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
													for (c=0;c<=NF;c++) if ((k-1,c) in goldRes) {
														if (c==0) {print goldRes[k-1,c], "\t"; } else {print goldRes[k-1,c],""; }
													}
													print "\n\t\t\t\t\tThis experiment outputs:\t"
													for (c=1;c<=v;c++) { if (c >= afterdelta) print $c,""; if (c==1) print $c,"\t";}
													print "\n";  ORS="\n";
													print "\t\t\t\t\tNew transition appeared in position: ",mismatch, " and instant: ",$1
													value= lastErrActivationTimes[mismatch] - lastErrDeactivationTimes[mismatch];
													errSeparationsHistogram[mismatch,value] +=1;
												}
											}
										}
									}
									mismatch=0; # reset for analysis of next position
								}
								break	# to stop checking later times in goldenRun until next entry

							# disappeared cases
							# a transition time has disappeared which was present in the goldenRun --------------------------------------
							} else if ($1 ~/[0-9]+/ && $1 > goldRes[k,0]) {
								mismatch = 0; # (nextkVal = k+1;)
								for (i=afterdelta; i<=NF; i++) {
									# USE THE NEXT BLOCK FOR MARKING AS WRONG A DELAYED OUTPUT WITH CORRECT VALUES
									# if (goldRes[k-1,i] != goldRes[k,i]) {
									if (goldRes[k,i] != lastLine[i]) {
										if ( (i <= lastOutputListOrder) || (!(i in errActive)) )
											mismatch = i;
									}
									# OR USE THE NEXT BLOCK TO ACCEPT AS GOOD A CORRECT DELAYED OUTPUT
									# if ($i != goldRes[k,i]) {
									else {
										if (i in errActive) {
											lastErrDeactivationTimes[i] = goldRes[k,0];
											delete errActive[i];
											value= lastErrDeactivationTimes[i] - lastErrActivationTimes[i];
											errDurationsHistogram[i,value] +=1;
											print "This position ", i ," had an error active and is deactivated in: ",goldRes[k,0]
											print "Its duration is: ",value
										}
									}
								
									if (mismatch !=0) {
										if (mismatch <=lastOutputListOrder) {
											if (lastFail != FILENAME) {
														split(FILENAME,auxl,"Fault0")
														split(auxl[2],auxl2,".lst")
														if (maxFailLatency < (goldRes[k,0] - injTable[(auxl2[1]+0)]) ) {
																maxFailLatency = (goldRes[k,0] - injTable[(auxl2[1]+0)]) }
												failureCount++;
												failLatencyHistogram[(goldRes[k,0] - injTable[(auxl2[1]+0)])]++;
												avgFailLatency+= (goldRes[k,0] - injTable[(auxl2[1]+0)]);
												print "\t\t\t\t\tFAILURE in ", FILENAME
												lastFail = FILENAME; #split($0,lastFailVal," ");
												ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
												for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
													lastFailGoldVal[c] = goldRes[k,c];
													if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
												}
												print "\n\t\t\t\t\tThis experiment outputs:\t"
												for (c=1;c<=v;c++) { if (c>=afterdelta) print lastLine[c],""; if (c==1) print lastline[c],"\t"; };
												print "\n";  ORS="\n";
												print "\t\t\t\t\tExpected transition has disappeared in position: ",mismatch, " and instant: ",goldRes[k,0]
												if (lastErr == FILENAME) { 
													errCount--; print "\t\t\t\t\tFound failure with previous Error in current file"
													failAfterErrCount++;
													failAfterErrLatencyHistogram[($1 - lastErrTime)]++;
													avgFailAfterErrLatency+= ($1 - lastErrTime)
													if (maxFailAfterErrLatency <= ($1 - lastErrTime) ) {
																maxFailAfterErrLatency = ($1 - lastErrTime) }
													
													for (i=afterdelta; i<=NF; i++) {
															if (i in errActive) {
																delete errActive[i];
																value= -1;
																errDurationsHistogram[i,value] +=1;
															}
													}
													
												}
												nextfile
											}
										} else {
											if (!(mismatch in errActive)) {
												lastErrActivationTimes[mismatch] = goldRes[k,0];
												errActive[mismatch] = 1;
												print "This position ", mismatch ," has found an error in: ",goldRes[k,0]
											}
											
											if (lastErr != FILENAME) {
														split(FILENAME,auxl,"Fault0")
														split(auxl[2],auxl2,".lst")
														lastErrLatency = (goldRes[k,0] - injTable[(auxl2[1]+0)]);
														lastErrTime = goldRes[k,0];
														if (maxErrLatency <= lastErrLatency ) {
																prevErrLatency = maxErrLatency;
																maxErrLatency = (goldRes[k,0] - injTable[(auxl2[1]+0)]) }
												errCount++;
												errLatencyHistogram[(goldRes[k,0] - injTable[(auxl2[1]+0)])]++;
												avgErrLatency+= (goldRes[k,0] - injTable[(auxl2[1]+0)]);
												print "\t\t\t\t\tERROR in ", FILENAME
												lastErr = FILENAME; # split(lastLine,lastErrVal," ");
												ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
												for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
													lastErrGoldVal[c] = goldRes[k,c];
													if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
												}
												print "\n\t\t\t\t\tThis experiment outputs:\t"
												for (c=1;c<=v;c++) { if (c >= afterdelta) print lastLine[c],""; if (c==1) print lastLine[c],"\t";}
												print "\n";  ORS="\n";
												print "\t\t\t\t\tExpected transition has disappeared in position: ",mismatch, " and instant: ",goldRes[k,0]
												lastErrPositions[mismatch] = 1;
												lastErrRepetitions[mismatch] = 1;
												lastErrBitUpsets[mismatch] = Upsetbits(goldRes[k,mismatch], lastLine[mismatch])
											}
											else {
												lastErrRepetitions[mismatch] += 1;
												lastErrBitUpsets[mismatch] += Upsetbits(goldRes[k,mismatch], lastLine[mismatch])

												if (!(mismatch in lastErrPositions)) {
													print "\t\t\t\t\tANOTHER ERROR in ", FILENAME
													ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
													for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
														if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
													}
													print "\n\t\t\t\t\tThis experiment outputs:\t"
													for (c=1;c<=v;c++) { if (c >= afterdelta) print lastLine[c],""; if (c==1) print lastLine[c],"\t";}
													print "\n";  ORS="\n";
													print "\t\t\t\t\tExpected transition has disappeared in position: ",mismatch, " and instant: ",goldRes[k,0]
													lastErrPositions[mismatch] = 1;
												}
												else {
													# repeated mismatch in a specific register
													print "\t\t\t\t\tREPEATED ERROR in ", FILENAME
													ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
													for (c=0;c<=NF;c++) if ((k,c) in goldRes) {
														if (c==0) {print goldRes[k,c], "\t"; } else {print goldRes[k,c],""; }
													}
													print "\n\t\t\t\t\tThis experiment outputs:\t"
													for (c=1;c<=v;c++) { if (c >= afterdelta) print lastLine[c],""; if (c==1) print lastLine[c],"\t";}
													print "\n";  ORS="\n";
													print "\t\t\t\t\tExpected transition has disappeared in position: ",mismatch, " and instant: ",goldRes[k,0]
													value= lastErrActivationTimes[mismatch] - lastErrDeactivationTimes[mismatch];
													errSeparationsHistogram[mismatch,value] +=1;
												}
											}
										}
									}
									mismatch=0; # reset for analysis of next position
								}
							}
							
						}  # end of for-k loop to check multiple new transitions or missings
						
						# endTime cases
						# a new transition time has appeared at the end which was not in the goldenRun  -------------------------------------------
						if ($1 ~/[0-9]+/ && $1 > goldRes[w-1,0]) {
								mismatch = 0;
								for (i=afterdelta; i<=NF; i++) {
									if ($i != goldRes[w-1,i]) {
										mismatch=i;
									}
									else {
										if (i in errActive) {
											lastErrDeactivationTimes[i] = $1;
											delete errActive[i];
											value= lastErrDeactivationTimes[i] - lastErrActivationTimes[i];
											errDurationsHistogram[i,value] +=1;
											print "This position ", i ," had an error active and is deactivated in: ",$1
											print "Its duration is: ",value
										}
									}
								
								
									if (mismatch !=0 && mismatch <=lastOutputListOrder) {
										if (lastFail != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													if (maxFailLatency < ($1 - injTable[(auxl2[1]+0)]) ) {
															maxFailLatency = ($1 - injTable[(auxl2[1]+0)]) }
											failureCount++;
											failLatencyHistogram[($1 - injTable[(auxl2[1]+0)])]++;
											avgFailLatency+= ($1 - injTable[(auxl2[1]+0)]);
											print "\t\t\t\t\tFAILURE in ", FILENAME
											lastFail = FILENAME; split($0,lastFailVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((w-1,c) in goldRes) {
												lastFailGoldVal[c] = goldRes[w-1,c];
												if (c==0) {print goldRes[w-1,c], "\t"; } else {print goldRes[w-1,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++)  { if (c >= afterdelta) print lastFailVal[c],""; if (c==1) print lastFailVal[c],"\t";}
											print "\n";  ORS="\n";
											print "\t\t\t\t\tNew transition appeared at the end in position: ",mismatch
											if (lastErr == FILENAME) { 
													errCount--; print "\t\t\t\t\tFound failure with previous Error in current file"
													failAfterErrCount++;
													failAfterErrLatencyHistogram[($1 - lastErrTime)]++;
													avgFailAfterErrLatency+= ($1 - lastErrTime)
													if (maxFailAfterErrLatency <= ($1 - lastErrTime) ) {
																maxFailAfterErrLatency = ($1 - lastErrTime) }
																
													for (i=afterdelta; i<=NF; i++) {
															if (i in errActive) {
																delete errActive[i];
																value= -1;
																errDurationsHistogram[i,value] +=1;
															}
													}
											
											}
											nextfile
										}
									}
									if (mismatch !=0 && mismatch  > lastOutputListOrder) {
										if (!(mismatch in errActive)) {
											lastErrActivationTimes[mismatch] = goldRes[k,0];
											errActive[mismatch] = 1;
											print "This position ", mismatch ," has found an error in: ",goldRes[k,0]
										}
										
										if (lastErr != FILENAME) {
													split(FILENAME,auxl,"Fault0")
													split(auxl[2],auxl2,".lst")
													lastErrLatency = ($1 - injTable[(auxl2[1]+0)]);
													lastErrTime = $1;
													if (maxErrLatency <= lastErrLatency ) {
															prevErrLatency = maxErrLatency;
															maxErrLatency = ($1 - injTable[(auxl2[1]+0)]) }
											errCount++;
											errLatencyHistogram[($1 - injTable[(auxl2[1]+0)])]++;
											avgErrLatency+= ($1 - injTable[(auxl2[1]+0)]);
											print "\t\t\t\t\tERROR in ", FILENAME
											lastErr = FILENAME; split($0,lastErrVal," ");
											ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
											for (c=0;c<=NF;c++) if ((w-1,c) in goldRes) {
												lastErrGoldVal[c] = goldRes[w-1,c];
												if (c==0) {print goldRes[w-1,c], "\t"; } else {print goldRes[w-1,c],""; }
											}
											print "\n\t\t\t\t\tThis experiment outputs:\t"
											for (c=1;c<=v;c++)  { if (c >= afterdelta) print lastErrVal[c],""; if (c==1) print lastErrVal[c],"\t";}
											print "\n";  ORS="\n";
											print "\t\t\t\t\tNew transition appeared at the end in position: ",mismatch
											lastErrPositions[mismatch] = 1;
											lastErrRepetitions[mismatch] = 1;
											lastErrBitUpsets[mismatch] = Upsetbits($mismatch, goldRes[w-1,mismatch])
										}
										else {
											lastErrRepetitions[mismatch] += 1;
											lastErrBitUpsets[mismatch] += Upsetbits($mismatch, goldRes[w-1,mismatch])

											if (!(mismatch in lastErrPositions)) {
												print "\t\t\t\t\tANOTHER ERROR in ", FILENAME
												ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
												for (c=0;c<=NF;c++) if ((w-1,c) in goldRes) {
													if (c==0) {print goldRes[w-1,c], "\t"; } else {print goldRes[w-1,c],""; }
												}
												print "\n\t\t\t\t\tThis experiment outputs:\t"
												for (c=1;c<=v;c++) { if (c >= afterdelta) print $c,""; if (c==1) print $c,"\t";}
												print "\n";  ORS="\n";
												print "\t\t\t\t\tNew transition appeared at the end in position: ",mismatch
												lastErrPositions[mismatch] = 1;
											}
											else {
												# repeated mismatch in a specific register
												value= lastErrActivationTimes[mismatch] - lastErrDeactivationTimes[mismatch];
												errSeparationsHistogram[mismatch,value] +=1;
											}
										}
									}
									mismatch=0; # reset for analysis of next position
								}
						}
						
						#########
					}					
				}	
		}  else { nextfile}
	}

	if ($1 ~ /[0-9]+/) { 				# auxiiliarys To check if transition has disappeared at the end which was present in GoldenRun
		split($0,lastLine," ");
		lastFilename= FILENAME;
	}

}
END{
detectedNoErr--; 
# USE ONLY IF LAST ONE IS AN INJECTED EXPERIMENT if (faultDetected == 0) noDetCount+=1;detectedNoErr++;
 		
## Detection of last file hang
			if (lastLine[1] ~ /[0-9]+/) {
				if (lastLine[1] < goldRes[w-1,0]) {
							if (lastFail != lastFilename) {
										split(lastFilename,auxl,"Fault0")
										split(auxl[2],auxl2,".lst")
										if (maxFailLatency <= (lastLine[1] - injTable[(auxl2[1]+0)]) ) {
												maxFailLatency = (lastLine[1] - injTable[(auxl2[1]+0)]) }
								failureCount++; hangCount++;
								failLatencyHistogram[(lastLine[1] - injTable[(auxl2[1]+0)])]++;
								avgFailLatency+= (lastLine[1] - injTable[(auxl2[1]+0)]);
								print "\t\t\t\t\tFAILURE in ", lastFilename
								lastFail = lastFilename; 
								ORS=""; print "\t\t\t\t\tGolden run outputs:\t\t\t";
								for (c=0; c<= v; c++) {
									lastFailVal[c] = lastLine[c]
									 if ((w-1,c) in goldRes) {
											lastFailGoldVal[c] = goldRes[0,c];
											if (c==0) {print goldRes[w-1,c], "\t"; } else {print goldRes[w-1,c],""; }
									}
								}								
								print "\n\t\t\t\t\tThis experiment outputs:\t";
								for (c=1;c<=v;c++) { if (c >= afterdelta) print lastLine[c],""; if (c==1) print lastLine[c],"\t";}
								ORS="\n";	 print "\n\t\t\t\t\tTransition(s) missed at the end"
								if (lastErr == lastFilename) { 
									errCount--; print "\t\t\t\t\tFound failure with previous Error in current file"
									failAfterErrCount++;
									failAfterErrLatencyHistogram[(lastLine[1] - lastErrTime)]++;
									avgFailAfterErrLatency+= (lastLine[1] - lastErrTime)
									if (maxFailAfterErrLatency <= (lastLine[1] - lastErrTime) ) {
												maxFailAfterErrLatency = (lastLine[1] - lastErrTime) }
								}
							}
							
				}
				
				for (i=afterdelta; i<=NF; i++) {
						if (i in errActive) {
							delete errActive[i];
							value= -1;
							errDurationsHistogram[i,value] +=1;
						}
				}
				
			}



 i=0; do {
				if (ARGV[ARGIND-i] ~ /.lst/) { print ARGV[ARGIND-i] " is last processed file\n\n"; i=ARGIND }
				i++
		} while (i<ARGIND)

				
			
			for (pos in lastErrPositions) {  			# lastErrPositions is a vector of 1 in the positions of registers with error 
				cumulativeErrors[pos] += lastErrPositions[pos]
			}
			delete lastErrPositions
			
			for (pos in lastErrRepetitions) {  		# vector of nr of repetitions in the positions of registers with error 
				cumulativeErrRepetitions[pos] += lastErrRepetitions[pos]
			}
			delete lastErrRepetitions
			
			for (pos in lastErrBitUpsets) {  		# vector of total nr of upset bits for each reg with error
				cumulativeErrBitUpsets[pos] += lastErrBitUpsets[pos]
			}
			delete lastErrBitUpsets
## end of detection of last file hang

if (failureCount) avgFailLatency = avgFailLatency / failureCount;
if (errCount + failAfterErrCount) avgErrLatency = avgErrLatency / (errCount + failAfterErrCount);
if (failAfterErrCount) avgFailAfterErrLatency = avgFailAfterErrLatency / failAfterErrCount;


n=asorti(failLatencyHistogram,fLHs)
print "Distribution of Failure Latencies from fault injection instant:";
for (i=1;i<=n;i++) {
	print "failLatency: ",fLHs[i], " Repetitions: ",failLatencyHistogram[fLHs[i]]; # >> "failLatencyHistogram.log"
}
print "\n"
n=asorti(errLatencyHistogram,eLHs)
print "Distribution of Error Latencies from fault injection instant:";
for (i=1;i<=n;i++) {
	print "errLatency: ",eLHs[i], " Repetitions: ",errLatencyHistogram[eLHs[i]]; # >> "errLatencyHistogram.log"
}
print "\n"
n=asorti(failAfterErrLatencyHistogram,faeLHs)
print "Distribution of Failure After Error Latencies:";
for (i=1;i<=n;i++) {
	print "failAfterErrLatency: ",faeLHs[i], " Repetitions: ",failAfterErrLatencyHistogram[faeLHs[i]]; # >> "failAfterErrLatencyHistogram.log"
}
print "\n"
addedErrs=0;
print "Distribution of accumulated total errors per position of outputs:";
for (pos in cumulativeErrors) {
	if (pos != 0) { 
		addedErrs+=cumulativeErrors[pos];
		print "Position: ",pos, " Accumulated Errors: ",cumulativeErrors[pos];
	}
}
print "\n"
addedRepeatedErrs=0;
print "Distribution of accumulated total error Repetitions per position of outputs:";
for (pos in cumulativeErrRepetitions) {
	if (pos != 0) { 
		addedRepeatedErrs+=cumulativeErrRepetitions[pos];
		print "Position: ",pos, " Accumulated Error Repetitions: ",cumulativeErrRepetitions[pos];
	}
}
print "\n"
addedBUs=0;
print "Distribution of accumulated and average total Bit Upsets per position of outputs:";
for (pos in cumulativeErrBitUpsets) {
	if (pos != 0) { 
		addedBUs+=cumulativeErrBitUpsets[pos];
		print "Position: ",pos, " Accumulated Error Bit Upsets: ",cumulativeErrBitUpsets[pos];
	}
}
print "--------------------"
for (pos in cumulativeErrBitUpsets) {
	if (pos != 0) { 
		print "Position: ",pos, " Average Error Bit Upsets: ",cumulativeErrBitUpsets[pos] / cumulativeErrRepetitions[pos];
	}
}
print "\n"
print "Distribution of Error Durations:";
for (pos in cumulativeErrRepetitions) {
print "POSITION ", pos;
	for (combi in errDurationsHistogram) {
		split(combi, sep, SUBSEP)
		if (sep[1] == pos) {
			print "ErrDurations: ",sep[2], " Repetitions: ",errDurationsHistogram[combi]; # >> "failAfterErrLatencyHistogram.log"
		}
	}
}
print "\n"
print "Distribution of Error Separations:";
for (pos in cumulativeErrRepetitions) {
print "POSITION ", pos;
	for (combi in errSeparationsHistogram) {
		split(combi, sep, SUBSEP)
		if (sep[1] == pos) {
			print "ErrSeparations: ",sep[2], " Repetitions: ",errSeparationsHistogram[combi]; # >> "failAfterErrLatencyHistogram.log"
		}
	}
}
print "\n"


if (addedRepeatedErrs) avgBUs = addedBUs / addedRepeatedErrs;

 # print "Transients (000): \n",transCount;
 # print "Intermittents (011):\n",interCount;
 # print "Permanents (101): \n",permCount;
 # print "Other detected faults/errors (others):\n",othersCount;
 # print "Non detected experiments (110):\n",noDetCount;
 # print "Redetections (not included in previous categories):\n",faultDetectedections;
 # print "Detected faults without ERRORS in registered outputs: \n",detectedNoErr;
 # print "Undetected faults with ERRORS in registered outputs: \n",undetectedErr;
 print "Number of failures found: \n",failureCount;
 print "\t Of total failures, hangs (incl. after error): \n\t ",hangCount;
 print "\t Of total failures, happened after error (incl. hangs): \n\t ",failAfterErrCount;
 print "Number of errors found (excl. prior to failure): \n",errCount;
 print "Accumulated number of errors in all experiments including multiple errors: \n",addedErrs;
 print "Average number of Error Bit Upsets:  \n",avgBUs;
 print "Maximum propagation latency for failures: \n",maxFailLatency;
 print "Maximum propagation latency for errors: \n",maxErrLatency;
 print "Maximum propagation latency from error to failure: \n",maxFailAfterErrLatency;
 print "Average propagation latency for failures: \n",avgFailLatency;
 print "Average propagation latency for errors: \n",avgErrLatency;
 print "Average propagation latency from error to failure: \n",avgFailAfterErrLatency;
 print "Total Experiments excluding FaultFree: \n",totalFiles-1;
 }