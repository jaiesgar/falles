# FILE TO RETRIEVE STAGE INVOLVED IN DIFFERENT TOGGLE NODES JOINING CACHES WITH STAGES
# CALL: gawk  -f simSplitTogglePerstage2.awk {List of inputs} > TogglePerStage.log
#
# EXAMPLE: gawk  -f simSplitTogglePerstage2.awk togglereport.txt > StageOfToggle.log
# EXAMPLE: gawk  -f simSplitTogglePerstage2.awk leon3-minimal2w_*/togglereport.txt > StageOfToggles.log
# EXAMPLE: gawk  -f simSplitTogglePerstage2.awk $(<inputslist.txt) > StageOfToggles.log
# UPDATED:19-may-2015 Created


BEGIN{		filep=0;
				 PROCINFO["sorted_in"] = "@ind_num_asc" # This forces arrays to be sorted by index in numerical increasing order
			}
{

	if (FNR ==1) {
		filep++
		filenames[filep] = FILENAME;
	}
	
	if (/1H->0L/) {
		getline
		getline
		while (!(/^\s*$/)) {
			Nodes[filep,$1,$4]++;
			getline
		}
	}
	
	
}

END{
print "Toggle coverage per stage:"


for (i=1; i<=filep; i++) {
	if (i>1) print "\n"
	print "############################################################"
	print "##   File: ",filenames[i], " ##"
	print "############################################################"
	print "FETCH +INSTR CACHE stage: r.f or rin.f  OR  ici or ico ---------------------------------------"
	for (combined in Nodes) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if ((sep[2] ~ /^(rin|r)\.f\./) || (sep[2] ~ /^ic(i|o)\./)) {
				# print sep[2]
				printedNodes[sep[2]]++;
				stageNodes[i,1]++;
				stagePct[i,1]+=sep[3];
			}
		}
	}
	if (stageNodes[i,1] !=0) {
		print "Toggle coverage of this stage: "(stagePct[i,1] / stageNodes[i,1]) " %\n"
	} else {
		print "No nodes in this stage"
	}
	print "DECODE stage: r.d or rin.d ---------------------------------------"
	for (combined in Nodes) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /^(rin|r)\.d\./) {
				# print sep[2]
				printedNodes[sep[2]]++;
				stageNodes[i,2]++;
				stagePct[i,2]+=sep[3];
			}
		}
	}
	if (stageNodes[i,2] !=0) {
		print "Toggle coverage of this stage: "(stagePct[i,2] / stageNodes[i,2]) " %\n"
	} else {
		print "No nodes in this stage"
	}
	
	print "REGISTER ACCESS stage: r.a or rin.a ---------------------------------------"
	for (combined in Nodes) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /^(rin|r)\.a\./) {
				# print sep[2]
				printedNodes[sep[2]]++;
				stageNodes[i,3]++;
				stagePct[i,3]+=sep[3];
			}
		}
	}
	if (stageNodes[i,3] !=0) {
		print "Toggle coverage of this stage: "(stagePct[i,3] / stageNodes[i,3]) " %\n"
	} else {
		print "No nodes in this stage"
	}
	
	print "EXECUTE stage: r.e or rin.e ---------------------------------------"
	for (combined in Nodes) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /^(rin|r)\.e\./) {
				# print sep[2]
				printedNodes[sep[2]]++;
				stageNodes[i,4]++;
				stagePct[i,4]+=sep[3];
			}
		}
	}
	if (stageNodes[i,4] !=0) {
		print "Toggle coverage of this stage: "(stagePct[i,4] / stageNodes[i,4]) " %\n"
	} else {
		print "No nodes in this stage"
	}
	
	print "MEMORY ACCESS + DATA CACHE stage: r.m or rin.m OR dci or dco ---------------------------------------"
	for (combined in Nodes) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if ((sep[2] ~ /^(rin|r)\.m\./) || (sep[2] ~ /^dc(i|o)\./)) {
				# print sep[2]
				printedNodes[sep[2]]++;
				stageNodes[i,5]++;
				stagePct[i,5]+=sep[3];
			}
		}
	}
	if (stageNodes[i,5] !=0) {
		print "Toggle coverage of this stage: "(stagePct[i,5] / stageNodes[i,5]) " %\n"
	} else {
		print "No nodes in this stage"
	}
	
	print "EXCEPTION stage: r.x or rin.x ---------------------------------------"
	for (combined in Nodes) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /^(rin|r)\.x\./) {
				# print sep[2]
				printedNodes[sep[2]]++;
				stageNodes[i,6]++;
				stagePct[i,6]+=sep[3];
			}
		}
	}
	if (stageNodes[i,6] !=0) {
		print "Toggle coverage of this stage: "(stagePct[i,6] / stageNodes[i,6]) " %\n"
	} else {
		print "No nodes in this stage"
	}
	
	print "WRITE BACK stage: r.w or rin.w ---------------------------------------"
	for (combined in Nodes) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /^(rin|r)\.w\./) {
				# print sep[2]
				printedNodes[sep[2]]++;
				stageNodes[i,7]++;
				stagePct[i,7]+=sep[3];
			}
		}
	}
	if (stageNodes[i,7] !=0) {
		print "Toggle coverage of this stage: "(stagePct[i,7] / stageNodes[i,7]) " %\n"
	} else {
		print "No nodes in this stage"
	}
	
	# print "DATA CACHE related signals: dci or dco ---------------------------------------"
	# for (combined in Nodes) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /^dc(i|o)\./) {
				# # print sep[2]
				# printedNodes[sep[2]]++;
				# stageNodes[i,8]++;
				# stagePct[i,8]+=sep[3];
			# }
		# }
	# }
	# if (stageNodes[i,8] !=0) {
		# print "Toggle coverage of this stage: "(stagePct[i,8] / stageNodes[i,8]) " %\n"
	# } else {
		# print "No nodes in this stage"
	# }
	
	# print "INSTRUCTION CACHE related signals: ici or ico ---------------------------------------"
	# for (combined in Nodes) {
		# split(combined,sep, SUBSEP)
		# if (sep[1] == i) {
			# if (sep[2] ~ /^ic(i|o)\./) {
				# # print sep[2]
				# printedNodes[sep[2]]++;
				# stageNodes[i,9]++;
				# stagePct[i,9]+=sep[3];
			# }
		# }
	# }
	# if (stageNodes[i,9] !=0) {
		# print "Toggle coverage of this stage: "(stagePct[i,9] / stageNodes[i,9]) " %\n"
	# } else {
		# print "No nodes in this stage"
	# }
	
	print "REGISTER FILE related signals: rfi or rfo ---------------------------------------"
	for (combined in Nodes) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (sep[2] ~ /^rf(i|o)\./) {
				# print sep[2]
				printedNodes[sep[2]]++;
				stageNodes[i,10]++;
				stagePct[i,10]+=sep[3];
			}
		}
	}
	if (stageNodes[i,10] !=0) {
		print "Toggle coverage of this stage: "(stagePct[i,10] / stageNodes[i,10]) " %\n"
	} else {
		print "No nodes in this stage"
	}
	
	print "OTHERS related signals ---------------------------------------"
	for (combined in Nodes) {
		split(combined,sep, SUBSEP)
		if (sep[1] == i) {
			if (!(sep[2] in printedNodes)) {
				# print sep[2]
				stageNodes[i,11]++;
				stagePct[i,11]+=sep[3];
			}
		}
	}
	if (stageNodes[i,11] !=0) {
		print "Toggle coverage of this stage: "(stagePct[i,11] / stageNodes[i,11]) " %\n"
	} else {
		print "No nodes in this stage"
	}
	
	delete printedNodes;

}

}