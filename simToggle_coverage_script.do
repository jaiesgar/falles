# EDIT startInjTime AND endTime
# CALL USING: vsim -c -do "project open testbench; do simToggle_coverage_script.do;quit"
vsim -quiet -t ps -vopt -voptargs=+acc -coverage +cover=t -togglefixedsizearray work.testbench
toggle add sim:/testbench/d3/u0/leon3x0/p0/iu/*
# run @startInjTime
run @1211065 ns
toggle reset -all
# run @endTime
run @1506785 ns
coverage save -code t -instance sim:/testbench/d3/u0/leon3x0/p0/iu ./toggles_iu
coverage report -file togglereport.txt -instance /testbench/d3/u0/leon3x0/p0/iu -recursive -detail -all -noannotate -assert -directive -cvg -code t