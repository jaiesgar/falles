# FILE TO RETRIEVE NODES TO INJECT FROM RTL-LEVEL WAVE SIMULATION FILES
# CURRENTLY ADDS EVERYTHING FOUND IN THE INPUT FILES
# AUTHOR: J. Espinosa
# INPUT PARAMETER: firstFile= 1 if first inputfile, to create header of simNodes.do
# INPUT PARAMETER: lastFile= 1 if last inputfile, to create tail of simNodes.do
# INPUT PARAMETER: nameOfUnit, the unit to inject
#
# CALL: gawk [-v firstFile=1] [-v lastFile=1] -v nameOfUnit="myNameOfUnit" -f simWriteNodesRTL.awk wavefile.do >simNodes.do
# EXAMPLE: gawk -v firstFile=1 -v lastFile=1 -v nameOfUnit=p0 -f simWriteNodesRTL.awk d3u0leon3p0wave.do >simNodes.do
# UPDATED 07/6/2014 added quietly to avoid printing nodes

BEGIN{ 	OFS="";
				if (firstFile==1) {
					print "# SUPPORT FILE TO SELECT NODES TO INJECT. CALLED BY simInjections";
					print "# Call using: source simNodes.do\n";
					print "quietly set injNodes {";
				}
			}
{

	# EXPAND AND ADD VIRTUAL SIGNALS FROM WAVE.DO FILE
	if (/virtual signal/ && $1 !~ /\#/) {
		base=0;
		for (i=1;i<=(NF-2);i++) {
			if($i ~/context/) {sigpath=$(i+1); base=i+2;}
			if (i==base) {
				sub (/\)\(/,"",$i);
				if (sigpath ~ nameOfUnit || $i ~ nameOfUnit) print "{",sigpath,"/",$i,"}"
			}
			if (base!=0 && i>base && $i !~ /&$/) {
				if ($i !~/&/) { if (sigpath ~ nameOfUnit || $i ~ nameOfUnit) print "{",sigpath,"/",$i,"}" }
				else { 
					sub(/&/, "",$i); # remove trailing '&' when it appears
					if (sigpath ~ nameOfUnit || $i ~ nameOfUnit) print "{",sigpath,"/",$i,"}"
				}
			}
		}
	}
	
	# ADD FROM MANUALLY SAVED EXPANDED SIGNALS (see d3u0leon3p0unexpanded.do FILE)
	if (FILENAME ~ /unexpanded/) {
		for (i=1;i<=(NF);i++) {
			if ($i ~/sim:/) {
				sub(/{sim:/,"",$i)
				if ($i ~ nameOfUnit) print "{",$i,"}"
			}
		}
	}
	

}
END{
			if (lastFile==1) {
				print "};\n"
			}
		}
 